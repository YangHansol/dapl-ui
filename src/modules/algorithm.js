import * as LoadingAction from "../library/loading/LoadingAction";
import * as ErrorAction from "../library/error/ErrorAction";
//TODO: workspace 연결
import * as WorkspaceAction from "../workspace/WorkspaceAction";
import AlgorithmService from "../library/service/AlgorithmService";

const CATEGORY = "algorithm";

export const ActionTypes = Object.freeze({
  INIT_ACTION: "INIT_ACTION",

  GET_ALGORITHM_LIST: `${CATEGORY}/GET_ALGORITHM_LIST`,
  UPDATE_ALGORITHM_LIST: `${CATEGORY}/UPDATE_ALGORITHM_LIST`,
  ALGORITHM_DELETE: `${CATEGORY}/ALGORITHM_DELETE`,
  BACKUP_ALGORITHM: `${CATEGORY}/BACKUP_ALGORITHM`,
});

export function deleteAlgo(athmId, wkspcId) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return AlgorithmService.deleteAlgo(athmId, wkspcId)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        dispatch({
          type: ActionTypes.ALGORITHM_DELETE,
        });
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        dispatch(ErrorAction.Error(e));
      });
  };
}

export function getAlgoList(page, size) {
  return (dispatch, getState) => {
    return AlgorithmService.getAlgoList(page, size).then(results => {
      dispatch({
        type: ActionTypes.GET_ALGORITHM_LIST,
        payload: {
          algorithmListInfo: results.data,
        },
      });
    });
  };
}

export function updateAlgo(postData) {
  return (dispatch, getState) => {
    return AlgorithmService.updateAlgo(postData)
      .then(res => {
        dispatch({
          type: ActionTypes.UPDATE_ALGORITHM_LIST,
        });
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        dispatch(ErrorAction.Error(e));
      });
  };
}

export function backupAlgorithm(wkspcId) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return AlgorithmService.backupAlgorithm(wkspcId)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        dispatch({
          type: ActionTypes.BACKUP_ALGORITHM,
        });
        dispatch(WorkspaceAction.deleteWorkspace(wkspcId));
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        dispatch(ErrorAction.Error(e));
      });
  };
}

const initialState = {
  action: "",
  algorithmListInfo: {},
};

export default function AlgorithmReducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.INIT_ACTION:
      return Object.assign({}, state, {
        action: initialState.action,
      });

    case ActionTypes.UPDATE_ALGORITHM_LIST:
    case ActionTypes.ALGORITHM_DELETE:
      return Object.assign({}, state, {
        action: "UPDATE",
      });

    case ActionTypes.GET_ALGORITHM_LIST:
      return Object.assign({}, state, {
        algorithmListInfo: action.payload.algorithmListInfo,
      });
    default:
      return state;
  }
}
