import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
// import MailIcon from "@material-ui/icons/Mail";
import { ExploreIcon } from "../../../utils/styles/Icons";
import styled from "styled-components";
import CustomInput from "../Input/CustomInput";

const useStyles = makeStyles({
  list: {
    width: 370
  },
  fullList: {
    width: "auto"
  }
});

const StyledForm = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  padding: 1rem;
  /* align-items: center; */
`;

const StyledButton = styled.span`
  color: ${props =>
    props.buttoncolorprops ? props.buttoncolorprops : "inherit"};
`;

export default function DatasetTemporaryDrawer({
  buttonProps,
  buttoncolorprops
}) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false
  });
  const [buttonText] = useState(buttonProps);
  // const [buttonText, setButtonText] = useState(buttonProps);

  // const datasetTitle = ["그룹명", "CPU", "디스크", "메모리"];
  // const datasetContent = [
  //   "그룹명을 입력하세요",
  //   "선택하세요",
  //   "선택하세요",
  //   "선택하세요"
  // ];

  const toggleDrawer = (side, open) => event => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      // onClick={toggleDrawer(side, false)}
      // onKeyDown={toggleDrawer(side, false)}
    >
      <StyledForm>
        <CustomInput />
      </StyledForm>
      <Divider />
      <List>
        <ListItem button onClick={() => alert("cancel")}>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText primary={"cancel"} />
        </ListItem>
        <ListItem button onClick={() => alert("complete")}>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText primary={"complete"} />
        </ListItem>
      </List>
    </div>
  );

  return (
    <div>
      <Button onClick={toggleDrawer("right", true)}>
        <StyledButton buttoncolorprops={buttoncolorprops}>
          {buttonText ? buttonText : <ExploreIcon />}
        </StyledButton>
      </Button>

      <Drawer
        anchor="right"
        open={state.right}
        onClose={toggleDrawer("right", false)}
      >
        {sideList("right")}
      </Drawer>
    </div>
  );
}
