import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import { ExploreIcon } from "../../../utils/styles/Icons";
import styled from "styled-components";

const useStyles = makeStyles({
  list: {
    width: 500
  },
  fullList: {
    width: "auto"
  }
});

const StyledForm = styled.div`
  display: flex;
  justify-content: space-around;
  /* align-items: center; */
`;

const StyledButton = styled.span`
  color: ${props =>
    props.buttoncolorprops ? props.buttoncolorprops : "inherit"};
`;

export default function DatasetTemporaryDrawer({
  buttonProps,
  buttoncolorprops
}) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false
  });
  const [buttonText, setButtonText] = useState(buttonProps);

  const datasetTitle = [
    "종류",
    "파일명",
    "용량",
    "공개여부",
    "생성자",
    "소유자"
  ];
  const datasetContent = [
    "FILE",
    "est.csv",
    "10 Bytes",
    "... ",
    "공개 ",
    "사용자(user)"
  ];

  const toggleDrawer = (side, open) => event => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      // onClick={toggleDrawer(side, false)}
      // onKeyDown={toggleDrawer(side, false)}
    >
      <StyledForm>
        <List>
          {datasetTitle.map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
        <List>
          {datasetContent.map((text, index) => (
            <ListItem button key={text} onClick={() => alert(text)}>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
      </StyledForm>
      <Divider />
      <List>
        {["All mail", "Trash", "Spam"].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
        <ListItem button onClick={() => alert("complete")}>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText primary={"complete"} />
        </ListItem>
      </List>
    </div>
  );

  return (
    <div>
      <Button onClick={toggleDrawer("right", true)}>
        <StyledButton buttoncolorprops={buttoncolorprops}>
          {buttonText ? buttonText : <ExploreIcon />}
        </StyledButton>
      </Button>

      <Drawer
        anchor="right"
        open={state.right}
        onClose={toggleDrawer("right", false)}
      >
        {sideList("right")}
      </Drawer>
    </div>
  );
}
