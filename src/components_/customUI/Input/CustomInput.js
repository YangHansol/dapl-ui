import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: 230
    }
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 230
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));

export default function CustomInput() {
  const classes = useStyles();

  const [state, setState] = React.useState({
    age: "",
    name: "hai"
  });

  const handleChange = name => event => {
    setState({
      ...state,
      [name]: event.target.value
    });
  };

  return (
    <>
      <form className={classes.root} noValidate autoComplete="off">
        <TextField id="standard-basic" label="그룹명" />
      </form>
      <div>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="age-native-simple">CPU</InputLabel>
          <Select
            native
            value={state.age}
            onChange={handleChange("age")}
            inputProps={{
              name: "age",
              id: "age-native-simple"
            }}
          >
            <option value="" />
            <option value={1}>1</option>
            <option value={2}>2</option>
            <option value={3}>3</option>
          </Select>
        </FormControl>
      </div>{" "}
      <div>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="age-native-simple">디스크</InputLabel>
          <Select
            native
            value={state.age}
            onChange={handleChange("age")}
            inputProps={{
              name: "age",
              id: "age-native-simple"
            }}
          >
            <option value="" />
            <option value={1}>1</option>
            <option value={2}>2</option>
            <option value={3}>3</option>
          </Select>
        </FormControl>
      </div>{" "}
      <div>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="age-native-simple">메모리</InputLabel>
          <Select
            native
            value={state.age}
            onChange={handleChange("age")}
            inputProps={{
              name: "age",
              id: "age-native-simple"
            }}
          >
            <option value="" />
            <option value={1}>1</option>
            <option value={2}>2</option>
            <option value={3}>3</option>
          </Select>
        </FormControl>
      </div>
    </>
  );
}
