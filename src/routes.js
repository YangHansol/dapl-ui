// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";

// 아이콘
import Group from "@material-ui/icons/Group";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import ArchiveIcon from "@material-ui/icons/Archive";
import AllInboxIcon from "@material-ui/icons/AllInbox";
import CategoryIcon from "@material-ui/icons/Category";
import ColorizeIcon from "@material-ui/icons/Colorize";
import InputIcon from "@material-ui/icons/Input";

// core components/views for Admin layout
import DashboardPage from "./views/Dashboard/Dashboard.js";
import GroupContainer from "./views/Group/GroupContainer.js";
import UserProfile from "./views/UserProfile/UserProfile.js";
import WorkspaceContainer from "./views/Workspace/WorkspaceContainer.js";
import DatasetContainer from "./views/Dataset/DatasetContainer.js";
import LibraryContainer from "./views/Package/PackageContainer.js/index.js";
import PackagePackContainer from "./views/PackagePack/PackagePackContainer.js/index.js";
import AlgorithmContainer from "./views/Algorithm/AlgorithmContainer.js";
import ExperimentContainer from "./views/Experiment/ExperimentContainer.js";
import ReleaseContainer from "./views/Release/ReleaseContainer.js";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "대시보드",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/group",
    name: "그룹",
    icon: Group,
    component: GroupContainer,
    layout: "/admin"
  },
  {
    path: "/user",
    name: "사용자",
    icon: Person,
    component: UserProfile,
    layout: "/admin"
  },
  {
    path: "/workspace",
    name: "워크스페이스",
    icon: LibraryBooks,
    component: WorkspaceContainer,
    layout: "/admin"
  },
  {
    path: "/dataset",
    name: "데이터셋",
    icon: EqualizerIcon,
    component: DatasetContainer,
    layout: "/admin"
  },
  {
    path: "/library",
    name: "라이브러리",
    icon: ArchiveIcon,
    component: LibraryContainer,
    layout: "/admin"
  },
  {
    path: "/pack",
    name: "라이브러리 팩",
    icon: AllInboxIcon,
    component: PackagePackContainer,
    layout: "/admin"
  },
  {
    path: "/algorithm",
    name: "알고리즘",
    icon: CategoryIcon,
    component: AlgorithmContainer,
    layout: "/admin"
  },
  {
    path: "/experiment",
    name: "실험",
    icon: ColorizeIcon,
    component: ExperimentContainer,
    layout: "/admin"
  },
  {
    path: "/release",
    name: "배포",
    icon: InputIcon,
    component: ReleaseContainer,
    layout: "/admin"
  }
];

export default dashboardRoutes;
