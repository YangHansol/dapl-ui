import React from "react";
import { Provider } from "react-redux";
import {
  StylesProvider,
  ThemeProvider as MuiThemeProvider,
} from "@material-ui/core/styles";
import { ThemeProvider } from "styled-components";
import RouterComponent from "./RouterComponent";
import GlobalStyles from "./utils/styles/GlobalStyles";
import { theme } from "./utils/styles/MuiTheme";
import "assets/css/material-dashboard-react.css?v=1.8.0";
import "../node_modules/react-vis/dist/style.css";

import ConfigStore from "./store";
import * as createHistory from "history";

import { DndProvider } from "react-dnd";
import Backend from "react-dnd-html5-backend";

export const store = ConfigStore();
export const history = createHistory.createBrowserHistory();

function App() {
  return (
    <Provider store={store}>
      <StylesProvider injectFirst>
        <ThemeProvider theme={theme}>
          <MuiThemeProvider theme={theme}>
            <GlobalStyles />
            <DndProvider backend={Backend}>
              <RouterComponent />
            </DndProvider>
          </MuiThemeProvider>
        </ThemeProvider>
      </StylesProvider>
    </Provider>
  );
}

export default App;
