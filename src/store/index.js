import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { createLogger } from "redux-logger";
import combinedReducers from "../state/moduels/reducers/RootReducers";

const logger = createLogger({ level: "info", collapsed: true });

export default function ConfigStore(initialState) {
  const middlewares = [thunkMiddleware];

  if (process.env.NODE_ENV === "development") {
    middlewares.push(logger);
  }

  const middlewareEnhancer = applyMiddleware(...middlewares);

  return createStore(combinedReducers, initialState, middlewareEnhancer);
}
