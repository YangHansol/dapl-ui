export const WorkspaceStatus = {
  ACT: "동작중",
  RJT: "반려",
  STOP: "중지",
  WAIT: "대기",
  BACKUP: "비활성",
};

export const WorkspaceStatusCode = {
  ACT: "ACT",
  RJT: "RJT",
  STP: "STOP",
  WT: "WAIT",
  BACKUP: "BACKUP",
};

export const WorkspaceSizeType = {
  CSTM: "Custom",
  LRG: "Large",
  MDM: "Medium",
  MIC: "Micro",
  XLRG: "Xlarge",
};

export const LibraryLrgCode = {
  ML: "ML",
  CE: "CE",
  LN: "LN",
  PP: "PP",
  RL: "RL",
  AN: "AN",
  ETC: "ETC",
};

export const LibrarySmlCdType = {
  TSF: "tensorflow",
  SCKL: "scikit-learn",
  H2O: "h2o",
  KRS: "keras",
  ETC: "기타",
  JL: "Jupyterlab",
  RS: "R-Studio",
  ZP: "zepplin",
  PY: "Python",
  R: "R",
  TSS: "scikit-learn release",
  SCKLR: "tensorboard",
};

export const LibrarySmlCdCode = {
  TSF: "TSF",
  SCKL: "SCKL",
  H2O: "H2O",
  KRS: "KRS",
  ETC: "ETC",
  JL: "JL",
  RS: "RS",
  ZP: "ZP",
  PY: "PY",
  R: "R",
  TSS: "TSS",
  SCKLR: "SCKLR",
};

export const LibraryType = {
  ETC: "기타",
  H2O: "H2O",
  KRS: "Keras",
  SCKL: "Scikit - learn",
  TSF: "Tensorflow",
};

export const LibraryTypeCode = {
  ETC: "ETC",
  H2O: "H2O",
  KRS: "KRS",
  SCKL: "SCKL",
  TSF: "TSF",
};

export const LibraryTypeText = {
  기타: "ETC",
  H2O: "H2O",
  Keras: "KRS",
  "Scikit - learn": "SCKL",
  Tensorflow: "TSF",
};

export const DistributeStatus = {
  END: "배포(서비스) 종료",
  RJT: "거절",
  RUN: "배포완료",
  WAIT: "요청중",
};

export const DistributeStatusCode = {
  END: "END",
  RJT: "RJT",
  RUN: "RUN",
  WT: "WAIT",
};

export const DockerImgType = {
  BASE: "기본",
  ENV: "워크스페이스 생성",
  PLG: "플러그인",
  RLS: "베포",
};

export const DockerImgTypeCode = {
  BASE: "BASE",
  ENV: "ENV",
  PLG: "PLG",
  RLS: "RLS",
};

export const DockerImgSelectItems = [
  { value: DockerImgTypeCode.BASE, title: DockerImgType.BASE },
  { value: DockerImgTypeCode.ENV, title: DockerImgType.ENV },
  { value: DockerImgTypeCode.PLG, title: DockerImgType.PLG },
  { value: DockerImgTypeCode.RLS, title: DockerImgType.RLS },
];

export const WorkspaceUsageTypeText = {
  LRN: "학습",
  PLG: "플러그인",
  TST: "테스트",
};

export const WorkspaceUsageType = {
  learning: "LRN",
  plugIn: "PLG",
  test: "TST",
};

export const ServerType = {
  MD: "운영",
  WK: "학습",
};

export const LogType = {
  LEARN: "학습",
  AGR: "알고리즘",
  DATA: "데이터셋",
  ETC: "기타",
  EXPR: "실험",
  MD: "모델",
  RLS: "배포",
  WKSPC: "워크스페이스",
  SQL: "SQL",
  "AGR-LOC": "로컬 알고리즘",

  BOARD: "게시판",
  COMM: "공통코드",
  "DATA-LOC": "로컬 데이터셋",
  DOCK: "도커이미지",
  "DOCK-LIB": "도커이미지 라이브러리",
  FUNCTION: "기능",
  "LIB-LRG": "라이브러리 대분류",
  "LIB-SML": "라이브러리 소분류",
  PKG: "패키지",
  SERV: "서버",
  USER: "사용자",
  "WKSPC-GPU": "워크스페이스 GPU할당",
  "WKSPC-SHARE": "워크스페이스 공유",
  SCHEDULER: "스케쥴러",
};

export const DatasetType = {
  FL: "File",
  QR: "SQL",
  "0": "DATABASE",
  "1": "FILE",
};

export const DatasetTypeCode = {
  FL: "FL",
  QR: "QR",
};

export const QueryDbType = {
  hive: "HIVE",
  impala: "IMPALA",
  DB2: "DB2",
  HIVE: "HIVE",
  IMPALA: "IMPALA",
};

export const AlgorithmTypeText = {
  BASE: "기본 제공",
  USER: "사용자 생성",
};

export const AlgorithmType = {
  BASE: "BASE",
  USER: "USER",
};

export const UserStatus = {
  ACT: "활성",
  STOP: "중지",
  WAIT: "대기",
  RETIRE: "퇴사",
};

export const UserStatusCode = {
  ACT: "ACT",
  STP: "STOP",
  WT: "WAIT",
  RETIRE: "RETIRE",
};

export const ExperimentStatusText = {
  CNCL: "취소",
  END: "완료",
  PPR: "준비",
  RUN: "실행중",
  FAIL: "실패",
};
export const LearnFlText = {
  Y: "필요",
  N: "불필요",
};

export const ExperimentStatus = {
  CNCL: "CNCL",
  END: "END",
  PPR: "PPR",
  RUN: "RUN",
  FAIL: "FAIL",
};

export const QueryStatus = {
  ACPT: "수락",
  REQ: "요청중",
  RJT: "거절",
  END: "완료",
  FAIL: "실패",
  RUN: "수행",
};

export const QueryStatusCode = {
  ACPT: "ACPT",
  REQ: "REQ",
  RJT: "RJT",
  END: "END",
  FAIL: "FAIL",
  RUN: "RUN",
};

export const ShareStatus = {
  Y: "Y",
  N: "N",
};

export const ShareText = {
  Y: "공개",
  N: "비공개",
  P: "미공개",
};

export const PortType = {
  JPTL: "JPTL",
  RSTD: "RSTD",
  TSBD: "TSBD",
  ZPL: "ZPL",
};

export const Volume = {
  "1GB": 1,
  "8GB": 8,
  "10GB": 10,
  "16GB": 16,
  "32GB": 32,
  "48GB": 48,
  "50GB": 50,
  "64GB": 64,
  "100GB": 100,
  "128GB": 128,
  "200GB": 200,
  "256GB": 256,
  "300GB": 300,
  "600GB": 600,
  "1TB": 1000,
};

export const VolumeText = {
  1: "1GB",
  8: "8GB",
  10: "10GB",
  16: "16GB",
  32: "32GB",
  48: "48GB",
  50: "50GB",
  64: "64GB",
  100: "100GB",
  128: "128GB",
  200: "200GB",
  256: "256GB",
  300: "300GB",
  600: "600GB",
  1000: "1TB",
};

export const WorkspaceSizeTypeText = {
  Custom: "CSTM",
  Large: "LRG",
  Medium: "MDM",
  Micro: "MIC",
  Xlarge: "XLRG",
};

export const WKSPCRadioItems = [
  { value: WorkspaceSizeTypeText["Micro"], title: "Micro" },
  { value: WorkspaceSizeTypeText["Medium"], title: "Medium" },
  { value: WorkspaceSizeTypeText["Large"], title: "Large" },
  { value: WorkspaceSizeTypeText["Xlarge"], title: "Xlarge" },
  { value: WorkspaceSizeTypeText["Custom"], title: "Custom" },
];

export const WorkspaceLangTypeText = {
  Python: "PY",
  R: "R",
};

export const LangText = {
  PY: "Python",
  R: "R",
  python: "Python",
};

export const WKSPCRadioLang = [
  { value: WorkspaceLangTypeText["Python"], title: "Python" },
  { value: WorkspaceLangTypeText["R"], title: "R" },
];

export const WKSPCRadioPrcs = [
  { value: "CPU", title: "CPU" },
  { value: "GPU", title: "GPU" },
];

export const WKSPCDockerImg = {
  PYCPU: { value: "PYCPU", title: "Jupyter Lab (CPU)" },
  PYGPU: { value: "PYGPU", title: "Jupyter Lab (GPU)" },
  RCPU: { value: "RCPU", title: "R-studio (CPU)" },
  RGPU: { value: "RGPU", title: "R-studio (GPU)" },
};

export const SelectWorkspaceType = {
  [WorkspaceSizeTypeText.Micro]: {
    asgndCpuNum: 1,
    asgndMemSz: Volume["8GB"],
    asgndGpuNum: 0,
    asgndDisSz: Volume["50GB"],
  },

  [WorkspaceSizeTypeText.Medium]: {
    asgndCpuNum: 4,
    asgndMemSz: Volume["16GB"],
    asgndGpuNum: 0,
    asgndDisSz: Volume["100GB"],
  },

  [WorkspaceSizeTypeText.Large]: {
    asgndCpuNum: 8,
    asgndMemSz: Volume["32GB"],
    asgndGpuNum: 0,
    asgndDisSz: Volume["200GB"],
  },

  [WorkspaceSizeTypeText.Xlarge]: {
    asgndCpuNum: 16,
    asgndMemSz: Volume["128GB"],
    asgndGpuNum: 0,
    asgndDisSz: Volume["300GB"],
  },
};

export const WKSPCUseTsbdValues = [
  { value: "Y", title: "사용" },
  { value: "N", title: "미사용" },
];

export const LibrarySelectItems = [
  { value: LibraryTypeText["기타"], title: "기타" },
  { value: LibraryTypeText["H2O"], title: "H2O" },
  { value: LibraryTypeText["Keras"], title: "Keras" },
  { value: LibraryTypeText["Scikit - learn"], title: "Scikit - learn" },
  { value: LibraryTypeText["Tensorflow"], title: "Tensorflow" },
];

export const EmailItems = [
  { value: "naver.com", title: "naver.com" },
  { value: "gmail.com", title: "gmail.com" },
  { value: "USEREMAIL", title: "직접입력" },
];

export const WKSPCCpuItems = [
  { value: "1", title: "1" },
  { value: "2", title: "2" },
  { value: "3", title: "3" },
  { value: "4", title: "4" },
  { value: "5", title: "5" },
  { value: "6", title: "6" },
  { value: "7", title: "7" },
  { value: "8", title: "8" },
  { value: "9", title: "9" },
  { value: "10", title: "10" },
  { value: "11", title: "11" },
  { value: "12", title: "12" },
  { value: "13", title: "13" },
  { value: "14", title: "14" },
  { value: "15", title: "15" },
  { value: "16", title: "16" },
  { value: "17", title: "17" },
  { value: "18", title: "18" },
  { value: "19", title: "19" },
  { value: "20", title: "20" },
  { value: "21", title: "21" },
  { value: "22", title: "22" },
  { value: "23", title: "23" },
  { value: "24", title: "24" },
  { value: "25", title: "25" },
  { value: "26", title: "26" },
  { value: "27", title: "27" },
  { value: "28", title: "28" },
  { value: "29", title: "29" },
  { value: "30", title: "30" },
  { value: "31", title: "31" },
  { value: "32", title: "32" },
  { value: "33", title: "33" },
  { value: "34", title: "34" },
  { value: "35", title: "35" },
  { value: "36", title: "36" },
  { value: "37", title: "37" },
  { value: "38", title: "38" },
  { value: "39", title: "39" },
  { value: "40", title: "40" },
];

export const WKSPCGpuItems = [
  { value: "0", title: "0" },
  { value: "1", title: "1" },
  { value: "2", title: "2" },
];

export const WKSPCMemoryItems = [
  { value: Volume["1GB"], title: "1GB" },
  { value: Volume["8GB"], title: "8GB" },
  { value: Volume["16GB"], title: "16GB" },
  { value: Volume["32GB"], title: "32GB" },
  { value: Volume["64GB"], title: "64GB" },
  { value: Volume["128GB"], title: "128GB" },
  { value: Volume["256GB"], title: "256GB" },
];

export const WKSPCDiskItems = [
  { value: Volume["10GB"], title: "10GB" },
  { value: Volume["50GB"], title: "50GB" },
  { value: Volume["100GB"], title: "100GB" },
  { value: Volume["200GB"], title: "200GB" },
  { value: Volume["300GB"], title: "300GB" },
];

export const PackageStatusText = {
  RUN: "시작",
  ERR: "오류",
  END: "완료",
};

export const PackageStatus = {
  RUN: "RUN",
  ERR: "ERR",
  END: "END",
};

export const apiUrl = "http://192.168.18.54:8080";
export const gitUrl = "http://192.168.18.54:10090/";
