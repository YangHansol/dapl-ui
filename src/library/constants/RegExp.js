export const RegExp = {
  EMAIL: /^[a-z0-9+.-]+@([a-z0-9-]+\.)+[a-z0-9]{2,4}$/,
  PASSWORD: /^(?=.*[a-zA-Z])((?=.*\d)|(?=.*\W)).{6,20}$/,
  ENGLISH: /^[A-Za-z0-9_+.-]*$/,
  KOREAN: /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/,
  PACKAGEEXTENSTION: /(\.)(tar.gz|zip|whl)$/i,
  CSV: /(\.)(csv)$/i,
};
