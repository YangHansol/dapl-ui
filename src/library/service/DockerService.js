import RestAPICaller from "../../RestAPICaller";

export default class DockerService {
  static getDockerList(page, size) {
    const url = `/v1/docker/list?number=${page}&size=${size}`;
    return RestAPICaller.get(url);
  }

  static getDockerDetail(imgId) {
    const url = `/v1/docker/read?imgId=${imgId}`;
    return RestAPICaller.get(url);
  }

  static deleteDocker(imgId) {
    const url = `/v1/docker/delete`;
    const postData = { imgId };
    return RestAPICaller.post(url, postData);
  }

  static updateDocker(updateData) {
    const url = `/v1/docker/update`;
    const postData = { ...updateData };
    return RestAPICaller.post(url, postData);
  }

  static getDockerLibrary(page, size) {
    const url = `/v1/docker/library/list?number=${page}&size=${size}`;
    return RestAPICaller.get(url);
  }

  static deleteDockerLibrary(data) {
    const url = `/v1/docker/library/delete`;
    const postData = {
      imgId: data.imgId,
      libSmlCd: data.libSmlCd
    };

    return RestAPICaller.post(url, postData);
  }

  static createDockerLibrary(data) {
    const url = `/v1/docker/library/create`;
    const postData = {
      imgId: data.imgId,
      libSmlCd: data.libSmlCd
    };

    return RestAPICaller.post(url, postData);
  }

  static getLibLrgListInfo(number, size) {
    const url = `/v1/library/large/list?number=${number}&size=${size}`;
    return RestAPICaller.get(url);
  }

  static getLibSmlListInfo(number, size, lrgCd) {
    const url = `/v1/library/large/list?number=${number}&size=${size}&libLrgCd=${lrgCd}`;
    return RestAPICaller.get(url);
  }
}
