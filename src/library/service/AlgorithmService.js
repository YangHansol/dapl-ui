import RestAPICaller from "../RestAPICaller";

export default class AlgorithmService {
  static getLocalAlgoList(wkspcId, page, size) {
    const url = `/v1/algorithm/local/list?number=${page}&size=${size}&wkspcId=${wkspcId}`;

    return RestAPICaller.get(url);
  }
  static getLocalAlgoRead(athmId) {
    const url = `/v1/algorithm/local/read?athmId=${athmId}`;

    return RestAPICaller.get(url);
  }
  static getAlgoList(page, size) {
    const url = `/v1/algorithm/list?number=${page}&size=${size}`;

    return RestAPICaller.get(url);
  }
  static getAlgoMoreList(page, size, athmTp) {
    const url = `/v1/algorithm/list?number=${page}&size=${size}&athmTp=${athmTp}`;

    return RestAPICaller.get(url);
  }

  static createAlgo(data) {
    const url = `/v1/algorithm/local/create`;
    const postData = {
      athmNm: data.athmNm,
      athmDesc: data.athmDesc,
      wkspcId: data.wkspcId,
      localRptr: data.localRptr,
      libTp: data.libTp
    };
    return RestAPICaller.post(url, postData);
  }

  static deleteAlgo(athmId) {
    const url = `/v1/algorithm/delete`;
    const postData = {
      athmId
    };
    return RestAPICaller.post(url, postData);
  }
  static downloadAlgo(athmId, wkspcId, newAthmNm, localRptr) {
    const url = `/v1/algorithm/local/download`;
    const postData = {
      athmId,
      wkspcId,
      athmNm: newAthmNm,
      localRptr
    };
    return RestAPICaller.post(url, postData);
  }

  static commitAlgo(athmId, cmtMsg) {
    const url = `/v1/algorithm/local/commit`;

    let postData = { athmId };
    if (cmtMsg) {
      postData.cmtMsg = cmtMsg;
    }
    return RestAPICaller.post(url, postData);
  }

  static publishAlgo(params) {
    const url = `/v1/algorithm/local/publish`;

    return RestAPICaller.post(url, params);
  }

  static getRollbackLog(athmId) {
    const url = `/v1/algorithm/local/logs?athmId=${athmId}`;

    return RestAPICaller.get(url);
  }

  static rollbackAlgo(athmId, rvsNum, learnId) {
    const url = `/v1/algorithm/local/rollback?athmId=${athmId}&rvsNum=${rvsNum}&learnId=${learnId}`;

    return RestAPICaller.get(url);
  }

  static updateAlgo(postData) {
    const url = `/v1/algorithm/update`;
    return RestAPICaller.post(url, postData);
  }

  static backupAlgorithm(wkspcId) {
    const url = `/v1/workspace/save`;
    const postData = { wkspcId };
    return RestAPICaller.post(url, postData);
  }
}
