import RestAPICaller from '../../RestAPICaller'

export default class BoardService {
  static getBoardDetail(msgId) {
		const url = `/v1/board/read?msgId=${msgId}`;

		return RestAPICaller.get(url);
	}
  static deleteBoard(msgId) {
		const url = `/v1/board/delete`;
    const postData = {msgId}

		return RestAPICaller.post(url, postData);
	}
  static updateBoard(data) {
		const url = `/v1/board/update`;
    const postData = {
      msgId: data.msgId,
      sjt: data.sjt,
      ctnt: data.ctnt,
    }
		return RestAPICaller.post(url, postData);
	}

  static createBoard(params) {
		const url = `/v1/board/create`;
		return RestAPICaller.fileUpload(url, params);
	}
  static deleteBoardFile(flId) {
		const url = `/v1/board/file/delete`;
    let postData = {flId}
		return RestAPICaller.post(url, postData);
	}
}