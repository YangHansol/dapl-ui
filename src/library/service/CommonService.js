import RestAPICaller from "../RestAPICaller";

export default class CommonService {
  static getCommonList(category, params) {
    const url = `/v1/${category}/list`;
    return RestAPICaller.get(url, params);
  }

  static getCommonCode(grpCd) {
    const url = `/v1/common_code/list?grpCd=${grpCd}`;
    return RestAPICaller.get(url);
  }

  static getLibSmList(libLrgCd) {
    const url = `/v1/library/small/list?number=0&size=10&sort=regDt.desc&libLrgCd=${libLrgCd}`;
    return RestAPICaller.get(url);
  }

  static getKubernetesUrl() {
    const url = `/v1/workspace/kubernetes`;
    return RestAPICaller.get(url);
  }

  static getPackageCounts() {
    const url = `/v1/package/count`;
    return RestAPICaller.get(url);
  }

  static getResourceList() {
    const url = `/v1/workspace/custom/list`;
    return RestAPICaller.get(url, null, "common");
  }

  static getMonitorWorker() {
    const url = `/v1/monitor/worker`;
    return RestAPICaller.get(url);
  }

  static getMonitorNode() {
    const url = `/v1/monitor/node`;
    return RestAPICaller.get(url);
  }

  static getMonitorModule() {
    const url = `/v1/monitor/module`;
    return RestAPICaller.get(url);
  }
}
