import RestAPICaller from "../../RestAPICaller";

export default class MemberService {
  static createMember(msgData) {
    const url = `/v1/user/create`;
    return RestAPICaller.post(url, msgData);
  }

  static joinMember(msgData) {
    const url = `/v1/user/join`;
    return RestAPICaller.post(url, msgData);
  }

  static loginMember(params) {
    const url = `/v1/user/login`;
    return RestAPICaller.post(url, params);
  }

  static getMemberList(params) {
    const url = `/v1/user/list`;
    return RestAPICaller.get(url, params);
  }

  static getGroupMember(page, size, groupId) {
    const url = `/v1/user/list?number=${page}&size=${size}&groupId=${groupId}`;
    return RestAPICaller.get(url);
  }

  static getUserDetail(userId) {
    const url = `/v1/user/read?userId=${userId}`;
    return RestAPICaller.get(url);
  }

  static deleteUser(userId) {
    const url = `/v1/user/delete`;
    const postData = { userId };
    return RestAPICaller.post(url, postData);
  }

  static updateUser(postData) {
    const url = `/v1/user/update`;
    return RestAPICaller.post(url, postData);
  }

  static passwordUpdate(data) {
    const url = `/v1/user/passupdate`;
    return RestAPICaller.post(url, data);
  }

  static passwordInit(userId) {
    const url = `/v1/user/passinit`;
    const postData = { userId };
    return RestAPICaller.post(url, postData);
  }

  static getLogList(params) {
    const url = `/v1/logs/list`;
    return RestAPICaller.get(url, params);
  }

  static ssoInit(userId) {
    const url = `/v1/user/ssoinit`;
    const postData = { userId };
    return RestAPICaller.post(url, postData);
  }

  static accessTokenReIssue(userId) {
    const url = `/v1/user/reissueGitToken`;
    const postData = { userId };
    return RestAPICaller.post(url, postData);
  }
}
