import RestAPICaller from "../../RestAPICaller";

export default class ExperimentService {
  static getExprList(number, size, params) {
    const url = `/v1/experiment/list?number=${number}&size=${size}`;
    return RestAPICaller.get(url, params);
  }

  static getExprLearnList(params) {
    const url = `/v1/experiment/learn/list`;
    return RestAPICaller.get(url, params);
  }

  static getExprDetail(exprId) {
    const url = `/v1/experiment/read?exprId=${exprId}`;
    return RestAPICaller.get(url);
  }

  static getExprLearnDetail(learnId) {
    const url = `/v1/experiment/learn/read?learnId=${learnId.toString()}`;
    return RestAPICaller.get(url);
  }

  static cancelExpr(exprId) {
    const url = `/v1/experiment/cancel?exprId=${exprId}`;
    return RestAPICaller.get(url);
  }

  static runExpr(exprId) {
    const url = `/v1/experiment/run?exprId=${exprId}`;
    return RestAPICaller.get(url);
  }

  static deleteExpr(exprId) {
    const url = `/v1/experiment/delete`;
    const postData = { exprId };

    return RestAPICaller.post(url, postData);
  }

  static createExpr(data) {
    const url = `/v1/experiment/create`;
    const postData = {
      exprNm: data.exprNm,
      exprDesc: data.exprDesc,
      exprTag: data.exprTag,
      wkspcId: data.wkspcId,
      modelFileNm: data.modelFileNm,
      modelFilePath: data.modelFilePath
    };

    return RestAPICaller.post(url, postData);
  }

  static createExprLearn(exprId, athmId, localDataId) {
    const url = `/v1/experiment/learn/create`;
    const postData = { exprId, athmId, localDataId };

    return RestAPICaller.post(url, postData);
  }

  static updateExpr(data) {
    const url = `/v1/experiment/update`;
    const postData = {
      exprId: data.exprId,
      exprNm: data.exprNm,
      exprDesc: data.exprDesc,
      exprTag: data.exprTag,
      exprSts: data.exprSts,
      modelFileNm: data.modelFileNm,
      modelFilePath: data.modelFilePath
    };

    return RestAPICaller.post(url, postData);
  }

  static updateLearnExpr(data) {
    const url = `/v1/experiment/learn/update`;
    const postData = {
      learnId: data.learnId,
      learnSts: data.learnSts
    };

    return RestAPICaller.post(url, postData);
  }

  static exprLearnRun(learnId, athmId, localDataId) {
    const url = `/v1/experiment/learn/run`;
    const postData = { learnId, athmId, localDataId };
    return RestAPICaller.post(url, postData);
  }

  static exprLearnCancel(learnId) {
    const url = `/v1/experiment/learn/cancel?learnId=${learnId}`;
    return RestAPICaller.get(url);
  }

  static exprLearnSaveModel(learnId) {
    const url = `/v1/experiment/learn/model?learnId=${learnId}`;
    return RestAPICaller.get(url);
  }

  static exprLearnShowLog(learnId) {
    const url = `/v1/experiment/learn/log?learnId=${learnId}`;
    return RestAPICaller.get(url);
  }

  static getPortMap(wkspcId, portTp) {
    const url = `/v1/workspace/portmap/list?wkspcId=${wkspcId}&portTp=${portTp}`;
    return RestAPICaller.get(url);
  }
}
