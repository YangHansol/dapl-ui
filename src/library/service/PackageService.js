import RestAPICaller from "../../RestAPICaller";

export default class PackageService {
  static getPackageList(pkgNm, number, size, libTp) {
    const url = `/v1/package/list?number=${number}&size=${size}&pkgNm=${pkgNm}&libTp=${libTp}`;
    return RestAPICaller.get(url);
  }

  static installPackage(data) {
    const url = `/v1/package/install`;
    const postData = {
      wkspcId: data.wkspcId,
      pkgId: data.pkgId
    };
    return RestAPICaller.post(url, postData);
  }

  static packageUpload(params) {
    const url = `/v1/package/create`;
    return RestAPICaller.fileUpload(url, params);
  }

  static packageUpdate(libTp, pkgNm, pkgVer) {
    const url = `/v1/library/update/apply?libTp=${libTp}&pkgNm=${pkgNm}&pkgVer=${pkgVer}`;
    return RestAPICaller.get(url);
  }

  static showPackageList(number, size, libTp) {
    const url = `/v1/package/list?number=${number}&size=${size}&sort=regDt.desc&libTp=${libTp}`;
    return RestAPICaller.get(url);
  }
}
