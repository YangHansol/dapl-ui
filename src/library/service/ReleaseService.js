import RestAPICaller from "../../RestAPICaller";

export default class ReleaseService {
  static createRelease(data) {
    const url = `/v1/release/create`;
    const postData = {
      rlsNm: data.rlsNm,
      learnId: data.learnId,
      reqMsg: data.reqMsg,
      rlsServerId: data.rlsServerId ? data.rlsServerId : null,
      inDataTp: data.inDataTp ? data.inDataTp : null,
      outDataTp: data.outDataTp ? data.outDataTp : null
    };
    return RestAPICaller.post(url, postData);
  }

  static getReleaseList(number, size, param) {
    const url = `/v1/release/list?number=${number}&size=${size}`;
    return RestAPICaller.get(url, param);
  }

  static getReleaseDetail(rlsId) {
    const url = `/v1/release/read?rlsId=${rlsId}`;
    return RestAPICaller.get(url);
  }

  static cancelRelease(rlsId) {
    const url = `/v1/release/cancel?rlsId=${rlsId}`;
    return RestAPICaller.get(url);
  }

  static deleteRelease(rlsId) {
    const url = `/v1/release/delete`;
    const postData = { rlsId };
    return RestAPICaller.post(url, postData);
  }

  static runRelease(rlsId, restPort, instanceCnt) {
    const url = `/v1/release/run`;
    const postData = { rlsId, instanceCnt };
    return RestAPICaller.post(url, postData);
  }

  static updateRelease(data) {
    const url = `/v1/release/update`;
    const postData = {
      rlsId: data.rlsId,
      rlsSts: data.rlsSts
    };
    return RestAPICaller.post(url, postData);
  }

  static testRelease(rlsId, inputs) {
    const url = `/v1/release/test`;
    const postData = {
      rlsId,
      inputs
    };
    return RestAPICaller.post(url, postData);
  }
}
