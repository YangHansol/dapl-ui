import RestAPICaller from "../../RestAPICaller";

export default class DatasetService {
  static getDatasetList(page, size, params) {
    const url = `/v1/dataset/list?number=${page}&size=${size}`;
    return RestAPICaller.get(url, params);
  }

  static getDatasetMoreList(page, size, params) {
    const url = `/v1/dataset/list?number=${page}&size=${size}`;
    let paramsData = {};
    paramsData = params ? params : null;
    return RestAPICaller.get(url, paramsData);
  }

  static getLocalDatasetList(page, size, wkspcId) {
    const url = `/v1/dataset/local/list?number=${page}&size=${size}&wkspcId=${wkspcId}`;
    return RestAPICaller.get(url);
  }

  static getDownloadLocalData(dataId, wkspcId) {
    const url = `/v1/dataset/local/download?dataId=${dataId}&wkspcId=${wkspcId}`;
    return RestAPICaller.get(url);
  }

  static getViewLocalData(localDataId) {
    const url = `/v1/dataset/local/view?localDataId=${localDataId}`;
    return RestAPICaller.get(url);
  }

  static getReadLocalData(localDataId) {
    const url = `/v1/dataset/local/read?localDataId=${localDataId}`;
    return RestAPICaller.get(url);
  }

  static uploadLocalData(data) {
    const url = `/v1/dataset/local/upload`;
    const postData = {
      fileNm: data.fileNm,
      dataNm: data.dataNm,
      dataDesc: data.dataDesc,
      pubFl: data.pubFl,
      wkspcId: data.wkspcId
    };

    return RestAPICaller.post(url, postData);
  }

  static registDatasetSQL(data) {
    const url = `/v1/dataset/create`;
    const postData = {
      dataNm: data.dataNm,
      dataDesc: data.dataDesc,
      dataTp: data.dataTp,
      queryStr: data.queryStr,
      queryDbTp: data.queryDbTp,
      fileNm: data.fileNm,
      pubFl: data.pubFl
    };

    return RestAPICaller.post(url, postData);
  }

  static datasetUpdate(data) {
    const url = `/v1/dataset/update`;
    return RestAPICaller.post(url, data);
  }

  static runQuery(dataId) {
    const url = `/v1/dataset/query`;
    const postData = {
      dataId
    };

    return RestAPICaller.post(url, postData);
  }

  static deleteDataset(dataId) {
    const url = `/v1/dataset/delete`;
    const postData = {
      dataId
    };

    return RestAPICaller.post(url, postData);
  }

  static deleteLocalDataset(localDataId) {
    const url = `/v1/dataset/local/delete`;
    const postData = {
      localDataId
    };

    return RestAPICaller.post(url, postData);
  }

  static getDatasetRead(dataId) {
    const url = `/v1/dataset/read?dataId=${dataId}`;
    return RestAPICaller.get(url);
  }

  static getDatasetView(dataId) {
    const url = `/v1/dataset/view?dataId=${dataId}&size=10`;
    return RestAPICaller.get(url);
  }

  static fileUpload(params) {
    const url = `/v1/dataset/upload`;
    return RestAPICaller.post(url, params);
  }

  static getLocalDatasetFileList(wkspcId) {
    const url = `/v1/dataset/local/filelist?wkspcId=${wkspcId}`;
    return RestAPICaller.get(url);
  }
}
