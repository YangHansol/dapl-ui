import RestAPICaller from "../../RestAPICaller";

export default class MemberService {
  static getGroupList(page, size, params) {
    const url = `/v1/group/list?number=${page}&size=${size}`;
    return RestAPICaller.get(url, params);
  }

  static createGroup(postData) {
    const url = `/v1/group/create`;
    return RestAPICaller.post(url, postData);
  }

  static updateGroup(postData) {
    const url = `/v1/group/update`;
    return RestAPICaller.post(url, postData);
  }

  static deleteGroup(groupId) {
    const url = `/v1/group/delete`;
    const postData = {
      groupId: groupId
    };

    return RestAPICaller.post(url, postData);
  }
}
