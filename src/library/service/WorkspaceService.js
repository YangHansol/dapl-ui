import RestAPICaller from "../../RestAPICaller";

// 새 워크스페이스 생성시 전달해야 할 데이터
// {
//   "wkspcNm": "aaaa",
//   "wkspcUseTp": "LRN",
//   "wkspcSzTp": "MIC",
//   "libTp": "TSF",
//   "asgndCpuNum": 1,
//   "asgndGpuNum": 0,
//   "asgndDisSz": 50,
//   "asgndMemSz": 8,
//   "wkspcDesc": "테스트",
//   "exptStDt": "20180302",
//   "exptEdDt": "20180303",
//   "tsbdYn": "Y"
// }

export default class WorkspaceService {
  // 새 워크스페이스 생성 API
  static createPostData(data) {
    const startDate = data.exptStDt.split("-").join("");
    const endDate = data.exptEdDt.split("-").join("");
    return {
      wkspcNm: data.wkspcNm,
      wkspcUseTp: data.wkspcUseTp,
      wkspcSzTp: data.wkspcSzTp,
      imgId: data.imgId,
      libTp: data.libTp,
      asgndCpuNum: data.asgndCpuNum,
      asgndGpuNum: data.asgndGpuNum,
      asgndDisSz: data.asgndDisSz,
      asgndMemSz: data.asgndMemSz,
      wkspcDesc: data.wkspcDesc,
      exptStDt: startDate,
      exptEdDt: endDate,
      tsbdYn: data.tsbdYn
    };
  }

  // 워크스페이스 수정 API
  static updatePostData(data) {
    const startDate = data.exptStDt.split("-").join("");
    const endDate = data.exptEdDt.split("-").join("");
    return {
      wkspcId: data.wkspcId,
      wkspcNm: data.wkspcNm,
      wkspcUseTp: data.wkspcUseTp,
      wkspcSzTp: data.wkspcSzTp,
      workSts: data.workSts,
      imgId: data.imgId,
      libTp: data.libTp,
      asgndCpuNum: data.asgndCpuNum,
      asgndGpuNum: data.asgndGpuNum,
      asgndDisSz: data.asgndDisSz,
      asgndMemSz: data.asgndMemSz,
      upldDataSz: data.upldDataSz,
      wkspcDesc: data.wkspcDesc,
      exptStDt: startDate,
      exptEdDt: endDate,
      delFl: data.delFl,
      tsbdYn: data.tsbdYn
    };
  }

  static getDockerImg() {
    const url = `/v1/docker/lib`;
    return RestAPICaller.get(url);
  }

  static getWorkspaceList(number, size, params) {
    const url = `/v1/workspace/list?number=${number}&size=${size}`;
    return RestAPICaller.get(url, params);
  }

  static getWorkspaceListWithOwnerId(number, size, ownerId) {
    const url = `/v1/workspace/list?number=${number}&size=${size}&ownerId=${ownerId}`;
    return RestAPICaller.get(url);
  }

  static getWorkspaceDetail(wkspcId) {
    const url = `/v1/workspace/detail?wkspcId=${wkspcId}`;
    return RestAPICaller.get(url);
  }

  static wkspcActivate(postData) {
    const url = `/v1/workspace/activate`;
    return RestAPICaller.post(url, postData);
  }

  static deleteWorkspace(wkspcId) {
    const url = `/v1/workspace/delete`;
    const postData = { wkspcId };
    return RestAPICaller.post(url, postData);
  }

  static createWorkspace(data) {
    const url = `/v1/workspace/create`;
    let postData = WorkspaceService.createPostData(data);
    return RestAPICaller.post(url, postData);
  }

  static updateWorkspace(data) {
    const url = `/v1/workspace/update`;
    const postData = { ...data };
    return RestAPICaller.post(url, postData);
  }

  static createShareWorkspace(wkspcId, userId) {
    const url = `/v1/workspace/share/create`;
    const postData = {
      wkspcId,
      userId
    };
    return RestAPICaller.post(url, postData);
  }

  static updateShareWorkspace(data) {
    const url = `/v1/workspace/share/update`;
    return RestAPICaller.post(url, data);
  }

  static getWorkspaceShareList(wkspcId) {
    const url = `/v1/workspace/share/list?wkspcId=${wkspcId}`;
    return RestAPICaller.get(url);
  }

  static getWorkspaceExprList(wkspcId, number, size) {
    const url = `/v1/experiment/list?number=${number}&size=${size}&wkspcId=${wkspcId.toString()}`;
    return RestAPICaller.get(url);
  }

  static getPortMap(wkspcId) {
    const url = `/v1/workspace/portmap/list?&wkspcId=${wkspcId}`;
    return RestAPICaller.get(url);
  }

  static createExpr(data) {
    const url = `/v1/experiment/create`;
    const postData = {
      exprNm: data.exprNm,
      exprDesc: data.exprDesc,
      exprTag: data.exprTag,
      wkspcId: data.wkspcId
    };
    return RestAPICaller.post(url, postData);
  }

  static customRelease(data) {
    const url = `/v1/release/custom`;
    const postData = {
      rlsNm: data.rlsNm,
      reqMsg: data.reqMsg,
      modelFlNm: data.modelFlNm,
      wkspcId: data.wkspcId
    };
    return RestAPICaller.post(url, postData);
  }

  static readWorkspace(wkspcId) {
    const url = `/v1/workspace/read?wkspcId=${wkspcId}`;
    return RestAPICaller.get(url);
  }

  static restoreWorkspace(wkspcId) {
    const url = `/v1/workspace/restore`;
    const postData = {
      wkspcId
    };

    return RestAPICaller.post(url, postData);
  }

  static backupWorkspace(wkspcId) {
    const url = `/v1/workspace/backup`;
    const postData = {
      wkspcId
    };

    return RestAPICaller.post(url, postData);
  }
}
