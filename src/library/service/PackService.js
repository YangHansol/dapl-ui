import RestAPICaller from "../../RestAPICaller";

export default class PackService {
  static getPackList(params) {
    const url = `/v1/package/mst/list`;
    return RestAPICaller.get(url, params);
  }

  static createPack(postData) {
    const url = `/v1/package/mst/create`;
    return RestAPICaller.post(url, postData);
  }

  static updatePack(postData) {
    const url = `/v1/package/mst/update`;
    return RestAPICaller.post(url, postData);
  }

  static deletePack(packId) {
    const url = `/v1/package/mst/delete`;
    const postData = {
      packId
    };
    return RestAPICaller.post(url, postData);
  }

  static getPackRead(packId) {
    const url = `/v1/package/mst/read?packId=${packId}`;
    return RestAPICaller.get(url);
  }
}
