import React, { Component, Fragment } from "react";
import { Router, Route } from "react-router-dom";
import { parseQueryString } from "../queryrouter/parseQueryString";
import PropTypes from "prop-types";
import GlobalStyle from "../../common/style/GlobalStyle";

class RouterConnection extends Component {
  render() {
    const Component = this.props.component;
    return (
      <Router history={this.props.history} basename={this.props.basename}>
        <Route
          render={props => {
            const newProps = {
              ...props,
              location: {
                ...props.location,
                query: parseQueryString(props.location.search),
                prevLocation: this.prevLocation
              }
            };
            this.prevLocation = {
              ...props.location,
              query: parseQueryString(props.location.search)
            };
            return (
              <Fragment>
                <GlobalStyle />
                <Component {...newProps} />
              </Fragment>
            );
          }}
        />
      </Router>
    );
  }
}

RouterConnection.propTypes = {
  basename: PropTypes.string,
  history: PropTypes.object.isRequired
};

RouterConnection.defaultProps = {
  basename: ""
};

export default RouterConnection;
