import axios from "axios";

function getAPIHost(category = null) {
  if (process.env.NODE_ENV === "development") {
    if (category === "common") {
      // return "http://192.168.18.51:8089/common";
      return "http://192.168.18.54:8099/common";
    } else {
      // return "http://192.168.18.51:8089/admin";
      return "http://192.168.18.54:8099/admin";
    }
  } else {
    if (category === "common") {
      return "/common";
    } else {
      return "/admin";
    }
  }
}

axios.defaults.xsrfHeaderName = "tkn";
axios.defaults.headers.common["tkn"] = sessionStorage.getItem("tkn");

export function getMakeURL(url, category = null) {
  if (url.indexOf("http") === 0) {
    return url;
  } else {
    if (url.substr(0, 1) === "/") {
      return getAPIHost(category).concat(url);
    } else {
      return getAPIHost(category).concat("/", url);
    }
  }
}

class RestAPICaller {
  static checkURL(url) {
    if (
      url.indexOf("/null/") !== -1 ||
      url.indexOf("/undefined/") !== -1 ||
      url.indexOf("/NaN/") !== -1
    ) {
      return false;
    }
    return true;
  }

  static get(url, params = null, category = null) {
    const fullUrl = getMakeURL(url, category);
    if (RestAPICaller.checkURL(url) === false) {
      throw Error("null parameter exception : " + fullUrl);
    }
    const fetchParams =
      params !== null ? { params: { ...params } } : { params: {} };

    return axios(fullUrl, fetchParams)
      .then(response => {
        return response;
      })
      .catch(e => {
        throw e;
      });
  }

  static post(url, params = null) {
    const fullUrl = getMakeURL(url);
    if (RestAPICaller.checkURL(url) === false) {
      throw Error("null parameter exception : " + fullUrl);
    }
    const fetchParams = params !== null ? params : {};

    return axios.post(fullUrl, fetchParams);
  }

  static fileUpload(url, params = null) {
    const fullUrl = getMakeURL(url);
    if (RestAPICaller.checkURL(url) === false) {
      throw Error("null parameter exception : " + fullUrl);
    }
    const fetchParams = params !== null ? params : {};
    const options = {
      headers: { "Content-type": "application/json" },
    };
    return axios.post(fullUrl, fetchParams, options);
  }
}

export default RestAPICaller;
