class LoadingGroup {
  static setLoading(fetchesInPrograss = 0) {
    this.fetchesInPrograss = fetchesInPrograss;
    return this.fetchesInPrograss > 0;
  }
  static startcall() {
    return LoadingGroup.setLoading(this.fetchesInPrograss + 1);
  }
  static completeCall() {
    if (this.fetchesInPrograss === 0) {
      throw new Error("호출중입니다.")
    }
    return LoadingGroup.setLoading(this.fetchesInPrograss - 1);
  }
}

export default LoadingGroup;
