import { ActionTypes as types } from "./LoadingAction"
import LoadingGroup from "./LoadingGroup"

const initialState = {
  loading: LoadingGroup.setLoading()
}

export default function LoadingReducer(state = initialState, action) {
  switch (action.type) {
    case types.START_CALL:
      return Object.assign({}, state, {
        loading: LoadingGroup.startcall()
      })
    case types.COMPLETE_CALL:
      return Object.assign({}, state, {
        loading: LoadingGroup.completeCall()
      })
    default:
      return state;
  }
}
