export const ActionTypes = Object.freeze({
  START_CALL: "START_CALL",
  COMPLETE_CALL: "COMPLETE_CALL"
});

export function startCall() {
  return {
    type: ActionTypes.START_CALL
  };
}
export function completeCall() {
  return {
    type: ActionTypes.COMPLETE_CALL
  };
}
