function etcMessage(msg) {
  if (msg) {
    return msg;
  } else {
    return "서버에 오류가 발생하였습니다.<br/>다시 시도해 주세요.";
  }
}

export default function httpErrorMessage(err, message) {
  if (process.env.NODE_ENV !== "production") {
    console.log(err);
  } else {
    console.log(err);
  }

  try {
    if (err.response.status >= 400) {
      let resultMessage = "";
      if (err.response.data && err.response.data.message) {
        resultMessage = err.response.data.message;
      }
      return etcMessage(resultMessage);
    }
  } catch (e) {
    return etcMessage(message);
  }

  return etcMessage(message);
}
