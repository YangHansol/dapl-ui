let config = {
  timeout: 5000
  // debug : false,
  // caching: false,
  // offlineMode : false,
  // healthCheckTime : 10000,
  // hostName: null,
  // hostName: 'https://server.local.danbi'
};

function getAPIHost() {
  if (process.env.NODE_ENV === "development") {
    if (config.hostName) {
      return config.hostName;
    }
    return "https://randomuser.me";
  } else {
    return "";
  }
}

function getMakeURL(url) {
  if (url.indexOf("http") === 0) {
    return url;
  } else {
    if (url.substr(0, 1) === "/") {
      return getAPIHost().concat(url);
    } else {
      return getAPIHost().concat("/", url);
    }
  }
}

class FetchApi {
  static checkURL(url) {
    if (
      url.indexOf("/null/") !== -1 ||
      url.indexOf("/undefined/") !== -1 ||
      url.indexOf("/NaN/") !== -1
    ) {
      return false;
    }
    return true;
  }

  static get(url, params = null) {
    const fullUrl = getMakeURL(url);
    if (FetchApi.checkURL(url) === false) {
      throw Error("null parameter exception : " + fullUrl);
    }
    const fetchParams =
      params !== null ? { params: { ...params } } : { params: {} };

    return Promise.race([
      fetch(fullUrl, fetchParams),
      new Promise(function(resolve, reject) {
        setTimeout(() => reject(new Error("request timeout")), config.timeout);
      })
    ])
      .then(response => {
        return response.json();
      })
      .catch(error => console.log(error));
  }

  static post(url, params = null) {
    const fullUrl = getMakeURL(url);
    if (FetchApi.checkURL(url) === false) {
      throw Error("null parameter exception : " + fullUrl);
    }
    const fetchParams = params !== null ? params : {};

    const options = {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(fetchParams)
    };

    return Promise.race([
      fetch(fullUrl, options),
      new Promise(function(resolve, reject) {
        setTimeout(() => reject(new Error("request timeout")), config.timeout);
      })
    ]).then(response => {
      return response.json();
    });
  }
}

export default FetchApi;
