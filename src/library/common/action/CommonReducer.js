import { ActionTypes as types } from "./CommonAction";

const initialState = {
  libSmListInfo: {},
  commonCodeListInfo: {},
  board: {
    content: []
  },
  experiment: {
    content: []
  },
  user: {
    content: []
  },
  package: {
    content: []
  },
  workspace: {
    content: []
  },
  dataset: {
    content: []
  },
  algorithm: {
    content: []
  },
  release: {
    content: []
  },
  docker: {
    content: []
  },
  group: {
    content: []
  },
  resourceList: {},
  workerServerList: []
};

export default function CommonReducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_COMMON_LIST:
      return Object.assign({}, state, {
        [action.payload.category]: action.payload.commonList
      });

    case types.GET_COMMON_CODE:
      return Object.assign({}, state, {
        commonCodeListInfo: action.payload.commonCodeListInfo
      });

    case types.GET_LIBRARY_SM_LIST:
      return Object.assign({}, state, {
        libSmListInfo: action.payload.libSmListInfo
      });

    case types.GET_RESOURCE_LIST:
      return Object.assign({}, state, {
        resourceList: action.payload.resourceList
      });

    case types.GET_CATEGORY_LIST:
      return Object.assign({}, state, {
        [action.payload.category]: action.payload.categoryList
      });

    case types.GET_MONITOR_WORKER:
      return Object.assign({}, state, {
        workerServerList: action.payload.workerServerList
      });

    default:
      return state;
  }
}
