import CommonService from "../../library/service/CommonService";
import * as ErrorAction from "../../error/ErrorAction";

export const ActionTypes = Object.freeze({
  INIT_ACTION: "INIT_ACTION",
  GET_LIBRARY_SM_LIST: "GET_LIBRARY_SM_LIST",
  GET_COMMON_CODE: "GET_COMMON_CODE",
  GET_COMMON_LIST: "GET_COMMON_LIST",

  GET_CATEGORY_LIST: "GET_CATEGORY_LIST",
  GET_RESOURCE_LIST: "GET_RESOURCE_LIST",
  GET_MONITOR_WORKER: "GET_MONITOR_WORKER",
});

function generateAPIQuery(params) {
  let query = { ...params };
  for (let key of Object.keys(query)) {
    if (Array.isArray(query[key] === true)) {
      query[key] = query[key].toString();
    } else if (query[key] === "") {
      delete query[key];
    }
  }
  return query;
}

export function initAction() {
  return {
    type: ActionTypes.INIT_ACTION,
  };
}

export function getCommonList(category, pageNo, size, query) {
  return (dispatch, getState) => {
    let number;
    if (pageNo === null) {
      number = 0;
    } else {
      number = pageNo;
    }

    let _query = {};
    if (query) {
      _query = { number, size, ...query };
    } else {
      _query = { number, size };
    }

    if (_query.pagename) {
      delete _query.pagename;
    }
    return CommonService.getCommonList(category, generateAPIQuery(_query))
      .then(results => {
        dispatch({
          type: ActionTypes.GET_COMMON_LIST,
          payload: {
            commonList: results.data,
            category,
          },
        });
      })
      .catch(e => {
        dispatch(ErrorAction.Error(e));
      });
  };
}

export function getCommonCode(grpCd) {
  return (dispatch, getState) => {
    return CommonService.getCommonCode(grpCd)
      .then(results => {
        dispatch({
          type: ActionTypes.GET_COMMON_CODE,
          payload: {
            commonCodeListInfo: results.data,
          },
        });
      })
      .catch(e => {
        dispatch(ErrorAction.Error(e));
      });
  };
}

export function getLibSmList(libLrgCd) {
  return (dispatch, getState) => {
    return CommonService.getLibSmList(libLrgCd)
      .then(results => {
        dispatch({
          type: ActionTypes.GET_LIBRARY_SM_LIST,
          payload: {
            libSmListInfo: results.data,
          },
        });
      })
      .catch(e => {
        dispatch(ErrorAction.Error(e));
      });
  };
}

export function getResourceList() {
  return (dispatch, getState) => {
    return CommonService.getResourceList()
      .then(results => {
        dispatch({
          type: ActionTypes.GET_RESOURCE_LIST,
          payload: {
            resourceList: results.data,
          },
        });
      })
      .catch(e => {
        dispatch(ErrorAction.Error(e));
      });
  };
}

export function getCategoryList(category) {
  let params = {
    number: 0,
    size: 999,
    sort: "userLv.asc",
  };

  return (dispatch, getState) => {
    return CommonService.getCommonList(category, params)
      .then(results => {
        dispatch({
          type: ActionTypes.GET_CATEGORY_LIST,
          payload: {
            categoryList: results.data,
            category,
          },
        });
      })
      .catch(e => {
        dispatch(ErrorAction.Error(e));
      });
  };
}

export function getMonitorWorker() {
  return (dispatch, getState) => {
    return CommonService.getMonitorWorker()
      .then(results => {
        dispatch({
          type: ActionTypes.GET_MONITOR_WORKER,
          payload: {
            workerServerList: results.data,
          },
        });
      })
      .catch(e => {
        dispatch(ErrorAction.Error(e));
      });
  };
}
