import React from "react";
import { LayerPopup } from "library/layerpopup";
import ModalContainer from "./components/layers/ModalContainer";
import LayerContainer from "./components/layers/LayerContainer";
import ConfirmModal from "./components/layers/ConfirmModal";
import NoticeModal from "./components/layers/NoticeModal";

export default class Popup {
  static layer(LayerPageComponent) {
    return LayerPopup.show(
      <LayerContainer>{LayerPageComponent}</LayerContainer>,
    );
  }
  static modal(LayerPageComponent) {
    return LayerPopup.show(
      <LayerContainer>{LayerPageComponent}</LayerContainer>,
    );
  }
  static confirm(props) {
    return LayerPopup.show(
      <ModalContainer>
        <ConfirmModal {...props} />
      </ModalContainer>,
    );
  }
  static notice(props) {
    return LayerPopup.show(
      <ModalContainer>
        <NoticeModal {...props} />
      </ModalContainer>,
    );
  }
  static hide(layerKey) {
    if (layerKey) {
      LayerPopup.hide(layerKey);
    }
  }
}
