import React from "react";
import { SpinnerWrapper } from "./style/StyleComponents";
const Spinner = ({ size = false, operation, customMargin = false }) => {
  return (
    <SpinnerWrapper
      size={size}
      operation={operation}
      customMargin={customMargin}
    >
      <div className="sk-circle sk-circle1"></div>
      <div className="sk-circle sk-circle2"></div>
      <div className="sk-circle sk-circle3"></div>
      <div className="sk-circle sk-circle4"></div>
      <div className="sk-circle sk-circle5"></div>
      <div className="sk-circle sk-circle6"></div>
      <div className="sk-circle sk-circle7"></div>
      <div className="sk-circle sk-circle8"></div>
      <div className="sk-circle sk-circle9"></div>
      <div className="sk-circle sk-circle10"></div>
      <div className="sk-circle sk-circle11"></div>
      <div className="sk-circle sk-circle12"></div>
    </SpinnerWrapper>
  );
};

export default Spinner;
