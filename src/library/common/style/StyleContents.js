import styled from "styled-components";
import { mixin } from "./mixin";
import { Variables } from "./Variables";

import LogoMain from "../../images/logo.png";

const ContentsWrap = styled.div`
  section.contents {
    min-width: 1280px;
    min-height: 100vh;
    height: 100%;
    padding-left: 250px;

    .contents-header {
      position: relative;
      ${mixin.flexBox};
      ${mixin.alignItems("center")};
      ${mixin.justifyContent("space-between")};
      height: 80px;
      padding: 0 50px;
      background-color: ${Variables.color.lightGray};
      border-bottom: 1px solid #e3e7ec;
      .logo {
        ${mixin.flexBox};
        ${mixin.alignItems("center")};
      }
      .logo-title {
        padding-left: 60px;
        font-size: 12px;
        color: #17365b;
        font-weight: ${Variables.fontSize.regular};
        &:before {
          position: absolute;
          top: 50%;
          left: 50px;
          display: block;
          width: 44px;
          height: 44px;
          content: "";
          background: url(${LogoMain}) no-repeat;
          background-size: cover;
          ${mixin.transform("translateY(-50%)")};
          background-position: 100% 50%;
          background-size: contain;
        }
      }
    }

    .contents-body {
      height: 100%;
      padding: 0 50px;
    }
  }

  .page-header {
    ${mixin.flexBox};
    ${mixin.alignItems("center")};
    ${mixin.justifyContent("space-between")};
    padding: 35px 0;
    border-bottom: 1px solid ${Variables.color.lightGray};
    .page-title {
      font-size: 14px;
      font-weight: ${Variables.fontSize.medium};
    }

    .button-container {
      button {
        margin-right: 10px;
      }
    }
  }

  .page-body {
    margin-top: 26px;
    margin-bottom: 41px;
  }

  .search-container {
    ${mixin.flexBox};
    ${mixin.alignItems("center")};

    .input-container {
      margin-right: 34px;

      .sel-type-1 {
        min-width: 415px;
      }
    }

    & + .table-container {
      margin-top: 25px;
    }

    .form-group {
      height: 32px;
      line-height: 32px;
      margin-bottom: 0;
      label {
        display: none;
      }
    }
  }

  .contents.main {
    background-color: ${Variables.color.lightGray};
    .page-header {
      padding: 27px 0 0px;
      margin-left: 30px;
    }
    .page-body {
      margin-top: 0;
      margin-bottom: 0;
      padding: 26px 50px 41px;
    }
  }

  .contents:not(.main) {
    .page-body .table-container {
      min-height: 45vh;
    }
  }

  .dashboard-column {
    width: 50%;
    &:first-child {
      margin-right: 50px;
    }

    @media (max-width: 1500px) {
      width: 100%;

      &:first-child {
        margin-right: 0;
      }
    }
  }
`;

export default ContentsWrap;
