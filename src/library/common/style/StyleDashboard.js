import styled from "styled-components";
import { mixin } from "./mixin";
import { Variables } from "./Variables";
import { Color } from "./Color";
import svgImages from "./svgImages";

export const DashboardWrap = styled.div`
  ${mixin.flexBox};

  @media (max-width: 1500px) {
    display: block;
  }

  .panel {
    ${mixin.flexShrink("0")};

    min-width: 550px;
    margin-bottom: 30px;
    padding: 30px;
    border-radius: 2px;
    background-color: ${Color.default.white};
    box-shadow: 0 0 10px 0 rgba(206, 206, 206, 0.2);
  }

  .panel-header {
    ${mixin.flexBox};
    ${mixin.alignItems("center")};
    ${mixin.justifyContent("space-between")};
    height: 24px;
    margin-bottom: 30px;

    .panel-title {
      font-size: 15px;
      font-weight: ${Variables.fontSize.medium};
      letter-spacing: -0.5px;
      color: #575757;
    }
  }

  /* workspace 아이콘 부분 */
  .workspace-list {
    ${mixin.flexBox};
    ${mixin.alignItems("center")};
    ${mixin.justifyContent("space-between")};
    margin-top: 10px;
    margin-bottom: 40px;

    & > .workspace-list-item {
      ${mixin.flexGrow("0")};
      width: 25%;
      margin-right: 8px;
      padding: 19px 12px;
      border: 1px solid #e3e7ec;
      border-radius: 3px;

      &:last-child {
        margin-right: 0;
      }

      .title {
        display: block;
        font-size: 10px;
        font-weight: ${Variables.fontSize.regular};
        letter-spacing: -0.4px;
        line-height: 1.2;
        min-height: 24px;
      }

      .block {
        ${mixin.flexBox};
        ${mixin.alignItems("center")};
        ${mixin.justifyContent("space-between")};
      }

      .count {
        max-width: calc(100% - 30px);
        ${mixin.ellipsis};
        font-size: 24px;
        font-weight: ${Variables.fontSize.medium};
      }

      [class^="ic-workspace-"] {
        display: block;
        width: 24px;
        height: 24px;
        background-repeat: no-repeat;
        background-position: 50%;
        background-size: 100%;
      }
    }
  }

  /* 프로그레스 바 */
  .progress {
    height: 6px;
    border-radius: 3px;
    background-color: ${Color.default.lightGray};
    overflow: auto;

    .progress-bar {
      height: 100%;
      border-radius: 3px;
      background-color: ${Color.default.purple};
    }
  }

  .size-bar {
    .size-text {
      display: block;
      margin-top: 14px;
      text-align: center;
      font-size: 12px;
      letter-spacing: -0.4px;
      .primary {
        font-size: 16px;
        font-weight: ${Variables.fontSize.medium};
      }
    }
  }

  /* 동그라미 스테이터스 영역 */
  .status-container {
    .status-list {
      position: relative;
      width: 100%;
      margin-bottom: 4px;
      overflow-x: auto;
    }

    .title {
      min-width: 43px;
      margin-right: 25px;
      color: #797979;
      font-size: 12px;
      letter-spacing: -0.4px;
    }

    .list {
      position: absolute;
      top: 50%;
      left: 70px;
      height: 10px;
      background-size: 15px 10px;
      transform: translateY(-50%);

      /* 대시보드에서 보여주는 스테이터스 동그라미 */
      ${Variables.statusColor.map(color => {
        return `
          &.status-${color.name} {
            background-image: ${svgImages(
              "status-circle-done",
              `${color.color}`
            )};
          }
        `;
      })}
    }

    & + .table-container {
      margin-top: 32px;
    }
  }
`;

export const MonitorPanel = {
  Wrap: styled.div`
    > h4 {
      font-size: 14px;
      margin-bottom: 30px;

      > em {
        color: #92278f;
        text-transform: uppercase;
      }
    }

    .table-container {
      > h5 {
        font-size: 14px;
        margin: 30px 0 20px;
      }
    }
  `,

  Bar: styled.div`
    > div {
      width: 100%;
      font-size: 13px;
      margin-right: 20px;

      &:last-child {
        margin-right: 0;
      }

      .remain-resource {
        display: block;
        font-size: 12px;
        text-align: right;
      }
    }

    .progress {
      margin: 10px 0 0 0;
      background-color: #f2f4f7;
    }
  `
};
