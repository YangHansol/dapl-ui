// SVG images
export default function svgImages(
  iconName,
  color = "000",
  strokeColor = "000",
  color2 = "000",
  strokeColor2 = "000",
  strokeWidth = 0,
  css = ""
) {
  let icons = {
    // 아래방향 작은 화살표 (드롭다운 사용)
    "arrow-bottom": `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24"><defs><path id="a" d="M7.063 0L8 .938l-4 4-4-4L.938 0 4 3.063z"/></defs><use fill="#${color}" fill-rule="evenodd" opacity=".8" transform="translate(8 10)" xlink:href="%23a"/></svg>`,

    // 왼쪽 작은 화살표 (페이지네이션 사용)
    "arrow-left": `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24"><defs><path id="a" d="M.938 0l4 4-4 4L0 7.062 3.063 4 0 .937z"/></defs><use fill="#${color}" fill-rule="evenodd" transform="matrix(-1 0 0 1 13.938 8)" xlink:href="%23a"/></svg>`,

    // 오른쪽 작은 화살표 (페이지네이션 사용)
    "arrow-right": `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24"><defs><path id="a" d="M.938 0l4 4-4 4L0 7.062 3.063 4 0 .937z"/></defs><use fill="#${color}" fill-rule="evenodd" transform="translate(11.063 8)" xlink:href="%23a"/></svg>`,

    // 오름차 화살표
    "arrow-sort-asc": `<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="10" height="10"><g fill="none" fill-rule="evenodd"><path d="M0 0h10v10H0z"/><path fill="#${color}" fill-rule="nonzero" d="M8.462 7.692L5.385 2.308 2.308 7.692z"/></g></svg>`,

    // 내림차 화살표
    "arrow-sort-desc": `<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="10" height="10"><g fill="none" fill-rule="evenodd"><path d="M0 0h10v10H0z"/><path fill="#${color}" fill-rule="nonzero" d="M8.462 2.308L5.385 7.692 2.308 2.308z"/></g></svg>`,

    // 드롭다운 화살표 - LNB 드롭다운 사용
    "dropdown-arrow": `<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="20" height="20"><path fill="#${color}" d="M14 9l-1-1-3 1-3-1-1 1 4 3z"/></svg>`,

    // 대시보드 아이콘 - 워크스페이스
    "workspace-mine": `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 20" version="1.1"><style>.st0{fill:none;stroke:#${color}}</style><path class="st0" d="M-79.5-42.5h23-23zM-74-34.5h12c.3 0 .5.2.5.5s-.2.5-.5.5h-12c-.3 0-.5-.2-.5-.5s.2-.5.5-.5zM-69.7-38.5l-1.6 4h6.5l-1.6-4h-3.3zM-78.5-53.5h21c.6 0 1 .4 1 1v13c0 .6-.4 1-1 1h-21c-.6 0-1-.4-1-1v-13c0-.6.4-1 1-1z" transform="translate(80 54)"/><path d="M-75.5-49.5h6v3h-6v-3zm8-2h7v7h-7v-7z" fill="#fff" stroke="#${color}" transform="translate(80 54)"/></svg>`,

    // 대시보드 아이콘 - 공유된 워크스페이스
    "workspace-shared": `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 20" version="1.1"><style>.st0,.st2{fill:none;stroke:#${color}}.st2{fill:#fff}</style><path class="st0" d="M.5 11.5h23-23zM6 19.5h12c.3 0 .5.2.5.5s-.2.5-.5.5H6c-.3 0-.5-.2-.5-.5s.2-.5.5-.5zM10.3 15.5l-1.6 4h6.5l-1.6-4h-3.3z"/><path class="st0" d="M1.5.5l21 .1c.6 0 1 .5 1 1v13c0 .6-.5 1-1 1l-21-.1c-.6 0-1-.5-1-1v-13c0-.6.5-1 1-1z"/><path d="M14.6 3.1L9 7.6l6 1" fill="none" stroke="#${color}" stroke-linecap="round" stroke-linejoin="round"/><ellipse transform="rotate(-89.801 14.016 3.574)" class="st2" cx="14" cy="3.6" rx="1" ry="1"/><ellipse transform="rotate(-89.801 14.998 8.577)" class="st2" cx="15" cy="8.6" rx="1" ry="1"/><ellipse transform="rotate(-89.801 9.002 7.556)" class="st2" cx="9" cy="7.6" rx="1" ry="1"/></svg>`,

    // 대시보드 내 스테이터스 동그라미
    "status-circle-done": `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 10" version="1.1"><circle cx="5" cy="5" r="5" fill="#${color}"/></svg>`
  };

  let encodeB64 = window.btoa(icons[iconName]);

  return `url('data:image/svg+xml;base64,${encodeB64}')`;
}
