import styled from "styled-components";
import BtnClose from "library/images/ic-close.png";

export const LayerM = {
  Wrapper: styled.div`
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    z-index: 99;
  `,

  Layer: styled.div`
    position: fixed;
    display: table;
    width: 100%;
    height: 100%;
    z-index: auto;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: ${props => {
      return props.zIndex;
    }};
  `,
};

export const LayerPopupStyle = {
  ModalWrap: styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    text-align: center;
    vertical-align: middle;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 1;
  `,

  Wrap: styled.div`
    display: table-cell;
    text-align: center;
    vertical-align: middle;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 1;
  `,

  Header: styled.div`
    position: relative;
    font-size: 25px;
    color: #373737;
    > .btn-modal-close {
      position: absolute;
      top: 15px;
      right: 15px;
      width: 24px;
      height: 24px;
      text-indent: -9999em;
      background: url(${BtnClose}) no-repeat;
      background-size: cover;
      background-position: 50% 50%;
    }
  `,

  Content: styled.div`
    min-width: 300px;
    width: 300px;
    max-width: 33.3%;
    margin: 0 auto;
    border-radius: 5px;
    background-color: #fff;
    > .modal-body {
      max-height: 600px;
      word-break: break-all;
      overflow-y: auto;
    }
  `,

  ModalContent: styled.div`
    width: ${props => {
      return props.width ? props.width : "500px";
    }};
    border-radius: 5px;
    background-color: #fff;
    > .modal-body {
      max-height: 725px;
      word-break: break-all;
      overflow-y: auto;
    }
  `,

  ModalBody: styled.div`
    text-align: left;
    max-height: ${props => {
      return props.Bheight ? props.Bheight : "700px";
    }};
    overflow-y: auto;
    margin-top: 20px;
    @media screen and (max-width: 1620px) {
      max-height: ${props => {
        return props.Bheight ? props.Bheight : "400px";
      }};
    }

    & > .msg-validate {
      padding: 0 15px;
      color: #92278f;
      font-size: 12px;
    }
  `,

  Footer: styled.div`
    padding: 15px;
    & button:nth-child(2) {
      margin-left: 10px;
    }
  `,

  SubText: styled.div`
    text-align: center;
    padding: 0 0 30px;
  `,

  Icon: styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 40px;
    height: 40px;
    color: rgb(53, 113, 214);
    margin: 0px auto 10px;
    border-radius: 20px;
    background: rgb(244, 244, 244);
  `,
};
