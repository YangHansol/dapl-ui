import styled from "styled-components";

export const CustomTable = {
  Thead: styled.thead`
    /* list형 table 또는 box형 table */
    ${props => {
      return props.type === "list"
        ? `
            tr th {
                position: relative;
                font-size: 13px;
                font-weight: 700;
                line-height: 1.5;
                color: #575757;
                padding: 16px 10px !important;
                border-bottom: 1px solid rgba(198,203,212,0.3) !important;
                background-color: #f2f4f7;
                white-space: nowrap;
                text-overflow: ellipsis;
                overflow: hidden;
            }
        `
        : `
            tr th {
                font-weight: 400;
                color: #fff;
                background-color: #777;
            }
        `;
    }}

    tr th.sort {
      span {
        cursor: pointer;
      }

      &.non-cursor > span {
        cursor: auto;
      }
    }
  `,

  Tbody: styled.tbody`
    ${props => {
      return props.type === "list"
        ? `
            tr td {
                position: relative;
                font-size: 13px;
                font-weight: 400;
                line-heith: 1.5;
                padding: 15px 10px !important;
                border-bottom: 1px solid rgba(198,203,212,0.3);
                background-color: #fff;
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                -webkit-letter-spacing: -0.5px;
                -moz-letter-spacing: -0.5px;
                -ms-letter-spacing: -0.5px;
                letter-spacing: -0.5px;
            }
        `
        : null;
    }}
  `,

  Header: styled.div`
    font-weight: 700;
    padding-bottom: 20px;

    & > h3 {
      display: inline-block;
      vertical-align: middle;
      margin: 0 !important;
    }

    & > button {
      margin-left: 10px;
    }
  `,

  Description: styled.td`
    max-width: ${props => (props.colwidth ? props.colwidth : "250px")};
  `,

  Ellipsis: styled.div`
    &,
    & > * {
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
    }
  `
};

export const EmptyBox = styled.div`
  text-align: center;
  margin-bottom: ${props => {
    return props.main ? "0px" : "15px";
  }};
  padding: 15px;
  background: #eee;
`;
