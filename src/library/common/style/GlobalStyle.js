import { createGlobalStyle } from "styled-components";
import { mixin } from "./mixin";
import { Variables } from "./Variables";

import WorkspaceMine from "../../images/ic-workspace-mine.png";
import WorkspaceShared from "../../images/ic-workspace-shared.png";
import WorkspaceCpu from "../../images/ic-workspace-cpu.png";
import WorkspaceGpu from "../../images/ic-workspace-gpu.png";
import WorkspaceActive from "../../images/ic-workspace-active.png";
import WorkspaceWait from "../../images/ic-workspace-wait.png";
import WorkspaceStop from "../../images/ic-workspace-stop.png";
import ArwHeader from "../../images/arw-header.png";
import ArwDropDown from "../../images/arw-dropdown.png";
import ArwLeft from "../../images/arw-left-on.png";
import ArwRight from "../../images/arw-right-on.png";

const GlobalStyle = createGlobalStyle`
/* Reset CSS */
html,
body,
div,
span,
h1,
h2,
h3,
h4,
h5,
h6,
p,
em,
img,
small article,
aside,
footer,
header,
nav,
address,
section,
main,
time,
ol,
ul,
li,
hr,
strong,
figure,
tr,
th,
td,
dl,
dt,
dd,
fieldset {
  margin: 0;
  padding: 0;
  border: 0;
  vertical-align: baseline;
  -webkit-text-size-adjust: none;
  font-style: normal;
}

*,
:before,
:after {
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

ul,
ol,
li,
dl {
  list-style: none;
}

a:link,
a:visited,
a:hover,
a:active {
  text-decoration: none;
  ${mixin.fontMain};
  color: inherit;
  outline: 0;
}

hr {
  display: none;
}

button {
  display: block;
  margin: 0;
  padding: 0;
  ${mixin.fontMain};
  border: 0;
  -webkit-appearance: none;
  -webkit-border-radius: 0;
  background: none;
  cursor: pointer;
  &:focus {
    outline: 0;
  }
}

i {
  font-style: normal;
}

a,
area,
button,
[role="button"],
input:not([type="range"]),
label,
select,
summary,
textarea {
  -ms-touch-action: manipulation;
  touch-action: manipulation;
  outline: 0;
}

select {
  cursor: pointer;
}

table {
  border-collapse: collapse;
}

caption {
  padding-top: 0.75rem;
  padding-bottom: 0.75rem;
  color: #868e96;
  text-align: left;
  caption-side: bottom;
}

th {
  text-align: inherit;
}

input {
  ${mixin.fontMain};
  ${mixin.ellipsis(1)};
  /* width: 100%; */
  vertical-align: top;
  margin: 0;
  padding: 0;
  border: 1px solid transparent;
  border-radius: 0;
  -webkit-border-radius: 0;
  box-sizing: border-box;
  background-color: transparent;
}

input[type=date]::-webkit-inner-spin-button,
input[type=date]::-webkit-outer-spin-button,
input[type=date]::-webkit-clear-button{
  display: none;
  margin: 0;
  -webkit-appearance: none;
}

html {
  position: relative;
  min-width: 100%;
  width: 100%;
  /* 1em = 10px로 설정 */
  font-size: 62.5%; 
  font-weight: ${Variables.fontSize.regular};
  line-height: 1.15;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%;
  overflow-x: auto;
  -ms-overflow-style: scrollbar;
  -webkit-tap-highlight-color: transparent;
}

body {
  position: relative;
  ${mixin.fontMain};
  font-weight: ${Variables.fontSize.regular};
  text-align: left;
  line-height: 1.5;
  color: #0c0c0c;
  background-color: #fff;
}

/* Common Css */
/* text-color */
.text-danger {
  color: #ff6c60;
}

.text-muted {
  color: #a1a1a1;
}

.text-primary {
  color: #59ace2;
}

.text-warning {
  color: #fcb322;
}

.text-success {
  color: #a9d86e;
}

.bg-danger {
  background-color: #ff6c60;
}

.bg-muted {
  background-color: #a1a1a1;
}

.bg-primary {
  background-color: #59ace2;
}

.bg-warning {
  background-color: #fcb322;
}

.bg-success {
  background-color: #a9d86e;
}

.margin-auto {
  margin: 0 auto;
}

.circle {
  display: inline-block;
  vertical-align: middle;
  width: 12px;
  height: 12px;
  margin-top: -3px;
  border-radius: 50%;

  &#stsText {
    width: 10px;
    height: 10px;
    margin-right: 5px;
  }
}

a.underline, button.underline {
  text-decoration: underline;
}

button.underline {
  width: 100%;
  text-align: left;
  overflow: hidden;
  text-overflow: ellipsis;
}

.primary,
.purple {
  color: ${Variables.color.purple};
}

.green {
  color: ${Variables.color.green};
}

.gray,
.inactive {
  color: #acacac;
}

.hidden {
  display: none;
}

/* Button Css */
/* button-container 내 버튼 병렬로 정리 */
.button-container, .flex-item {
  ${mixin.flexBox};
  ${mixin.alignItems("center")};
}

.flex-item {
  > input[type="file"] {
    width: 60%;
  }
  > span {
    width: calc(40% - 10px);
    margin-left: 10px;
  }
}

/* btn-basic- 에 대한 정의 */
button[class^="btn-basic-"] {
  color: #fff;
  min-height: 32px;
  padding: 4px 14px;
  font-size: 14px;
  border-radius: 2px;
  text-align: center;
  font-weight: ${Variables.fontSize.regular};
  border: 1px solid transparent;
}

/* btn-basic-white */
[class^="btn-basic-"][class*="white"] {
  color: #4d4d4d;
  background-color: #fff;
  border-color: ${Variables.color.grayBorder};
  &:hover {
    background-color: #f6f6f6;
  }
  &.btn-table {
    display: inline-block;
    line-height: 32px;
    vertical-align: middle;
    padding: 0 10px;
  }
}

/* btn-basic-primary */
[class^="btn-basic-"][class*="primary"] {
  border-color: ${Variables.color.purple};
  background-color: ${Variables.color.purple};
  &:hover {
    border-color: ${Variables.color.purpleHover};
    background-color: ${Variables.color.purpleHover};
  }
  &.btn-table {
    display: inline-block;
    line-height: 32px;
    vertical-align: middle;
    padding: 0 10px;
  }
}

/* btn-basic-gray */
[class^="btn-basic-"][class*="gray"] {
  color: #333333;
  border-color: ${Variables.color.lightGray};
  background-color: ${Variables.color.lightGray};
  &:hover {
    border-color: #e1e7ec;
    background-color: #e1e7ec;
  }
}

/* 테이블 내 sort버튼에 대한 정의 */
[class^="arrow-sort"] {
  position: relative;
  display: inline-block;
  vertical-align: middle;
  margin-left: 5px;
  &.arrow-sort-asc {
    border-left: 3px solid transparent;
    border-right: 3px solid transparent;
    border-top: 5px solid #666;
    
    &:hover {
      border-top: 5px solid #333;
    }
  }
  &.arrow-sort-desc {
    border-left: 3px solid transparent;
    border-right: 3px solid transparent;
    border-bottom: 5px solid #666;
    
    &:hover {
      border-bottom: 5px solid #333;
    }
  }
}

/* 양옆 모서리가 둥그런 버튼 */
.btn-radius {
  color: #4d4d4d;
  padding: 2px 12px;
  font-size: 12px;
  font-weight: ${Variables.fontSize.medium};
  border-radius: 12px;
  border: 1px solid ${Variables.color.grayBorder};
  &:hover {
    background-color: ${Variables.color.lightGray};
  }
}

.btn-toggle {
  position: absolute;
  top: 50%;
  left: 0;
  transform: translate(0, -50%);
  width: 60px;
  margin-left: 10px;

  input {
    display: none;
  }

  input:checked + div label > span {
    left: 45px;
    border: 1px solid #92278f;
  }

  input:checked + div label {
    text-align: left;
    background: #92278f;
  }

  & > div {
    position: relative;
    width: 100%;
    height: 30px;
  }

  label {
    display: block;
    width: 100%;
    height: 30px;
    line-height: 30px;
    text-align: right;
    font-size: 13px;
    font-weight: 400;
    color: #fff;
    margin-bottom: 0;
    padding: 0 8px;
    box-sizing: border-box;
    border-radius: 2px;
    background: #aaa;
    transition: all 0.3s ease;
    cursor: pointer;
  }

  label > span {
    position: absolute;
    top: 50%;
    left: 0;
    display: block;
    width: 15px;
    height: 28px;
    margin-top: -14px;
    border: 1px solid #aaa;
    border-radius: 2px;
    box-sizing: border-box;
    box-shadow: 0 1px 1px 0 rgba(0,0,0,0.24), 0 0 1px 0 rgba(0,0,0,0.12);
    transition: all 0.3 cubic-bezier(0.275, -0.45, 0.725, 1.45);
    background: #fff;
  }
}

/* Input Css */
.input-container {
  position: relative;
  ${mixin.flexBox};
  ${mixin.alignItems("center")};
  & > .title {
    display: block;
    ${mixin.flexShrink("0")};
    font-size: 14px;
    font-weight: ${Variables.fontSize.regular};
    margin-right: 10px;
  }
  input {
    width: 440px;
  }
  button[class^="btn-"] {
    min-width: 78px;
  }
}

.input-container.has-button {
  input, select {
    width: 284px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }
  button {
    border-radius: 0;
    border-left: 0;
    &:last-child {
      border-top-right-radius: 2px;
      border-bottom-right-radius: 2px;
    }
  }
}

.input-basic {
  height: 32px;
  padding: 0 12px;
  line-height: 32px;
  color: #333;
  font-size: 14px;
  font-weight: ${Variables.fontSize.regular};
  border-radius: 2px;
  border: 1px solid ${Variables.color.grayBorder};
  background-color: #fff;
}

/* Select Css */
.sel-type-2 {
  ${mixin.flexBox};
  ${mixin.justifyContent("space-between")};
  position: relative;
  cursor: pointer;
  
  &::after {
    display: inline-block;
    width: 20px;
    height: 20px;
    background: url(${ArwHeader}) no-repeat;
    background-size: cover;
    content: "";
  }
  .select-text {
    text-align: right;
    margin-right: 14px;
    font-size: 12px;
    font-weight: ${Variables.fontSize.regular};
    color: #17365b;
    cursor: pointer;
  }
  select {
    cursor: pointer;
    opacity: 0;
    position: absolute;
    top: 0px;
    right: 0;
    width: 100%;
  }
}

.sel-type-1 {
  position: relative;
  overflow: hidden;
  display: inline-block;
  width: 100%;
  min-width: 440px;
  height: 32px;
  line-height: 32px;
  color: #333;
  font-size: 14px;
  font-weight: ${Variables.fontSize.regular};
  border-radius: 2px;
  border: 1px solid ${Variables.color.grayBorder};
  background-color: #fff;
  
  &::after {
    display: block;
    position: absolute;
    top: 4px;
    right: 4px;
    width: 24px;
    height: 24px;
    background: url(${ArwDropDown}) no-repeat 50%;
    background-size: cover;
    content: "";
    ${mixin.pointerNone};
  }
  select {
    width: 100%;
    height: 100%;
    padding: 0 12px;
    color: inherit;
    font-size: 14px;
    border: none;
    border-radius: 0;
    box-shadow: none;
    background-color: transparent;
    background-image: none;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
  }
}

/* Pagination Css */
.pagination {
  display: block;
  margin: 25px auto 40px;
  text-align: center;
  li {
    display: inline-block;
    & > a {
      display: block;
      position: relative;
      width: auto;
      min-width: 32px;
      height: 32px;
      padding: 0 12px;
      margin-right: 4px;
      line-height: 30px;
      text-align: center;
      font-size: 14px;
      font-weight: ${Variables.fontSize.regular};
      color: #333;
      background-color: #fff;
      border-radius: 2px;
      border: 1px solid ${Variables.color.grayBorder};
      &:hover {
        background-color: #fbfbfb;
      }
    }
  }

  span[aria-label="Previous"] {
    display: block;
    width: 100%;
    height: 100%;
    background: url(${ArwLeft}) no-repeat 50%;
    background-size: 24px;
    color: transparent;
  }

  span[aria-label="Next"] {
    display: block;
    width: 100%;
    height: 100%;
    background: url(${ArwRight}) no-repeat 54%;
    background-size: 24px;
    color: transparent;
  }

  .active {
    & > a {
      color: #fff;
      border-color: ${Variables.color.purple};
      background-color: ${Variables.color.purple};
      &:hover {
        background-color: ${Variables.color.purple};
      }
    }
  }

  .disabled {
    & > a {
      border-color: #eceef1;
    }
    span:not(.ellipse) {
      opacity: 0.3;
    }
    &:hover {
      background-color: inherit;
    }
  }

  .ellipse {
    position: relative;
    top: 2px;
    min-width: auto;
    display: block;
    font-size: 14px;
    margin-left: 5px;
    margin-right: 10px;
    padding: 0;
    border: 0;
    color: #333;
  }
}

/* Table Css */
.table-basic {
  width: 100%;
  table-layout: fixed;

  thead th {
    position: relative;
    height: 51px;
    line-height: 51px;
    font-size: 13px;
    font-weight: ${Variables.fontSize.bold};
    color: #575757;
    background-color:${Variables.color.lightGray};
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    padding: 0 10px;
  }

  tbody td {
    position: relative;
    font-size: 13px;
    height: 51px;
    line-height: 51px;
    font-weight: ${Variables.fontSize.regular};
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    letter-spacing: -0.5px;
    padding: 0 10px;
    
    button {
      font-size: 13px;
    }
  }

  tbody tr {
    border-top: 1px solid rgba(198, 203, 212, 0.3);
    &:last-child {
      border-bottom: 1px solid rgba(198, 203, 212, 0.3);
    }
    &:hover {
      background-color: #f2f4f7;
    }
  }

  td.text-center {
    & > button {
      margin: 0 auto;
    }
  }

  td .button-container {
    display: inline-flex;
    margin: 0 auto;

    button {
      border-radius: 0;
      min-width: 50px;
    }

    button:first-child {
      border-top-left-radius: 2px;
      border-bottom-left-radius: 2px;
    }

    button:last-child {
      border-top-right-radius: 2px;
      border-bottom-right-radius: 2px;
    }
  }

  td .text-noData {
    text-align: center;
    font-weight: 400;
    letter-spacing: -0.5px;
    font-size: 13px;
    color: #000;
  }

  &.table-user-log {
    tbody td {
      height: auto;
      line-height: 1.5;
      word-break: break-all;
      padding: 15px 10px;
      overflow: visible;
      white-space: initial;
    }
  }
}

/* table-type2 */
.table-type2 {
  width: 100%;
  table-layout: fixed;

  thead th {
    position: relative;
    height: 26px;
    span {
      font-size: 12px;
      font-weight: ${Variables.fontSize.medium};
      padding: 0 7px;
      height: 18px;
      line-height: 18px;
      border-radius: 10px;
      background-color: ${Variables.color.lightGray};
    }
    &:last-child {
      padding-right: 0;
    }
  }
  tbody td {
    line-height: 36px;
    letter-spacing: -0.5px;
    font-size: 13px;
    font-weight: ${Variables.fontSize.regular};
    ${mixin.ellipsis(1)};
    padding-right: 7px;
    &:last-child {
      padding-right: 0;
    }
  }
}

.title-small {
  display: block;
  font-size: 12px;
  font-weight: ${Variables.fontSize.medium};
  letter-spacing: -0.4px;
  margin-bottom: 17px;
}

.ic-workspace-mine {
  background: url(${WorkspaceMine}) no-repeat;
  background-size: cover;
}

.ic-workspace-shared {
  background: url(${WorkspaceShared}) no-repeat;
  background-size: cover;
}

.ic-workspace-cpu {
  background: url(${WorkspaceCpu}) no-repeat;
  background-size: cover;
}

.ic-workspace-gpu {
  background: url(${WorkspaceGpu}) no-repeat;
  background-size: cover;
}

.ic-workspace-active {
  background: url(${WorkspaceActive}) no-repeat;
  background-size: cover;
}

.ic-workspace-wait {
  background: url(${WorkspaceWait}) no-repeat;
  background-size: cover;
}

.ic-workspace-stop {
  background: url(${WorkspaceStop}) no-repeat;
  background-size: cover;
}

/* 새 실험 구성 단계 Progress Bar */
.bs-wizard {margin-top: 40px;}
.bs-wizard > div:last-child .progress{background:none;}
/* Form Wizard */
.bs-wizard {border-bottom: 1px solid #e0e0e0; padding: 0 0 10px 0;}
.bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
.bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
.bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
.bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 18px; height: 18px; display: block; background: #92278f; top: 50%; left: 50%; margin-top: -9px; margin-left: -9px; border-radius: 50%;}

.bs-wizard > .bs-wizard-step > .progress {position: relative; left: 50%; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
.bs-wizard > .bs-wizard-step > .progress > .progress-bar {width: 0px; box-shadow: none; background: #92278f;}
.bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width: 100%;}

.bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width: 0%;}
.bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
.bs-wizard > .bs-wizard-step:first-child > .progress {left: 50%;}
.bs-wizard > .bs-wizard-step:last-child > .progress {width: 50%;}
.bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot {pointer-events: none;}

.btn-wkspc-detail {
  margin-top: 24px;
}

tr th.sort {
  span {
    cursor: pointer;

    span.mnmwB {
      display: inline;
    }
  }

  &.non-cursor > span {
    cursor: auto;
  }
}
`;

export default GlobalStyle;
