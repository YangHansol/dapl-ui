export const Color = {
  danger: {
    bgc: "#ff6c60",
    color: "#fff",
    borderColor: "#ff6c60",
    hoverBgColor: "#ec6459",
    hoverColor: "#fff"
  },

  noColor: {
    bgc: "#fff",
    color: "#4d4d4d",
    borderColor: "#c6cbd4",
    hoverBgColor: "#f6f6f6",
    hoverColor: "#4d4d4d"
  },

  warning: {
    bgc: "#f1c500",
    color: "#fff",
    borderColor: "#f1c500",
    hoverBgColor: "#e4ba00",
    hoverColor: "#fff"
  },

  default: {
    bgc: "#92278f",
    color: "#fff",
    borderColor: "#92278f",
    hoverBgColor: "#791975",
    hoverColor: "#fff",
    white: "#fff",
    purple: "#92278f",
    purpleHover: "#791975",
    lightGray: "#f2f4f7",
    grayBorder: "#c6cbd4",
    green: "#afc74a"
  }
};
