import styled, { keyframes } from "styled-components";
import { mixin } from "./mixin";
import { Variables } from "./Variables";
import { Color } from "./Color";

export const HeaderWrap = styled.div`
  position: fixed;
  top: 0;
  left: 0%;
  z-index: 9;
  /* ${mixin.flexBox};
  ${mixin.flex(`0 0 ${Variables.lyGnbWidth}`)};
  ${mixin.flexDirection("column")}; */
  width: ${Variables.lyGnbWidth};
  height: 100vh;
  color: ${Color.default.white};
  background-image: linear-gradient(to bottom, #222222, #201f1f);
  overflow: auto;

  > .header-top {
    width: 100%;
    height: 120px;
    font-weight: ${Variables.fontSize.bold};
    .header-top-userbox {
      padding: 50px 40px;
      font-size: 15px;
    }
  }

  > .header-nav {
    & > li {
      position: relative;
      display: block;
      min-height: 40px;
      text-align: left;
      border-bottom: 1px solid rgba(255, 255, 255, 0.04);
      background-color: #1d1d1d;
      &:hover {
        background-color: #0a0a0a;
      }
      &:first-child {
        border-top: 1px solid rgba(255, 255, 255, 0.04);
      }
      & > a {
        display: block;
        width: 100%;
        height: 100%;
        font-size: 14px;
        font-weight: ${Variables.fontSize.regular};
        color: #fff;
        padding: 12px 40px 12px 66px;
      }
      
      & > i {
        position: absolute;
        top: 50%;
        left: 40px;
        display: block;
        width: 16px;
        height: 16px;
        margin-top: -8px;
      }
      &.active {
        & > i {
          color: #fcaf17;
        }
      }
    }
  }
  > .version-text {
    position: absolute;
    bottom: 14px;
    left: 50%;
    transform: translate(-50%, 0);
    font-size: 14px;
    color: #fff;
  }
`;

export const FooterWrap = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  ${mixin.flexBox};
  ${mixin.alignItems("center")};
  ${mixin.justifyContent("center")};
  padding: 15px;
  background: #d0d0d0;
`;

export const Divider = styled.div`
  height: 1px;
  width: 100%;
  margin: 24px 0;
  background: #e8e8e8;
  ${props => {
    return props.last ? `display: none` : null;
  }}
`;

export const LayoutContent = styled.div`
  margin: 24px 0 24px;
  padding: 10px 0;
  background: ${Color.default.white};

  h3 {
    font-size: 14px;
    margin-bottom: 15px;
  }

  p.detailBox {
    word-break: break-all;
    margin-bottom: 20px;
    padding: 10px;
    background-color: #f9f9f9;
  }

  .input-margin-none {
    > .form-group {
      margin-bottom: 0 !important;
    }
  }

  .log-table {
    .search-container {
      margin-bottom: 25px;
      .search-box-wave {
        margin: 0 5px;
      }
      .input-container.has-button input {
        &[type="date"] {
          width: 134px;
        }
      }
    }
  }
`;

export const LayoutHeader = styled.div`
  ${mixin.flexBox};
  ${mixin.alignItems("center")};
  ${mixin.justifyContent("space-between")};
  padding: 35px 0;
  border-bottom: 1px solid ${Color.default.lightGray};
  background: ${Color.default.white};

  .page-detail-title {
    font-size: 14px;
    font-weight: 500;
  }

  .go-prev-page {
    margin-right: 5px;
  }
`;

// 디테일 정보
export const DetailInfo = {
  Row: styled.div`
    ${mixin.flexBox};
    ${mixin.alignItems("center")};
    margin-bottom: 15px;

    &:last-child {
      margin-bottom: 0;
    }

    & .detail-header {
      flex: 0 0 15%;
      font-weight: 700;
      text-align: right;
      padding-right: 15px;

      &.detail-workspace {
        display: block;
        text-align: left;
      }
    }

    & .detail-info {
      flex: 0 0 78%;
      padding: 10px;
      background: #f9f9f9;

      &.detail-workspace {
        display: block;
      }

      &.detail-userInfo {
        flex: 0 0 15%;
      }

      &.detail-group {
        background: transparent;
      }
    }

    & .form-group {
      margin-bottom: 0 !important;
    }

    .sts {
      margin-right: 10px;
    }

    ${props => {
      if (props.page === "workspace") {
        return `
          justify-content: space-around;
          align-items: baseline;
          > div {
            flex: 1;
            margin-right: 10px;
          }
        `;
      } else if (props.page === "wkspcConfirm") {
        return `
          margin-bottom: 25px;

          &:last-child {
            margin-top: -10px;
            margin-bottom: 15px;
          }

          .detail-header {
            flex: 0 0 22%;
            padding-right: 25px;
          }

          .detail-info, .form-group {
            flex: 0 0 75%;
          }

          .remain-resource {
            display: block;
            text-align: right;
            font-size: 12px;
          }
        `;
      } else if (props.page === "groupDetail") {
        return `
          .detail-info {
            flex: 0 0 78%;
          }
        `;
      }
    }};
  `
};

export const DateWrap = styled.div`
  margin-top: 5px;
`;

//리스트 없을때
export const Empty = {
  Wrapper: styled.div`
    display: table;
    width: 100%;
    min-height: ${props => {
      return props.setHeight ? props.setHeight + "px" : "400px";
    }};
    background: #f9f9f9;
  `,

  Text: styled.div`
    display: table-cell;
    vertical-align: middle;
    text-align: center;
    font-size: 40px;
  `
};

export const ListDetail = styled.ul`
  border-bottom: 1px solid #ddd;
  & > li {
    padding: 10px 0;
    overflow: hidden;
    border-bottom: 1px solid #ddd;
  }
  & > li:first-child {
    padding-top: 0;
  }
  & > li:last-child {
    border-bottom: 0;
  }
`;

export const DetailSearchBox = styled.div`
  & > .formWrap {
    display: flex;
    align-items: center;
    justify-content: flex-end;

    & .btnGroup {
      margin-left: 10px;

      & > button {
        margin-left: 5px;
      }
    }
  }
  & .formGroup {
    display: flex;

    & .form-group {
      margin: 0;
      display: flex;
      align-items: center;
    }

    & .form-group:last-child {
      margin-left: 7px;
    }
  }
`;

export const InnerBlock = styled.div`
  display: flex;
  flex-wrap: wrap;

  & > .innerBlock {
    display: inline-block;
    padding: 10px;
    margin: 3px;
    background: #fff;
    border-radius: 4px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

export const InOperation = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 50%;
  margin: 0 auto 10px;
  background: #3571d6;
  padding: 15px;
  color: #fff;
  border-radius: 4px;
  box-shadow: 0 3px 3px rgba(0, 0, 0, 0.16), 0 3px 3px rgba(0, 0, 0, 0.23);

  & > span {
    margin: 0 10px;
  }
`;

///spinner 스타일
const skCircleFadeDelay = keyframes`
  0%, 39%, 100% { opacity: 0; }
  40% { opacity: 1; }
`;

export const LoadingWrap = styled.div`
  z-index: 999999;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.54);
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;

  & .loadingPanel {
    background: #fff;
    width: 300px;
    height: 200px;
    padding: 30px 20px;
    border-radius: 13px;

    & p {
      font-size: 20px;
      text-align: center;
      margin-top: 40px;
    }
  }
`;
export const SpinnerWrapper = styled.div`
  margin: ${props => (props.customMargin ? props.customMargin : "0 auto")};
  width: ${props => (props.size ? props.size + "px" : "70px")};
  height: ${props => (props.size ? props.size + "px" : "70px")};
  position: relative;

  & > .sk-circle {
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
  }
  & > .sk-circle:before {
    content: "";
    display: block;
    margin: 0 auto;
    width: 15%;
    height: 15%;
    background-color: ${props => (props.operation ? "#fff" : "#92278f")};
    border-radius: 100%;
    animation: ${skCircleFadeDelay} 1.2s infinite ease-in-out both;
  }
  & > .sk-circle2 {
    transform: rotate(30deg);
  }
  & > .sk-circle3 {
    transform: rotate(60deg);
  }
  & > .sk-circle4 {
    transform: rotate(90deg);
  }
  & > .sk-circle5 {
    transform: rotate(120deg);
  }
  & > .sk-circle6 {
    transform: rotate(150deg);
  }
  & > .sk-circle7 {
    transform: rotate(180deg);
  }
  & > .sk-circle8 {
    transform: rotate(210deg);
  }
  & > .sk-circle9 {
    transform: rotate(240deg);
  }
  & > .sk-circle10 {
    transform: rotate(270deg);
  }
  & > .sk-circle11 {
    transform: rotate(300deg);
  }
  & > .sk-circle12 {
    transform: rotate(330deg);
  }
  & > .sk-circle2:before {
    animation-delay: -1.1s;
  }
  & > .sk-circle3:before {
    animation-delay: -1s;
  }
  & > .sk-circle4:before {
    animation-delay: -0.9s;
  }
  & > .sk-circle5:before {
    animation-delay: -0.8s;
  }
  & > .sk-circle6:before {
    animation-delay: -0.7s;
  }
  & > .sk-circle7:before {
    animation-delay: -0.6s;
  }
  & > .sk-circle8:before {
    animation-delay: -0.5s;
  }
  & > .sk-circle9:before {
    animation-delay: -0.4s;
  }
  & > .sk-circle10:before {
    animation-delay: -0.3s;
  }
  & > .sk-circle11:before {
    animation-delay: -0.2s;
  }
  & > .sk-circle12:before {
    animation-delay: -0.1s;
  }
`;

export const ShareList = styled.div`
  display: flex;
`;
export const ShareListItem = styled.div`
  display: flex;
`;
export const FileTypeWithInput = styled.div`
  position: relative;

  & .form-group {
    margin-right: 65px;
  }
`;
export const FileType = styled.div`
  position: absolute;
  top: 50%;
  right: 0;
  padding: 5px 10px;
  background: #fff;
  border: 1px solid #c6cbd4;
  border-radius: 4px;
  transition: all 0.3s ease-in;
  transform: translate(0, -50%);

  & .on {
    color: #fff;
    border: 1px solid #92278f;
    background: #92278f;
  }
`;

export const ProgressContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 9999;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  overflow: hidden;
`;

export const ProgressBox = styled.div`
  flex: 0 0 500px;
  padding: 30px 40px 15px;
  background: #fff;
  > h5 {
    font-size: 18px;
    margin-bottom: 5px;
  }
  > span {
    display: block;
    font-size: 14px;
    margin-bottom: 20px;
  }
`;

export const PackageHeader = styled.span`
  padding: 10px;
  display: block;
  border-bottom: 2px solid #e8e8e8;
  background: #eee;
`;

export const PackageBody = styled.div`
  height: 243px;
  max-height: 243px;
  overflow: auto;
  border: 1px solid #e8e8e8;
  border-top: 0;
`;

export const PackageListItem = styled.li`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 5px;
  border-top: 1px solid #e8e8e8;
  > .packageList {
    width: calc(100% - 15px);
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  > .packageBtn {
    > button {
      width: 15px;
      height: 15px;
      line-height: 15px;
      border: 1px solid #c6cbd4;
      border-radius: 2px;
    }
  }
`;

export const PackageBox = styled.div`
  ${props => {
    if (props.type === "wide") {
      return `
        flex: 0 0 80%;
      `;
    }
  }}

  .form-group {
    margin-bottom: 0;
  }

  .search-container {
    margin-bottom: 20px;
    > h5 {
      font-size: 14px;
      font-weight: bold;
      margin-bottom: 15px;
    }
  }
`;
