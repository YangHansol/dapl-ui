export const Variables = {
  color: {
    purple: "#92278f",
    purpleHover: "#791975",
    lightGray: "#f2f4f7",
    grayBorder: "#c6cbd4",
    green: "#afc74a"
  },

  statusColor: [
    { name: "red", color: "f37160" },
    { name: "orange", color: "fcb040" },
    { name: "green", color: "afc74a" },
    { name: "blue", color: "5ec0de" },
    { name: "purple", color: "db2a92" }
  ],

  fontSize: {
    bold: 700,
    medium: 500,
    regular: 400
  },

  lyGnbWidth: "250px",

  iconList: [
    "workspace-mine",
    "workspace-shared",
    "workspace-cpu",
    "workspace-gpu",
    "workspace-active",
    "workspace-wait",
    "workspace-stop"
  ]
};
