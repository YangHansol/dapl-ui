import styled from "styled-components";
import { Color } from "./Color";

export const CustomButton = {
  Styled: styled.button`
    display: inline-block;
    font-size: 14px;
    font-weight: normal;
    text-align: center;
    white-space: nowrap;
    line-height: 1.42857143;
    vertical-align: middle;
    margin-bottom: 0;
    padding: 6px 12px;
    border: 1px solid transparent;
    border-radius: 4px;
    background-image: none;

    width: ${props => {
      return props.colwidth ? props.colwidth : "auto";
    }};
    margin: ${props => {
      return props.margin ? props.margin : "0 0 0 5px;";
    }};
    color: ${props => {
      return props.btStyle ? Color[props.btStyle].color : Color.default.color;
    }};
    border-color: ${props => {
      return props.btStyle
        ? Color[props.btStyle].borderColor
        : Color.default.borderColor;
    }};
    background-color: ${props => {
      return props.btStyle ? Color[props.btStyle].bgc : Color.default.bgc;
    }};

    touch-action: manipulation;
    user-select: none;
    cursor: pointer;

    &[disabled] {
      opacity: 0.65;
      box-shadow: none;
      filter: alpha(opacity=65);
      cursor: not-allowed;
    }

    &:hover,
    &:active,
    &:focus {
      color: ${props => {
        return props.btStyle
          ? Color[props.btStyle].hoverColor
          : Color.default.hoverColor;
      }};

      background-color: ${props => {
        return props.btStyle
          ? Color[props.btStyle].hoverBgColor
          : Color.default.hoverBgColor;
      }};
    }

    &.btn-back.fixed {
      position: fixed;
      bottom: 7%;
      right: 50px;
      z-index: 8;
    }
  `,

  PageFooter: styled.div`
    margin-top: 25px;
    padding: 20px 0;
    border-top: 1px solid #ddd;
  `
};

export const Btn = {
  BtnBasic: styled.button`
    display: inline-block;
    min-height: 32px;
    line-height: 32px;
    font-size: 14px;
    font-weight: 500;
    text-align: center;
    vertical-align: middle;

    color: ${props => {
      if (props.color === "white") {
        return `#4d4d4d`;
      } else if (props.color === "gray") {
        return `#000`;
      } else {
        return `#fff`;
      }
    }};

    padding: 0 10px;
    border: 1px solid transparent;
    border-radius: 2px;
    border-color: ${props => {
      if (props.color === "white") {
        return `#c6cbd4`;
      } else if (props.color === "primary") {
        return `#92278f`;
      } else if (props.color === "gray") {
        return `#f2f4f7`;
      }
    }};

    background-color: ${props => {
      if (props.color === "white") {
        return `#fff`;
      } else if (props.color === "primary") {
        return `#92278f`;
      } else if (props.color === "gray") {
        return `#f2f4f7`;
      }
    }};

    &:hover {
      border-color: ${props => {
        if (props.color === "primary") {
          return `#791975`;
        } else if (props.color === "gray") {
          return `#e1e7ec`;
        }
      }};
      background-color: ${props => {
        if (props.color === "white") {
          return `#f6f6f6`;
        } else if (props.color === "primary") {
          return `#791975`;
        } else if (props.color === "gray") {
          return `#e1e7ec`;
        }
      }};
    }

    @media (max-width: 1500px) {
      min-height: 25px;
      line-height: 25px;
      font-size: 11px;
      padding: 0 8px;
    }
  `
};

export const MoreButton = styled.div`
  text-align: center;
  border: 1px solid #ddd;
  border-radius: 10px;
  margin-bottom: 20px;

  & .more-link {
    padding: 15px;
    display: block;
  }
`;
