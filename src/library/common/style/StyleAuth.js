import styled from "styled-components";
import { Color } from "./Color";

import LoginLogo from "../../images/logo-login.png";
import IconEye from "../../images/ic-eye.png";
import IconEyeOff from "../../images/ic-eye-off.png";
import IconBack from "../../images/ic-back.png";

export const Login = {
  Wrapper: styled.div`
    position: relative;
    > .bg {
      display: flex;
      justify-content: center;
      flex-direction: column;
      align-items: center;
      min-width: 1200px;
      height: 100vh;
      text-align: center;
      background: #f2f4f7;
    }

    > .btn-back {
      position: absolute;
      top: 50%;
      left: 450px;
      width: 45px;
      font-size: 14px;
      color: #7b7b7b;
      padding-top: 53px;
      background: url(${IconBack}) no-repeat;
      background-size: 45px;
      transform: translate(0, -50%);

      @media (max-width: 1600px) {
        left: 315px;
        width: 31.5px;
        font-size: 10px;
        padding-top: 37px;
        background-size: 31.5px;
      }
    }
  `,

  Container: styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: stretch;
    width: 557px;
    padding: 68px 135px 0;
    border-top-left-radius: 15px;
    border-top-right-radius: 15px;
    box-shadow: 0 2px 41px 1px rgba(0, 0, 0, 0.06);
    background: ${Color.default.color};

    > .version-text {
      position: absolute;
      top: 30px;
      left: 40px;
      font-size: 18px;
      color: #8b8b8b;
    }

    @media (max-width: 1600px) {
      width: 389px;
      padding: 47px 94px 0;

      > .version-text {
        top: 20px;
        left: 20px;
        font-size: 12px;
      }
    }
  `,

  Header: styled.div`
    > h1 {
      width: 205px;
      height: 117px;
      text-indent: -9999em;
      margin: 0 auto 13px;
      background: url(${LoginLogo}) no-repeat center bottom;
      background-size: contain;
    }
    > h2 {
      font-size: 30px;
      color: #373737;
    }
    > span {
      font-size: 20px;
      color: #8b8b8b;
    }
    > small {
      font-size: 16px;
      color: #676767;
    }

    @media (max-width: 1600px) {
      > h1 {
        width: 143px;
        height: 81px;
        margin: 0 auto 9px;
      }
      > h2 {
        font-size: 21px;
      }
      > span {
        font-size: 14px;
      }
      > small {
        font-size: 11px;
      }
    }
  `,

  Footer: styled.div`
    width: 557px;
    padding: 30px 0 23px;
    background: #383838;
    border-bottom-left-radius: 15px;
    border-bottom-right-radius: 15px;

    & .copyright {
      display: block;
      color: #7a7a7a;
      font-size: 13px;
      line-height: 1.5;
    }

    @media (max-width: 1600px) {
      width: 389px;
      padding: 21px 0 16px;

      & .copyright {
        font-size: 10px;
      }
    }
  `,

  FormStyle: styled.div`
    width: 287px;
    margin-top: 50px;

    /* 각 input의 clssName이 form-control - bootstrap-react.css */
    & .form-control {
      height: 50px !important;
      font-size: 18px;
      color: #292929;
      padding: 15px 20px;
      border: 1px solid #d8d8d8;
      background-color: #fff;

      &.input-pw-icon {
        padding: 15px 40px 15px 20px;
      }

      ::placeholder {
        color: #9c9c9c;
      }
    }

    & .form-group {
      position: relative;
      margin-bottom: 6px;
    }

    /* 비밀번호 입력 시 내용 보이게 하는 눈 모양 아이콘 css */
    #icon-show {
      position: fixed;
      top: -9999em;
    }
    .icon-show-label {
      position: absolute;
      top: 50%;
      right: 14px;
      width: 25px;
      height: 13px;
      text-indent: -9999em;
      background: url(${IconEye}) no-repeat;
      background-size: cover;
      transform: translate(0, -50%);
    }
    #icon-show:checked + .icon-show-label {
      background: url(${IconEyeOff}) no-repeat;
      background-size: cover;
    }

    /* 비밀번호 변경, 회원가입 시 패스워드 설정 정보 css */
    .icon_question {
      position: absolute;
      top: 50%;
      right: -30px;
      width: 23px;
      height: 23px;
      font-size: 15px;
      text-align: center;
      line-height: 23px;
      color: #fff;
      border-radius: 23px;
      background: #cecece;
      transform: translate(0, -50%);
      cursor: pointer;

      &:hover {
        background: #373737;
      }

      &:hover + .info_password {
        opacity: 1;
      }
    }

    .info_password {
      position: absolute;
      top: -25px;
      right: -330px;
      text-align: left;
      padding: 28px 30px;
      border-radius: 5px;
      box-shadow: 2px 2px 20px 0 rgba(0, 0, 0, 0.1);
      background: #fff;
      opacity: 0;
      transition: 0.5s;

      > h4 {
        font-size: 16px;
        color: #373737;
        margin-bottom: 15px;
      }

      > ul > li {
        font-size: 13px;
        color: #626262;
        margin-bottom: 5px;

        &:last-of-type {
          margin-bottom: 0px;
        }

        > em {
          font-weight: bold;
          color: #373737;
        }
      }
    }

    /* 에러 메시지 css */
    .msg-validate {
      height: 14px;
      font-size: 14px;
      line-height: 1;
      color: #92278f;
      padding-top: 21px;
      box-sizing: content-box;
    }

    @media (max-width: 1600px) {
      width: 200px;
      margin-top: 35px;
      & .form-control {
        height: 40px !important;
        font-size: 12px;
        padding: 10px 13px;

        &.input-pw-icon {
          padding: 10px 24px 10px 13px;
        }
      }

      .icon-show-label {
        right: 9.8px;
        width: 17.5px;
        height: 9px;
      }

      .icon_question {
        right: -21px;
        width: 16px;
        height: 16px;
        font-size: 10.5px;
        line-height: 16px;
        border-radius: 16px;
      }

      .info_password {
        top: -17.5px;
        right: -245px;
        padding: 19px 21px;

        > h4 {
          font-size: 12px;
          margin-bottom: 10.5px;
        }

        > ul > li {
          font-size: 10px;
        }
      }

      .msg-validate {
        height: 10px;
        font-size: 10px;
        padding-top: 11px;
      }
    }
  `,

  Button: styled.button`
    display: block;
    width: 100%;
    font-size: 18px;
    color: ${Color.default.color};
    margin: ${props => {
      return props.type === "submit" ? "27px 0 68px" : "20px 0 30px";
    }};
    padding: 15px;
    border-radius: 3px;
    background-color: ${Color.default.bgc};

    &:hover {
      background-color: ${Color.default.hoverBgColor};
    }

    @media (max-width: 1600px) {
      font-size: 12px;
      margin: ${props => {
        return props.type === "submit" ? "15px 0 40px" : "14px 0 21px";
      }};
      padding: 10.5px;
    }
  `,

  UserSetBtn: styled.div`
    margin-bottom: 40px;

    > button {
      display: inline-block;
      font-size: 14px;
      line-height: 1;
      color: #898989;
      padding: 0 10px;
      border-left: 1px solid #d0d0d0;
      cursor: pointer;

      &:first-of-type {
        border-left: 0 none;
      }
    }

    @media (max-width: 1600px) {
      margin-bottom: 28px;

      > button {
        font-size: 10px;
        padding: 0 7px;
      }
    }
  `
};
