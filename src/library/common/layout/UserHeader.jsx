import React, { Component } from "react";
import StartsWith from "../../library/StartsWith";
import { Nav, NavItem } from "react-bootstrap";

import { HeaderWrap } from "../style/StyleComponents";

class UserHeader extends Component {
  onClickMove = (e, url) => {
    e.preventDefault();
    this.props.history.push("/user" + url);
    return false;
  };

  render() {
    const { pagename } = this.props.location.query;
    const getName = sessionStorage.getItem("name");
    return (
      <header>
        <HeaderWrap>
          <div className="header-top">
            <div className="header-top-userbox">
              <p>안녕하세요. {getName}님</p>
            </div>
            {/* <div className="header-top-logobox">
              <a
                href=""
                onClick={e =>
                  this.onClickMove(e, "/index.html?pagename=main/main")
                }
              >
                <div style={{width:150, height:40, background:'#eee'}}></div>
              </a>
            </div> */}
          </div>

          <Nav bsClass={"header-nav"}>
            {/*new-header nav*/}
            <NavItem
              eventKey={1}
              active={StartsWith(pagename, "main")}
              href="#"
              onClick={e =>
                this.onClickMove(e, "/index.html?pagename=main/main")
              }
            >
              홈
            </NavItem>
            <NavItem
              eventKey={3}
              active={StartsWith(pagename, "workspace")}
              href="#"
              onClick={e =>
                this.onClickMove(
                  e,
                  "/index.html?pagename=workspace/workspacelist"
                )
              }
            >
              워크스페이스
            </NavItem>
            <NavItem
              eventKey={4}
              active={StartsWith(pagename, "experiment")}
              href="#"
              onClick={e =>
                this.onClickMove(
                  e,
                  "/index.html?pagename=experiment/experimentlist"
                )
              }
            >
              실험
            </NavItem>
            <NavItem
              eventKey={5}
              active={StartsWith(pagename, "dataset")}
              href="#"
              onClick={e =>
                this.onClickMove(e, "/index.html?pagename=dataset/datasetlist")
              }
            >
              데이터셋
            </NavItem>
            <NavItem
              eventKey={6}
              active={StartsWith(pagename, "release")}
              href="#"
              onClick={e =>
                this.onClickMove(e, "/index.html?pagename=release/releaselist")
              }
            >
              배포
            </NavItem>
            {/* <NavItem eventKey={7} active={StartsWith(pagename, 'board')} href="#" onClick={(e) => (this.onClickMove(e, '/?pagename=board/boardlist'))}>
              게시판
            </NavItem> */}
          </Nav>
        </HeaderWrap>
      </header>
    );
  }
}

export default UserHeader;
