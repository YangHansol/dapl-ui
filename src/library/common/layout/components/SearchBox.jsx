import React, { Component } from "react";
import moment from "moment";

import InputForm from "../../components/InputForm";
import SelectForm from "../../components/SelectForm";
import Popup from "../../Popup";

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchType: "",
      searchKeyword: "",
      fmDate: moment().format("YYYY-MM-DD"),
      toDate: moment().format("YYYY-MM-DD"),
      isRadioTp: false,
      isDateTp: false
    };
  }

  handleSearch = e => {
    const { searchType, searchKeyword, fmDate, toDate, isDateTp } = this.state;
    let query = { ...this.props.location.query };

    if (query.searchQuery) {
      delete query.searchQuery;
    }

    if (query.number) {
      delete query.number;
    }

    if (searchType === "") {
      return Popup.notice({ message: "검색타입을 입력해 주세요" });
    } else if (searchType !== "") {
      if (!!isDateTp) {
        if (query.searchType) {
          delete query.searchType;
          delete query.searchText;
        }

        if (fmDate > toDate) {
          Popup.notice({
            message: "종료일자는 시작일자보다 앞 설수 없습니다."
          });
          return false;
        }
        query.fmDate = fmDate;
        query.toDate = toDate;
        this.props.getListForPaging(query);
      } else {
        if (query.fmDate) {
          delete query.fmDate;
        }

        if (query.toDate) {
          delete query.toDate;
        }

        if (searchKeyword.trim().length === 0) {
          return Popup.notice({ message: "검색어를 입력해 주세요" });
        } else {
          query.searchType = searchType;
          query.searchText = searchKeyword;
          this.props.getListForPaging(query);
        }
      }
    }
  };
  onClickResetSearch = () => {
    let query = { ...this.props.location.query };

    if (query.searchType) {
      delete query.searchText;
      delete query.searchType;

      this.searchType.value = "";
      this.searchKeyword.value = "";
    }

    if (query.fmDate) {
      delete query.fmDate;

      this.searchType.value = "";
      this.fmDate.value = "";
    }

    if (query.toDate) {
      delete query.toDate;

      this.searchType.value = "";
      this.toDate.value = "";
    }

    if (query.number) {
      delete query.number;
    }

    if (query.sort) {
      delete query.sort;
    }

    this.setState({
      isRadioTp: false,
      isDateTp: false,
      searchType: "",
      searchKeyword: "",
      fmDate: moment().format("YYYY-MM-DD"),
      toDate: moment().format("YYYY-MM-DD")
    });

    this.props.getListForPaging(query);
  };

  handleSelectSearchType = e => {
    if (
      e.target.value === "libTp" ||
      e.target.value === "serverId" ||
      e.target.value === "logTp" ||
      e.target.value === "snupSts" ||
      e.target.value === "dataTp" ||
      e.target.value === "downSts" ||
      e.target.value === "rlsSts" ||
      e.target.value === "workSts"
    ) {
      this.setState({
        isDateTp: false,
        isRadioTp: true,
        searchKeyword: "",
        [e.target.name]: e.target.value
      });
    } else if (e.target.value === "date") {
      this.setState({
        isRadioTp: false,
        isDateTp: true,
        fmDate: moment().format("YYYY-MM-DD"),
        toDate: moment().format("YYYY-MM-DD"),
        [e.target.name]: e.target.value
      });
    } else {
      this.setState({
        isRadioTp: false,
        isDateTp: false,
        searchKeyword: "",
        [e.target.name]: e.target.value
      });
    }
  };

  inputType = () => {
    const { RadioSearchItem } = this.props;
    const { isRadioTp, isDateTp, fmDate, toDate, searchType } = this.state;

    if (!!isRadioTp) {
      return (
        <SelectForm
          name="searchKeyword"
          items={RadioSearchItem ? RadioSearchItem[searchType] : null}
          inputRef={ref => (this.searchKeyword = ref)}
          onChange={this.handleChange}
        />
      );
    } else if (!!isDateTp) {
      return (
        <React.Fragment>
          <InputForm
            type="date"
            name="fmDate"
            inputRef={ref => (this.fmDate = ref)}
            defaultValue={fmDate}
            max={moment().format("YYYY-MM-DD")}
            onChange={this.handleChange}
            onKeyDown={e => {
              e.preventDefault();
              return false;
            }}
          />
          <span className="search-box-wave"> ~ </span>
          <InputForm
            type="date"
            name="toDate"
            inputRef={ref => (this.toDate = ref)}
            defaultValue={toDate}
            min={fmDate}
            max={moment().format("YYYY-MM-DD")}
            onChange={this.handleChange}
            onKeyDown={e => {
              e.preventDefault();
              return false;
            }}
          />
        </React.Fragment>
      );
    } else {
      return (
        <InputForm
          name="searchKeyword"
          type="text"
          inputRef={ref => (this.searchKeyword = ref)}
          value={this.state.searchKeyword}
          placeholder="검색어를 입력해 주세요."
          onKeyPress={this.handleKeyPress}
          onChange={this.handleChange}
        />
      );
    }
  };

  handleChange = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value
    });
  };

  handleKeyPress = e => {
    this.handleSearch(e);
  };

  render() {
    const { SearchItem, innerSearch } = this.props;

    return (
      <React.Fragment>
        <div className="search-container">
          <div className="input-container">
            <strong className="title">검색</strong>
            <div className="select sel-type-1">
              <SelectForm
                name="searchType"
                items={innerSearch ? innerSearch : SearchItem}
                inputRef={ref => (this.searchType = ref)}
                onChange={this.handleSelectSearchType}
              />
            </div>
          </div>
          <div className="input-container has-button">
            {this.inputType()}
            <button
              className="btn-basic-white"
              onClick={this.onClickResetSearch}
            >
              초기화
            </button>
            <button className="btn-basic-primary" onClick={this.handleSearch}>
              검색
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default SearchBox;
