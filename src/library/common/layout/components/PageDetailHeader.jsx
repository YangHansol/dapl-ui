import React, { Component } from "react";
import { LayoutHeader } from "../../style/StyleComponents";
import { CustomButton } from "../../style/StyleButton";

class PageDetailHeader extends Component {
  onClickBack = e => {
    e.preventDefault();
    let query = { ...this.props.location.query };
    if (query.searchType) {
      delete query.searchType;
      delete query.searchText;
    }
    if (typeof this.props.onBack && this.props.onBack) {
      this.props.onBack(query.actPreSort, query.actPrePage, query.groupId);
    } else {
      this.props.history.goBack();
    }
  };

  handleScroll = event => {
    const winScroll =
      document.body.scrollTop || document.documentElement.scrollTop;
    const btnBack = document.querySelector(".btn-back");

    if (!!btnBack && winScroll > 175) {
      btnBack.classList.add("fixed");
    } else if (!!btnBack && winScroll <= 175) {
      btnBack.classList.remove("fixed");
    } else {
      btnBack.classList.remove("fixed");
    }
  };

  componentDidMount = () => {
    window.addEventListener("scroll", this.handleScroll);
  };

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  render() {
    const { title, backToText = null } = this.props;

    return (
      <div>
        <LayoutHeader>
          <h1 className="page-detail-title">{title}</h1>
          <div className="button-container">
            {backToText && (
              <CustomButton.Styled
                className="btn-back"
                btStyle="noColor"
                onClick={this.onClickBack}
              >
                <i className="fa fa-arrow-left go-prev-page" />
                {backToText}
              </CustomButton.Styled>
            )}
            {this.props.children && React.cloneElement(this.props.children)}
          </div>
        </LayoutHeader>
      </div>
    );
  }
}

export default PageDetailHeader;
