import React from "react";
import { Empty } from "../../style/StyleComponents";

const EmptyBox = ({ setHeight = null }) => {
  return (
    <Empty.Wrapper setHeight={setHeight}>
      <Empty.Text>
        <h3>해당 리스트 정보가 없습니다.</h3>
      </Empty.Text>
    </Empty.Wrapper>
  );
};

export default EmptyBox;
