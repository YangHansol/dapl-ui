import React, { Component } from "react";
import { CustomButton } from "../../style/StyleButton";

class PageListHeader extends Component {
  refresh = () => {
    const { item, refreshList } = this.props;
    refreshList(item);
  };

  render() {
    const { title, length } = this.props;
    return (
      <div className="page-header">
        <h2 className="page-title">
          {title} {length ? "(" + length + ")" : null}
        </h2>
        <div className="button-container">
          <CustomButton.Styled btStyle="noColor" onClick={this.refresh}>
            <i className="fa fa-refresh" />
          </CustomButton.Styled>
          {this.props.children && React.cloneElement(this.props.children)}
        </div>
      </div>
    );
  }
}

export default PageListHeader;
