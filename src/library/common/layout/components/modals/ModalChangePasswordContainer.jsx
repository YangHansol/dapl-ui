import React, { Component } from "react";
import { connect } from "react-redux";
import { Modal, Col, Form } from "react-bootstrap";

import { RegExp } from "../../../../library/constants/RegExp";
import * as MemberAction from "../../../../member/MemberAction";
import ModalButtonGroups from "../../../components/ModalButtonGroups";
import InputForm from "../../../components/InputForm";
import Popup from "../../../Popup";

import { LayerPopupStyle } from "../../../style/StyleModal";

class ModalChangePasswordContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      old_Pswd: "",
      new_Pswd: "",
      check_Pswd: "",
      validateMsg: ""
    };
  }

  //Input Validation Fuction
  validateInput = () => {
    let { old_Pswd, new_Pswd, check_Pswd } = this.state;

    if (old_Pswd.trim().length === 0) {
      this.setState({ validateMsg: "이전 비밀번호를 입력해주세요." });
      return false;
    }

    if (new_Pswd.trim().length === 0) {
      this.setState({ validateMsg: "변경할 비밀번호를 입력해주세요." });
      return false;
    }

    if (!RegExp.PASSWORD.test(new_Pswd)) {
      this.setState({
        validateMsg: "형식에 맞춰 비밀번호를 입력해주세요."
      });
      return false;
    }

    if (new_Pswd !== check_Pswd) {
      this.setState({
        validateMsg: "변경할 비밀번호와 일치하지 않습니다."
      });
      return false;
    }

    return true;
  };

  onClose = () => {
    this.props.onClose();
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onClickChangePassword = e => {
    e.preventDefault();

    if (this.validateInput() === false) return;

    let { old_Pswd, new_Pswd } = this.state;
    let getUserId = sessionStorage.getItem("id");
    let postData = {
      oldPswd: old_Pswd,
      newPswd: new_Pswd,
      userId: getUserId
    };

    Popup.confirm({
      message: "비밀번호를 변경하시겠습니까?",
      callback: type => {
        if (type === "OK") {
          this.props.passwordUpdate(postData);
          this.props.onClose();
        }
      }
    });
  };

  render() {
    return (
      <LayerPopupStyle.ModalWrap>
        <LayerPopupStyle.ModalContent>
          <LayerPopupStyle.Header className="modal-header" border>
            <Modal.Title>비밀번호 변경</Modal.Title>
            <button className="btn-modal-close" onClick={this.onClose}>
              닫기
            </button>
          </LayerPopupStyle.Header>
          <LayerPopupStyle.ModalBody className="modal-body">
            <Form>
              <Col sm={12}>
                <InputForm
                  id="old_Pswd"
                  type="password"
                  name="old_Pswd"
                  label="기존 비밀번호"
                  placeholder="이전 비밀번호"
                  onChange={this.handleChange}
                  autoComplete="off"
                  bsSize="large"
                />
                <InputForm
                  id="new_Pswd"
                  type="password"
                  name="new_Pswd"
                  label="새 비밀번호"
                  placeholder="변경할 비밀번호"
                  onChange={this.handleChange}
                  autoComplete="off"
                  bsSize="large"
                />
                <InputForm
                  id="check_Pswd"
                  type="password"
                  name="check_Pswd"
                  label="비밀번호 확인"
                  placeholder="비밀번호 확인"
                  onChange={this.handleChange}
                  autoComplete="off"
                  bsSize="large"
                />
              </Col>
            </Form>
            <p className="msg-validate">{this.validateMsg}</p>
          </LayerPopupStyle.ModalBody>
          <ModalButtonGroups
            onClose={this.onClose}
            onConfirm={this.onClickChangePassword}
            confirmText="변경"
          />
        </LayerPopupStyle.ModalContent>
      </LayerPopupStyle.ModalWrap>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    passwordUpdate: data => {
      dispatch(MemberAction.passwordUpdate(data));
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(ModalChangePasswordContainer);
