import React from "react";

const EmptyList = ({ colSpan }) => {
  return (
    <tbody>
      <tr>
        <td colSpan={colSpan}>
          <div className="text-noData">해당 리스트 정보가 없습니다.</div>
        </td>
      </tr>
    </tbody>
  );
};

export default EmptyList;
