import React from "react";

const PageTopHeader = ({ userHeader, handleClick, isClosed }) => {
  const getName = sessionStorage.getItem("name");

  if (!!isClosed) {
    document.querySelector(".select-combobox").value = "";
  }

  return (
    <div className="contents-header">
      <h1 className="logo">
        <span className="logo-title">AICP-{userHeader ? "USER" : "ADMIN"}</span>
      </h1>
      <div className="select sel-type-2">
        <div className="select-text">{getName}</div>
        <select
          name=""
          id=""
          className="select-combobox"
          defaultValue=""
          onChange={handleClick}
        >
          <option value="" disabled hidden>
            선택하세요.
          </option>
          <option value="changepw">비밀번호 변경</option>
          <option value="logout">로그아웃</option>
        </select>
      </div>
    </div>
  );
};

export default PageTopHeader;
