import React, { Component } from "react";
import cn from "classnames";
import PageTopHeader from "./components/PageTopHeader";
import Popup from "../Popup";

import ModalChangePasswordContainer from "./components/modals/ModalChangePasswordContainer";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isClosed: false
    };
  }

  onCloseModal = () => {
    Popup.hide(this.createModal);
    this.setState({
      isClosed: true
    });
  };

  handleClick = e => {
    if (e.target.value === "logout") {
      e.preventDefault();

      let basename = this.props.userHeader ? "/user" : "/admin";

      sessionStorage.removeItem("tkn");
      sessionStorage.removeItem("id");
      sessionStorage.removeItem("name");
      sessionStorage.removeItem("userLv");

      this.props.history.push(basename + "/index.html?pagename=member/login");
    } else if (e.target.value === "changepw") {
      e.preventDefault();

      this.createModal = Popup.modal(
        <ModalChangePasswordContainer onClose={this.onCloseModal} />
      );
      this.setState({
        isClosed: false
      });
    }
  };

  render() {
    const { userHeader } = this.props;

    return (
      <section
        className={cn("contents", {
          " main": this.props.location.search === "?pagename=main/main"
        })}
      >
        <PageTopHeader
          userHeader={userHeader}
          isClosed={this.state.isClosed}
          handleClick={this.handleClick}
        />
        <div className="contents-body">{this.props.children}</div>
      </section>
    );
  }
}

export default Dashboard;
