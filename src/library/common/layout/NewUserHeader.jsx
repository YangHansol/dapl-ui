import React, { Component } from "react";
import { Layout, Menu, Icon } from "antd";
import { Link } from "react-router-dom";
const { Sider } = Layout;

class NewUserHeader extends Component {
  render() {
    return (
      <Sider
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0
        }}
      >
        <div
          className="logo"
          style={{
            height: "32px",
            background: "rgba(255,255,255,.2)",
            margin: "16px"
          }}
        />
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1">
            <Link to="?pagename=main">
              <Icon type="user" />
              <span className="nav-text">홈</span>
            </Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link to="?pagename=workspace">
              <Icon type="video-camera" />
              <span className="nav-text">워크스페이스</span>
            </Link>
          </Menu.Item>
          <Menu.Item key="3">
            <Link to="?pagename=experiment"></Link>
            <Icon type="upload" />
            <span className="nav-text">실험</span>
          </Menu.Item>
          <Menu.Item key="4">
            <Link to="?pagename=dataset">
              <Icon type="bar-chart" />
              <span className="nav-text">데이터 셋</span>
            </Link>
          </Menu.Item>
          <Menu.Item key="5">
            <Link to="?pagename=release">
              <Icon type="cloud-o" />
              <span className="nav-text">배포</span>
            </Link>
          </Menu.Item>
        </Menu>
      </Sider>
    );
  }
}

export default NewUserHeader;
