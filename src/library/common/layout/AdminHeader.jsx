import React, { Component } from "react";
import { Link } from "react-router-dom";
import cn from "classnames";

import StartsWith from "../../library/StartsWith";

import { HeaderWrap } from "../style/StyleComponents";

const urlPrefix = "/admin";
const navItem = [
  {
    page: "홈",
    pagename: "main",
    path: "/index.html?pagename=main/main",
    icon: "home"
  },
  {
    page: "그룹",
    pagename: "group",
    path: "/index.html?pagename=group/grouplist",
    icon: "users"
  },
  {
    page: "사용자",
    pagename: "user",
    path: "/index.html?pagename=user/userlist",
    icon: "user"
  },
  {
    page: "워크스페이스",
    pagename: "workspace",
    path: "/index.html?pagename=workspace/workspacelist",
    icon: "folder"
  },
  {
    page: "데이터셋",
    pagename: "dataset",
    path: "/index.html?pagename=dataset/datasetlist",
    icon: "book"
  },
  {
    page: "패키지",
    pagename: "package",
    path: "/index.html?pagename=package/packagelist",
    icon: "file"
  },
  {
    page: "패키지 팩",
    pagename: "pkgpack",
    path: "/index.html?pagename=pkgpack/packlist",
    icon: "archive"
  },
  {
    page: "알고리즘",
    pagename: "algorithm",
    path: "/index.html?pagename=algorithm/algorithmlist",
    icon: "code"
  },
  {
    page: "실험",
    pagename: "experiment",
    path: "/index.html?pagename=experiment/experimentlist",
    icon: "flask"
  },
  {
    page: "배포",
    pagename: "release",
    path: "/index.html?pagename=release/releaselist",
    icon: "exchange"
  } /*,
  {
    page: "도커",
    pagename: "docker",
    path: "/index.html?pagename=docker/dockerlist"
  }*/
];

class AdminHeader extends Component {
  onClickMove = (e, url) => {
    e.preventDefault();
    this.props.history.push("/admin" + url);
    return false;
  };

  render() {
    const { pagename } = this.props.location.query;
    const getName = sessionStorage.getItem("name");
    return (
      <header className="header">
        <HeaderWrap>
          <div className="header-top">
            <div className="header-top-userbox">
              <p>안녕하세요. {getName}님</p>
            </div>
          </div>
          <ul className="header-nav">
            {navItem.map((nav, i) => (
              <li
                key={i}
                role="presentation"
                className={cn({
                  active: StartsWith(pagename, nav.pagename)
                })}
              >
                <i className={`fa fa-${nav.icon}`} />
                <Link to={`${urlPrefix}${nav.path}`}>{nav.page}</Link>
              </li>
            ))}
          </ul>
          <small className="version-text">( v 1. 5. 0 )</small>
        </HeaderWrap>
      </header>
    );
  }
}

export default AdminHeader;
