import React, { Component } from "react";
import { FormGroup, ControlLabel, Radio } from "react-bootstrap";

class RadioForm extends Component {
  render() {
    const { label, onChange, items, name, id, defaultitem } = this.props;
    return (
      <FormGroup controlId={id} onChange={onChange}>
        {label ? <ControlLabel>{label}</ControlLabel> : null}
        {items.map((item, index) => {
          return (
            <Radio
              inline
              key={index}
              name={name}
              defaultChecked={defaultitem === item.value}
              value={item.value}
              title={item.title}
              {...this.props}
            >
              {item.title}
            </Radio>
          );
        })}
      </FormGroup>
    );
  }
}

export default RadioForm;
