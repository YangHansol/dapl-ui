import React, { Component } from "react";
import { FormGroup, ControlLabel, FormControl } from "react-bootstrap";

class SelectForm extends Component {
  render() {
    const {
      id,
      label,
      items,
      value,
      emptyoption = "true",
      deval = "선택하세요"
    } = this.props;
    return (
      <FormGroup controlId={id}>
        {!!label ? <ControlLabel>{label}</ControlLabel> : null}
        <FormControl
          componentClass="select"
          placeholder="select"
          {...this.props}
        >
          {emptyoption === "true" && <option value="">{deval}</option>}
          {items &&
            items.map((item, index) => {
              return (
                <option key={index} value={item.value || value}>
                  {item.title ? item.title : value}
                </option>
              );
            })}
        </FormControl>
      </FormGroup>
    );
  }
}

export default SelectForm;
