import React from "react";
import Utils from "../../library/utils/Utils";

const StatusBullet = ({ status, stsId = null }) => {
  return (
    <i
      id={stsId ? stsId : null}
      className={`circle bg-${Utils.statusCircle(status)}`}
    />
  );
};

export default StatusBullet;
