import React from "react";
import { Modal } from "react-bootstrap";

import { CustomButton } from "../style/StyleButton";

const ModalButtonGroups = ({
  onClose,
  onConfirm,
  colwidth,
  danger,
  confirmText = "등록",
  nomal = true
}) => {
  return (
    <Modal.Footer>
      <CustomButton.Styled
        className="btn"
        onClick={onClose}
        colwidth="120px"
        btStyle={nomal ? "noColor" : null}
      >
        취소
      </CustomButton.Styled>
      <CustomButton.Styled
        className="btn"
        onClick={onConfirm}
        colwidth="120px"
        btStyle={danger ? "danger" : null}
      >
        {confirmText}
      </CustomButton.Styled>
    </Modal.Footer>
  );
};

export default ModalButtonGroups;
