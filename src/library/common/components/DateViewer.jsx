import React from "react";
import moment from "moment";
import { DateFormat } from "library/constants/Date";

const DateViewer = ({ date, half }) => {
  let dateFormat = half ? DateFormat.half : DateFormat.full;
  let infoDate = moment(date);
  return <span>{infoDate.format(dateFormat)}</span>;
};

export default DateViewer;
