import React from "react";
import styled from "styled-components";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

const TooltipText = styled.span`
  ${props => {
    return props.bulwidth ? null : "display: block";
  }}
  width: 100%;
  text-overflow: ellipsis;
  overflow: hidden;
  cursor: pointer;
`;

const TooltipBox = ({ id, children, fullName, bulwidth }) => {
  return (
    <OverlayTrigger
      placement="top"
      overlay={<Tooltip id={id}>{fullName}</Tooltip>}
      delayShow={300}
      delayHide={150}
    >
      <TooltipText bulwidth={bulwidth}>{children}</TooltipText>
    </OverlayTrigger>
  );
};

export default TooltipBox;
