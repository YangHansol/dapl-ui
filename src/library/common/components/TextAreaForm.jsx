import React from "react";
import {
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock
} from "react-bootstrap";

const TextAreaForm = ({
  id,
  label,
  help,
  minHeight = 200,
  validationstate = null,
  ...props
}) => {
  return (
    <FormGroup controlId={id} validationState={validationstate}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl
        {...props}
        componentClass="textarea"
        style={{ resize: "none", minHeight: minHeight }}
      />
      {help !== "" ? <HelpBlock>{help}</HelpBlock> : null}
    </FormGroup>
  );
};

export default TextAreaForm;
