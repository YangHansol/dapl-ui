import React, { Component, Fragment } from "react";
import {
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock
} from "react-bootstrap";

class InputForm extends Component {
  handleSearch = e => {
    const { onKeyPress } = this.props;
    if (e.charCode === 13) {
      if (!onKeyPress) {
        e.preventDefault();
      } else {
        this.props.onKeyPress();
      }
    }
  };

  render() {
    const {
      id,
      validationstate = null,
      help = "",
      label,
      placeholder
    } = this.props;
    return (
      <Fragment>
        <FormGroup controlId={id} validationState={validationstate}>
          {!!label ? (
            <ControlLabel className="title">{label}</ControlLabel>
          ) : null}
          <FormControl
            className="input-basic"
            {...this.props}
            placeholder={placeholder}
            onKeyPress={this.handleSearch}
          />
          {help !== "" ? <HelpBlock>{help}</HelpBlock> : null}
        </FormGroup>
      </Fragment>
    );
  }
}

export default InputForm;
