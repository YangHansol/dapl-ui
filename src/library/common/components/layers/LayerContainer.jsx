import React, { Component } from "react";
import { LayerM } from "../../style/StyleModal";

class LayerContainer extends Component {
  componentWillUnmount() {
    document.body.classList.remove("non-scroll");
  }

  componentDidUpdate(prevProps, prevState) {
    document.body.classList.add("non-scroll");
  }

  componentDidMount() {
    document.body.classList.add("non-scroll");
  }

  render() {
    return (
      <LayerM.Wrapper className="layer">
        {React.cloneElement(this.props.children)}
      </LayerM.Wrapper>
    );
  }
}

export default LayerContainer;
