import React, { Component } from "react";
import LayerPopup from "library/layerpopup/LayerPopup";
import { Modal } from "react-bootstrap";
import { CustomButton } from "../../style/StyleButton";
import { LayerPopupStyle } from "../../style/StyleModal";

class ConfirmModal extends Component {
  onClick = (e, type) => {
    e.preventDefault();
    let { callback, params } = this.props;
    let resultCallback = false;
    if (callback) {
      resultCallback = callback(type, params);
      if (resultCallback === undefined) {
        resultCallback = true;
      }
    } else {
      resultCallback = true;
    }
    if (resultCallback && this.props.layerKey) {
      LayerPopup.hide(this.props.layerKey);
    }
  };
  render() {
    let { message, buttonLabel1 = "아니오", buttonLabel2 = "예" } = this.props;
    return (
      <LayerPopupStyle.Wrap className="layer-wrap">
        <LayerPopupStyle.Content>
          <LayerPopupStyle.Header className="modal-header">
            <Modal.Title>확인</Modal.Title>
            <button
              className="btn-modal-close"
              onClick={e => this.onClick(e, "CANCEL")}
            >
              닫기
            </button>
          </LayerPopupStyle.Header>
          <Modal.Body>
            <p dangerouslySetInnerHTML={{ __html: message }} />
          </Modal.Body>
          <LayerPopupStyle.Footer className="text-center">
            <CustomButton.Styled
              // style={{ margin: "0 0 0 5px" }}
              btStyle="noColor"
              colwidth="120px"
              onClick={e => this.onClick(e, "CLOSE")}
            >
              {buttonLabel1}
            </CustomButton.Styled>
            <CustomButton.Styled
              // style={{ margin: "0 0 0 5px" }}
              colwidth="120px"
              onClick={e => this.onClick(e, "OK")}
            >
              {buttonLabel2}
            </CustomButton.Styled>
          </LayerPopupStyle.Footer>
        </LayerPopupStyle.Content>
      </LayerPopupStyle.Wrap>
    );
  }
}

export default ConfirmModal;
