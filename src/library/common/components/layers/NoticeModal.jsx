import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import LayerPopup from "library/layerpopup/LayerPopup";
import { LayerPopupStyle } from "../../style/StyleModal";
import { CustomButton } from "../../style/StyleButton";

class ConfirmModal extends Component {
  onClick = (e, type) => {
    e.preventDefault();
    if (type === "CLOSE" || type === "XCLOSE") {
      let { callback, params } = this.props;
      let resultCallback = false;

      if (callback) {
        resultCallback = callback(type, params);
        if (resultCallback === undefined) {
          resultCallback = true;
        }
      } else {
        resultCallback = true;
      }
      if (resultCallback && this.props.layerKey) {
        LayerPopup.hide(this.props.layerKey);
      }
    }
  };

  render() {
    let { message, buttonLabel1 = "확인" } = this.props;
    return (
      <LayerPopupStyle.Wrap className="layer-wrap">
        <LayerPopupStyle.Content>
          <LayerPopupStyle.Header className="modal-header">
            <Modal.Title>안내</Modal.Title>
            <button
              className="btn-modal-close"
              onClick={e => this.onClick(e, "CLOSE")}
            >
              닫기
            </button>
          </LayerPopupStyle.Header>
          <Modal.Body>
            <p dangerouslySetInnerHTML={{ __html: message }} />
          </Modal.Body>
          <LayerPopupStyle.Footer className="text-center">
            <CustomButton.Styled
              // style={{ margin: "0 0 0 5px" }}
              colwidth="120px"
              onClick={e => this.onClick(e, "CLOSE")}
              ref={ref => (this.noticeBtn = ref)}
            >
              {buttonLabel1}
            </CustomButton.Styled>
          </LayerPopupStyle.Footer>
        </LayerPopupStyle.Content>
      </LayerPopupStyle.Wrap>
    );
  }
}

export default ConfirmModal;
