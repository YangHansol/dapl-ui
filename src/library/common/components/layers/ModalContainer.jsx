import React, { Component } from "react";
import { LayerM } from "../../style/StyleModal";

class ModalContainer extends Component {
  componentWillUnmount() {
    document.body.classList.remove("non-scroll");
  }
  componentDidUpdate(prevProps, prevState) {
    document.body.classList.add("non-scroll");
  }
  componentDidMount() {
    document.body.classList.add("non-scroll");
  }

  render() {
    const newProps = {
      location: this.props.location,
      history: this.props.history,
      layerKey: this.props.layerKey,
      layerCount: this.props.layerCount
    };

    return (
      <LayerM.Layer zIndex={newProps.layerKey}>
        {React.cloneElement(this.props.children, newProps)}
      </LayerM.Layer>
    );
  }
}

export default ModalContainer;
