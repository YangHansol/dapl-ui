import React from "react";
import Spinner from "../Spinner";
import { Button } from "react-bootstrap";
import { InOperation } from "../style/StyleComponents";

const SpinOperation = () => {
  return (
    <InOperation>
      <Spinner size={40} customMargin={0} operation />
      <span>현재 실험 중 입니다.</span>
    </InOperation>
  );
};

export default SpinOperation;
