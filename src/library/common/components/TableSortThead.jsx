import React, { Component } from "react";
import classnames from "classnames";

import TooltipBox from "./TooltipBox";

class TableSortThead extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSort = (index, type) => {
    const cells = Object.values(this.props.data);
    cells.forEach((cell, index) => {
      if (cell.sortBy !== type) {
        this.setState({
          [cell.sortBy]: false
        });
      }
    });

    if (cells[index].sortBy !== null) {
      let query = {
        ...this.props.location.query,
        sort: `${type}.${this.state[type] ? "asc" : "desc"}`
      };
      this.setState({
        [type]: !this.state[type]
      });
      this.props.getListFunc(query);
    }
  };

  componentDidMount() {
    const cells = Object.values(this.props.data);
    cells.forEach((cell, index) => {
      this.setState({
        [cell.sortBy]: false
      });
    });
  }

  render() {
    const { data = [] } = this.props;
    const cell = Object.values(this.props.data) || [];

    return (
      <tr>
        {data.map((col, index) => {
          return (
            <th
              key={index}
              className={`sort ${col.sortBy === null ? "non-cursor" : ""}${
                col.align
              }`}
            >
              <span onClick={() => this.handleSort(index, col.sortBy)}>
                <TooltipBox id="thNm" fullName={col.title} bulWidth>
                  {col.title}
                </TooltipBox>
                {col.sortBy !== null && (
                  <i
                    className={classnames({
                      "arrow-sort-desc": this.state[cell[index].sortBy],
                      "arrow-sort-asc":
                        !this.state[cell[index].sortBy] || false,
                      "non-cursor": col.sortBy === null
                    })}
                  />
                )}
              </span>
            </th>
          );
        })}
      </tr>
    );
  }
}

export default TableSortThead;
