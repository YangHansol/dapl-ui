import React from "react";

const ConditionContainer = props => {
  if (!props.visible) {
    return null;
  }
  return React.cloneElement(props.children);
};

export default ConditionContainer;
