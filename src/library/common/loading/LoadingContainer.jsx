import React, { Component } from "react";
import { connect } from "react-redux";
import LoadingBox from "./LoadingBox";
class LoadingContainer extends Component {
  render() {
    const { isLoading, isProgress } = this.props;
    if (isLoading) {
      return (<LoadingBox isProgress={isProgress} />)
    } else {
      return null
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    isLoading: state.LoadingReducer.loading
  };
};
export default connect(mapStateToProps)(LoadingContainer);
