import React, { Component } from "react";
import Spinner from "../Spinner";
import { LoadingWrap } from "../style/StyleComponents";

class LoadingBox extends Component {
  componentWillUnmount() {
    document.body.classList.remove("non-scroll");
  }
  componentDidUpdate(prevProps, prevState) {
    document.body.classList.add("non-scroll");
  }
  componentDidMount() {
    document.body.classList.add("non-scroll");
  }

  render() {
    return (
      <LoadingWrap>
        <div className="loadingPanel">
          <Spinner size={60} customMargin={"15px auto"} />
          <p>잠시만 기다려 주세요.</p>
        </div>
      </LoadingWrap>
    );
  }
}

export default LoadingBox;
