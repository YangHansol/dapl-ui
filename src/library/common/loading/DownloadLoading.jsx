import React from "react";
import { ProgressBar } from "react-bootstrap";
import { ProgressContainer, ProgressBox } from "../style/StyleComponents";

const DownloadLoading = ({ progressText, progress }) => {
  return (
    <ProgressContainer>
      <ProgressBox>
        <h5>{progressText}</h5>
        <span>잠시만 기다려 주세요. ({progress}/100%)</span>
        <ProgressBar
          active
          now={progress ? progress : 0}
          label={`${progress}`}
        />
      </ProgressBox>
    </ProgressContainer>
  );
};

export default DownloadLoading;
