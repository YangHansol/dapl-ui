import httpErrorMessage from "../httpErrorMessage";

export const ActionTypes = Object.freeze({
  ERROR: "ERROR",
  LOGIN_ERROR: "LOGIN_ERROR",
  CLEAR_ERROR: "CLEAR_ERROR"
});

export function clearError() {
  return {
    type: ActionTypes.CLEAR_ERROR
  };
}

export function Error(err, message) {
  return (dispatch, getState) => {
    if (getState().ErrorReducer.err === null) {
      return dispatch({
        type: ActionTypes.ERROR,
        payload: {
          err,
          message: httpErrorMessage(err, message)
        }
      });
    }
  };
}

export function LoginError(loginErr, loginErrMessage) {
  return (dispatch, getState) => {
    if (getState().ErrorReducer.loginErr === null) {
      return dispatch({
        type: ActionTypes.LOGIN_ERROR,
        payload: {
          loginErr,
          loginErrMessage: httpErrorMessage(loginErr, loginErrMessage)
        }
      });
    }
  };
}
