import React, { Component } from "react";
import { Modal, Grid, Row, Col, Button } from "react-bootstrap";
import TextAreaFrom from "../../../common/components/TextAreaForm";
import { LayerPopupStyle } from "../../../common/style/StyleModal";

class ModalLongError extends Component {
  onClose = () => {
    this.props.onClose();
  };
  render() {
    const { message } = this.props;
    return (
      <LayerPopupStyle.ModalWrap className="layer-wrap">
        <LayerPopupStyle.ModalContent>
          <LayerPopupStyle.Header className="modal-header" border>
            <Modal.Title>패키지 설치 오류</Modal.Title>
            <button className="btn-modal-close" onClick={this.onClose}>
              닫기
            </button>
          </LayerPopupStyle.Header>
          <LayerPopupStyle.ModalBody className="modal-body">
            <Grid fluid>
              <Row>
                <Col sm={12}>
                  <TextAreaFrom
                    disabled
                    id="errorLog"
                    name="errorLog"
                    value={message}
                    minHeight={"400px"}
                    inputRef={ref => (this.message = ref)}
                  />
                </Col>
              </Row>
            </Grid>
          </LayerPopupStyle.ModalBody>
          <Modal.Footer className="modal-footer">
            <Button onClick={this.onClose}>닫기</Button>
          </Modal.Footer>
        </LayerPopupStyle.ModalContent>
      </LayerPopupStyle.ModalWrap>
    );
  }
}

export default ModalLongError;
