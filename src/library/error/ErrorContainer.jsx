import React, { Component } from "react";
import { connect } from "react-redux";
import Popup from "../../common/Popup";
import ModalLongError from "./components/ModalLongError";
import { withRouter } from "react-router";
import * as ErrorAction from "./ErrorAction";

class ErrorContainer extends Component {
  openModalError = message => {
    this.errorModal = Popup.modal(
      <ModalLongError message={message} onClose={this.onCloseModal} />
    );
  };

  onCloseModal = () => {
    this.props.clearError();
    Popup.hide(this.errorModal);
  };

  componentDidUpdate(prevProps, prevState) {
    const { err, message } = this.props;

    if (err) {
      if (
        err.response &&
        err.response.status >= 400 &&
        err.response.status < 500
      ) {
        if (
          err.response &&
          err.response.data &&
          (err.response.data.code === "C9403" ||
            err.response.data.code === "0301")
        ) {
          this.props.clearError();
          sessionStorage.removeItem("name");
          sessionStorage.removeItem("id");
          sessionStorage.removeItem("tkn");
          this.props.history.push("/?pagename=member/login");
        } else {
          Popup.notice({
            message: message,
            callback: () => {
              this.props.clearError();
            }
          });
        }
      } else if (err.response && err.response.status >= 500) {
        if (
          err.response &&
          err.response.data &&
          err.response.data.code === "2001"
        ) {
          this.openModalError(message);
        } else {
          Popup.notice({
            message: message,
            callback: () => {
              this.props.clearError();
            }
          });
        }
      } else {
        Popup.notice({
          message: message,
          callback: () => {
            this.props.clearError();
          }
        });
      }
    }
  }

  render() {
    return null;
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    clearError: () => {
      dispatch(ErrorAction.clearError());
    }
  };
};
const mapStateToProps = (state, ownProps) => {
  return {
    err: state.ErrorReducer.err,
    message: state.ErrorReducer.message
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ErrorContainer)
);
