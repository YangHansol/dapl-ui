import { ActionTypes as types } from "./ErrorAction";

const initialState = {
  err: null,
  loginErr: null,
  message: "",
  loginErrMessage: ""
};

export default function ErrorReducer(state = initialState, action) {
  switch (action.type) {
    case types.CLEAR_ERROR:
      return Object.assign({}, state, {
        err: null,
        loginErr: null,
        message: initialState.message,
        loginErrMessage: initialState.loginErrMessage
      });
    case types.ERROR:
      return Object.assign({}, state, {
        err: action.payload.err,
        message: action.payload.message
      });
    case types.LOGIN_ERROR:
      return Object.assign({}, state, {
        loginErr: action.payload.loginErr,
        loginErrMessage: action.payload.loginErrMessage
      });
    default:
      return state;
  }
}
