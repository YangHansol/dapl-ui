import {
  ExperimentStatus,
  WorkspaceStatusCode,
  UserStatusCode,
  PackageStatus
} from "../constants/Code";

class Utils {
  static numberToSize(info) {
    if (info >= 1000) {
      return info / 1000 + "TB";
    } else {
      return info + "GB";
    }
  }

  static formatBytes(bytes, decimals) {
    if (!bytes) return "0 Byte";
    let k = 1024;
    let dm = decimals || 2;
    let size = ["Bytes", "KB", "MB", "GB", "TB"];
    let i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + size[i];
  }

  static statusCircle(status) {
    if (status === ExperimentStatus.PPR) {
      return "warning";
    } else if (status === ExperimentStatus.RUN) {
      return "primary";
    } else if (status === ExperimentStatus.END) {
      return "success";
    } else if (status === ExperimentStatus.CNCL) {
      return "muted";
    } else if (status === WorkspaceStatusCode.ACT) {
      return "success";
    } else if (status === WorkspaceStatusCode.WT) {
      return "warning";
    } else if (status === WorkspaceStatusCode.BACKUP) {
      return "muted";
    } else if (status === UserStatusCode.ACT) {
      return "success";
    } else if (status === UserStatusCode.STP) {
      return "danger";
    } else if (status === UserStatusCode.WT) {
      return "warning";
    } else if (status === PackageStatus.ERR) {
      return "danger";
    } else if (status === "Y") {
      return "success";
    } else if (status === "N") {
      return "danger";
    } else if (status === "1") {
      return "success";
    } else if (status === "0") {
      return "danger";
    } else {
      return "muted";
    }
  }

  static isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
}

export default Utils;
