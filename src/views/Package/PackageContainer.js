import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Table from "../../components/Table/Table.js";
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardBody from "../../components/Card/CardBody.js";
import Pagination from "material-ui-flat-pagination";

const styles = {
  cardCategoryWhite: {
    color: "#fff",
    fontSize: "14px",
    marginLeft: "3px"
  },

  cardTitleWhite: {
    display: "flex",
    alignItmes: "center",
    color: "#fff",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "700",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "0",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

export default function PackageContainer() {
  const classes = useStyles();
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              라이브러리<span className={classes.cardCategoryWhite}>(2)</span>
            </h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["이름", "CPU", "GPU", "디스크", "메모리"]}
              tableData={[
                ["Admin(5)", "2", "8", "100GB", "8GB"],
                ["User(5)", "1", "3", "20GB", "1GB"]
              ]}
            />
          </CardBody>
          <Pagination
            limit={10}
            offset={0}
            total={100}
            currentPageColor={"primary"}
            otherPageColor={"default"}
            style={{ textAlign: "center", marginBottom: "10px" }}
          />
        </Card>
      </GridItem>
    </GridContainer>
  );
}
