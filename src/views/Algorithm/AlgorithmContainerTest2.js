import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { connect } from "react-redux";
// import { Pagination } from "react-bootstrap";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Table from "../../components/Table/Table.js";
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardBody from "../../components/Card/CardBody.js";
import Pagination from "material-ui-flat-pagination";

import { ShareText, AlgoTypeText } from "../../utils/code";
import { AlgoTh } from "../../state/constants/TableHead";
import DateViewer from "../../components_/Date/DateViewer";

// state
import * as AlgorithmAction from "../../state/moduels/actions/AlgorithmAction";
import * as CommonAction from "../../state/moduels/actions/CommonAction";
import { generateQuery } from "library/GenerateQuery";
import { getAlgorithmList } from "../../state/moduels/actions/AlgorithmAction";

import Switch from "@material-ui/core/Switch";

// import moment from "moment";

const styles = {
  cardCategoryWhite: {
    color: "#fff",
    fontSize: "14px",
    marginLeft: "3px"
  },

  cardTitleWhite: {
    display: "flex",
    alignItmes: "center",
    color: "#fff",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "700",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "0",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

function AlgorithmContainerTest2({
  update,
  algorithmListInfo,
  initAction,
  deleteAlgo,
  getListForPaging,
  getCommonList,
  updateAlgo,
  location
}) {
  const classes = useStyles();

  // useSelector, useDispatch
  // useSelector 를 아직 못 씀. 먼저 정리한 후 hooks 를 사용할 수 있게 점차 변경.
  // const algorithmList = useSelector(
  //   state => state.AlgorithmReducer.algorithmList
  // );
  // const { totalElements, content } = algorithmList;
  // algorithmListInfo 에 데이터가 안들어옴. -> loadList(); 를 통해 데이터 요청하고, 그것을 통해 데이터 전달.
  // const dispatch = useDispatch();
  // const algorithmDispatch = useCallback(
  //   () =>
  //     dispatch(getAlgorithmList({ number: 0, size: 10, sort: "regDt.desc" })),
  //   [dispatch]
  // );
  // useEffect(() => {
  //   algorithmDispatch();
  // }, [algorithmDispatch]);

  // 기존 코드
  const { totalElements, content, totalPages } = algorithmListInfo;
  // console.log(algorithmListInfo);

  // const { totalElements, totalPages } = algorithmListInfo;
  // const pageNo = parseInt(this.props.location.query.number, 10) + 1;
  // let activePage = pageNo ? pageNo : 1;

  const loadList = useCallback(() => {
    const query = { ...location.query };
    // console.log(query);
    if (query.searchType) {
      query[query.searchType] = query.searchText;
    }
    if (!query.sort) {
      query.sort = "regDt.desc";
    }
    initAction();
    getCommonList(query);
  }, [getCommonList, initAction, location.query]);

  useEffect(() => {
    loadList();
    // loadListUpdate();
  }, [loadList, update, location.query]);

  const onClickSetFl = (e, id) => {
    e.preventDefault();
    const pubFl = e.target.checked;
    // console.log(pubFl);
    const postData = {
      athmId: id,
      pubFl: pubFl === true ? "Y" : "N"
    };

    updateAlgo(postData);
  };

  const refresh = () => {
    // 여기서 refreshList 는 AlgorithmContainer 에서 전달한 loadList() 이고,
    // item 은 뭔지 확인 안됨.
    // 아마 그냥 loadList() 를 다시 로딩 하는 것으로 충분할 듯 하나 확인 필요.
    // const { item, refreshList } = this.props;
    // refreshList(item);
    loadList();
  };

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              알고리즘
              <span className={classes.cardCategoryWhite}>
                {!!totalElements ? `(${totalElements})` : `(0)`}
              </span>
            </h4>
            <button onClick={refresh}>새로고침</button>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={AlgoTh}
              tableData={
                !!content
                  ? content.map(data => {
                      return [
                        // 이름
                        data.athmNm,
                        // 구분
                        AlgoTypeText[data.athmTp],
                        // 저장소
                        data.rptrNm,
                        // 설명
                        data.athmDesc,
                        // 공개 여부
                        <Switch
                          id={`pubFl${data.athmId}`}
                          name={`pubFl${data.athmId}`}
                          checked={data.pubFl === "Y"}
                          onChange={e => {
                            onClickSetFl(e, data.athmId);
                          }}
                          color="primary"
                          inputProps={{ "aria-label": "primary checkbox" }}
                        />,
                        // 생성자
                        data.ownerNm + `(${data.ownerId})`,
                        // 수정일자
                        <DateViewer date={data.modDt} />,
                        // 삭제
                        <button>삭제</button>
                        // moment(algo.modDt).format(DateFormat.full)
                      ];
                    })
                  : [["-", "-", "-", "-", "-", "-", "-"]]
              }
            />
          </CardBody>
          <Pagination
            limit={10}
            offset={0}
            total={100}
            currentPageColor={"primary"}
            otherPageColor={"default"}
            style={{ textAlign: "center", marginBottom: "10px" }}
          />
        </Card>
      </GridItem>
    </GridContainer>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    update: state.AlgorithmReducer.action === "UPDATE",
    algorithmListInfo: state.CommonReducer.algorithm
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const {
    listPage = "algorithm/algorithmlist",
    pageSize = 10,
    pageCategory = "algorithm"
  } = ownProps;
  return {
    initAction: () => {
      dispatch(CommonAction.initAction());
    },

    deleteAlgo: athmId => {
      dispatch(AlgorithmAction.deleteAlgorithm(athmId));
    },

    getListForPaging: pageQuery => {
      let newQuery = {};
      if (pageQuery) {
        newQuery = {
          pagename: listPage,
          ...pageQuery
        };
      } else {
        newQuery = {
          pagename: listPage
        };
      }
      ownProps.history.push(generateQuery(newQuery));
    },

    getCommonList: pageQuery => {
      const pageNumber = pageQuery && pageQuery.number ? pageQuery.number : 0;
      dispatch(
        CommonAction.getCommonList(
          pageCategory,
          pageNumber,
          pageSize,
          pageQuery
        )
      );
    },

    updateAlgo: postData => {
      dispatch(AlgorithmAction.updateAlgorithm(postData));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AlgorithmContainerTest2);
