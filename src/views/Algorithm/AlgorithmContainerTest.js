import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Table from "../../components/Table/Table.js";
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardBody from "../../components/Card/CardBody.js";
import Pagination from "material-ui-flat-pagination";

import {
  getAlgorithmList,
  updateAlgorithm
} from "../../state/moduels/actions/AlgorithmAction";
import { ShareText, AlgoTypeText } from "../../utils/code";
import { AlgoTh } from "../../state/constants/TableHead";
import DateViewer from "../../components_/Date/DateViewer";
import useAlgoritmList from "state/hooks/useAlgorithmList";
import Switch from "@material-ui/core/Switch";

// import moment from "moment";

const styles = {
  cardCategoryWhite: {
    color: "#fff",
    fontSize: "14px",
    marginLeft: "3px"
  },

  cardTitleWhite: {
    display: "flex",
    alignItmes: "center",
    color: "#fff",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "700",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "0",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

export default function AlgorithmContainerTest() {
  const classes = useStyles();

  const [toggleState, setToggleState] = React.useState(false);

  const dispatch = useDispatch();
  const algorithmDispatch = useCallback(
    () =>
      dispatch(getAlgorithmList({ number: 0, size: 10, sort: "regDt.desc" })),
    [dispatch]
  );

  const updateAlgo = useCallback(
    postData => {
      dispatch(updateAlgorithm(postData));
    },
    [dispatch]
  );

  useEffect(() => {
    algorithmDispatch();
  }, [algorithmDispatch]);

  const onClickSetFl = (e, id) => {
    e.preventDefault();
    const pubFl = e.target.checked;
    // console.log(pubFl);
    const postData = {
      athmId: id,
      pubFl: pubFl === true ? "Y" : "N"
    };

    updateAlgo(postData);
  };

  const algorithmList = useSelector(state => state.algorithm.algorithmList);
  const update = useSelector(
    state => state.AlgorithmReducer.action === "UPDATE"
  );
  const { totalElements, content } = algorithmList;

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              알고리즘
              <span className={classes.cardCategoryWhite}>
                {!!totalElements ? `(${totalElements})` : `(0)`}
              </span>
            </h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={AlgoTh}
              tableData={
                !!content
                  ? content.map(algo => {
                      return [
                        // 이름
                        algo.athmNm,
                        // 구분
                        AlgoTypeText[algo.athmTp],
                        // 저장소
                        algo.rptrNm,
                        // 설명
                        algo.athmDesc,
                        // 공개 여부
                        <Switch
                          id={`pubFl${algo.athmId}`}
                          name={`pubFl${algo.athmId}`}
                          checked={algo.pubFl === "Y"}
                          onChange={e => {
                            onClickSetFl(e, algo.athmId);
                          }}
                          color="primary"
                          inputProps={{ "aria-label": "primary checkbox" }}
                        />,
                        // 생성자
                        algo.ownerNm + `(${algo.ownerId})`,
                        // 수정일자
                        <DateViewer date={algo.modDt} />,
                        // 삭제
                        <button>삭제</button>
                        // moment(algo.modDt).format(DateFormat.full)
                      ];
                    })
                  : [["-", "-", "-", "-", "-", "-", "-"]]
              }
            />
          </CardBody>
          <Pagination
            limit={10}
            offset={0}
            total={100}
            currentPageColor={"primary"}
            otherPageColor={"default"}
            style={{ textAlign: "center", marginBottom: "10px" }}
          />
        </Card>
      </GridItem>
    </GridContainer>
  );
}
