import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Table from "../../components/Table/Table.js";
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardBody from "../../components/Card/CardBody.js";
import Pagination from "material-ui-flat-pagination";

import { getAlgorithmList } from "../../state/moduels/actions/AlgorithmAction";
import { ShareText, AlgoTypeText } from "../../utils/code";
import { AlgoTh } from "../../state/constants/TableHead";
import DateViewer from "../../components_/Date/DateViewer";
import useAlgoritmList from "state/hooks/useAlgorithmList";
// import moment from "moment";

const styles = {
  cardCategoryWhite: {
    color: "#fff",
    fontSize: "14px",
    marginLeft: "3px"
  },

  cardTitleWhite: {
    display: "flex",
    alignItmes: "center",
    color: "#fff",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "700",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "0",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

export default function AlgorithmContainer() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const algorithmDispatch = useCallback(
    () =>
      dispatch(getAlgorithmList({ number: 0, size: 10, sort: "regDt.desc" })),
    [dispatch]
  );

  useEffect(() => {
    algorithmDispatch();
  }, [algorithmDispatch]);

  const algorithmList = useSelector(
    state => state.AlgorithmReducer.algorithmList
  );
  const { totalElements, content } = algorithmList;

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>
              알고리즘
              <span className={classes.cardCategoryWhite}>
                {!!totalElements ? `(${totalElements})` : `(0)`}
              </span>
            </h4>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={AlgoTh}
              tableData={
                !!content
                  ? content.map(algo => {
                      return [
                        algo.athmNm,
                        AlgoTypeText[algo.athmTp],
                        algo.rptrNm,
                        algo.athmDesc,
                        ShareText[algo.pubFl],
                        algo.ownerNm + `(${algo.ownerId})`,
                        <DateViewer date={algo.modDt} />
                        // moment(algo.modDt).format(DateFormat.full)
                      ];
                    })
                  : [["-", "-", "-", "-", "-", "-", "-"]]
              }
            />
          </CardBody>
          <Pagination
            limit={10}
            offset={0}
            total={100}
            currentPageColor={"primary"}
            otherPageColor={"default"}
            style={{ textAlign: "center", marginBottom: "10px" }}
          />
        </Card>
      </GridItem>
    </GridContainer>
  );
}
