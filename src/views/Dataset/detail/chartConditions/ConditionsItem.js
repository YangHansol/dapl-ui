import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

import styled from "styled-components";

// connect

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: 150,
    },
  },
  formControl: {
    margin: theme.spacing(1),
    Width: 150,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const style = {
  Wrapper: styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  `,
  ItemWrapper: styled.div`
    width: 500px;
  `,
  InputWrapper: styled.div`
    display: flex;
    align-items: center;
    &.Left {
      justify-content: flex-end;
    }
  `,
  InputTitle: styled.div``,
  AddButton: styled.button`
    margin: 0;
    padding: 0;
    border: none;
    background: none;
    color: ${props => props.theme.PrimaryColor};
    margin-left: 0.5rem;
    font-weight: bolder;
  `,
  FormControl: styled(FormControl)`
    min-width: 150px;
  `,
};

const ConditionsItem = ({ chartTp, chartValue, onEdit }) => {
  const classes = useStyles();

  const onSubmit = e => {
    e.preventDefault();
  };
  console.log(chartTp, chartValue, onEdit);

  return (
    <>
      <style.InputWrapper>
        <style.FormControl name="chartTp" className={classes.formControl}>
          <InputLabel id="demo-simple-select-label">차트 종류</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            name="chartTp"
            value={chartTp}
            onChange={onEdit}
            inputProps={{
              name: "chartTp",
            }}
          >
            {chartTp &&
              chartTp.map(item => (
                <MenuItem value={item} key={item}>
                  {item}
                </MenuItem>
              ))}
          </Select>
        </style.FormControl>

        <style.FormControl name="xValue" className={classes.formControl}>
          <InputLabel id="demo-simple-select-label">행</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            name="xValue"
            value={chartValue}
            onChange={onEdit}
            inputProps={{
              name: "xValue",
            }}
          >
            {chartValue &&
              chartValue.map(item => (
                <MenuItem value={item} key={item}>
                  {item}
                </MenuItem>
              ))}
          </Select>
        </style.FormControl>
        <style.FormControl name="yValue" className={classes.formControl}>
          <InputLabel id="demo-simple-select-label">열</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            name="yValue"
            value={chartValue}
            onChange={onEdit}
            inputProps={{
              name: "yValue",
            }}
          >
            {chartValue &&
              chartValue.map(item => (
                <MenuItem value={item} key={item}>
                  {item}
                </MenuItem>
              ))}
          </Select>
        </style.FormControl>
        {/* <style.FormControl name="value" className={classes.formControl}>
            <form
              className={classes.root}
              noValidate
              autoComplete="off"
              noValidate
              onSubmit={onSubmit}
            >
              <TextField
                id="standard-basic"
                label="값"
                name="value"
                onChange={onEdit(item.id)}
                inputProps={{
                  name: "value"
                }}
              />
            </form>
          </style.FormControl> */}
        {/* <style.AddButton onClick={() => onRemove(item.id)}>X</style.AddButton> */}
      </style.InputWrapper>
      {/* // )) */}
    </>
  );
};

export default React.memo(ConditionsItem);
