import React from "react";
import ConditionsItem from "./ConditionsItem";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  /* justify-content: center; */
  align-items: center;
  width: max-content;
`;

function ConditionsLists(conditions) {
  console.log("conditions lists: ", conditions);
  return (
    <div>
      <Wrapper>
        <ConditionsItem conditions={conditions} />
      </Wrapper>
    </div>
  );
}

export default React.memo(ConditionsLists);
