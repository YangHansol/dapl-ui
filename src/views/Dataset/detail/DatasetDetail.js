/*eslint-disable*/
import React, { useState, useEffect, useCallback, useRef } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// import Hidden from "@material-ui/core/Hidden";
// core components
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import Table from "components/Table/Table.js";
import DatasetTemporaryDrawer from "../../../components/customUI/SideMenu/DatasetTemporaryDrawer.js";
import styled from "styled-components";
import { Link, withRouter } from "react-router-dom";
import useDatasetSelector from "../../../state/hooks/useDatasetSelector";
import useDatasetDispatch from "../../../state/hooks/useDatasetDispatch";
import DataVisualization from "../dataVisualization/DataVisualization.js";
import {
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  TextField,
  Modal,
  Backdrop,
  Fade,
  Grid,
  TableCell,
  CardContent,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import CloseIcon from "@material-ui/icons/Close";
import moment from "moment";

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    width: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #E1E1E1",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    borderRadius: "8px",
  },
}));

export const StyledFlex = styled.div`
  display: flex;
  justify-content: ${props => props.justifyprops};
  align-items: baseline;
`;

const StyledCardTitle = styled.h4`
  font-weight: 500;
  font-size: 1.7rem;
  display: flex;
  align-items: center;
`;

const StyledCardBodyInfo = styled.div`
  display: grid;
  grid-template-columns: 2fr 8fr;
  align-items: center;
  font-size: 1rem;
  font-weight: 500;
  margin-bottom: 2rem;
`;

const StyeldCardBody = styled(CardBody)`
  margin-bottom: 1.2rem;
`;

const StyledCardBodyTitle = styled.div`
  font-size: 1rem;
  font-weight: 500;
  margin-bottom: 1rem;
`;

const ChartWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  margin-top: 1rem;
  &.small {
    flex-direction: row;
  }
`;

const ChartSize = styled.div`
  /* width: 1000px;
  height: 500px;
  &.small {
    width: 380px;
    height: 380px;
  } */
  width: 380px;
  height: 380px;
  display: flex;
  justify-content: center;
  align-items: center;
  &.big {
    width: 1000px;
    height: 500px;
    flex-direction: column;
  }
`;

const ChartSetWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const ChartEachWrapper = styled.div`
  border: ${props => props.theme.Border};
`;

const ChartInfo = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 1.2rem;
  padding: 0 1rem;
  margin: 0.2rem 0;
  border-bottom: 1px solid #e1e1e1;
  height: 2.6rem;
  &.chartTitle {
    font-size: 2rem;
    border-bottom: none;
    padding-top: 0.5rem;
  }

  &.chartInfo {
    padding-top: 0.5rem;
    justify-content: flex-start;
    align-items: baseline;
  }
`;

const style = {
  Card: styled(Card)`
    box-shadow: unset;
    border: 1px solid ${props => props.theme.GrayColor};
  `,
  CardContent: styled(CardContent)`
    &.CardContentHeader {
      display: flex;
      justify-content: space-between;
      align-items: center;
      border-bottom: 1px solid ${props => props.theme.GrayColor};
    }
  `,

  TableCell: styled(TableCell)`
    padding: 0.8rem;
    &.TableHeadCell {
      background-color: ${props => props.theme.GrayLightColor};
    }
  `,
};

function DatasetDetail({ history, match }) {
  const {
    params: { dataId },
  } = match;
  const classes = useStyles();

  const {
    datasetRead,
    datasetCreateChart: getDatasetCreateChart,
  } = useDatasetSelector();
  const {
    getDatasetRead,
    initAction,
    datasetCreateChart,
    datasetDeleteChart,
  } = useDatasetDispatch({ history });

  // chart size
  const loadList = useCallback(() => {
    initAction();
    getDatasetRead(dataId);
  }, []);

  useEffect(() => {
    loadList();
  }, [dataId]);

  const PUBFL = {
    Y: "공개",
    N: "비공개",
    P: "미공개(개인정보포함)",
  };

  // datasetInfos for table data
  const datasetInfos = datasetRead && [
    `${datasetRead.dataTp ? datasetRead.dataTp : "-"}`,
    `${
      datasetRead.databaseInfo && datasetRead.databaseInfo.dbNm
        ? datasetRead.databaseInfo.dbNm
        : "-"
    }`,
    `${datasetRead.fileSz ? datasetRead.fileSz : "-"} Bytes`,
    "...",
    `${PUBFL[datasetRead.pubFl] ? PUBFL[datasetRead.pubFl] : "-"}`,
    `${datasetRead.regNm ? datasetRead.regNm : "-"}`,
    `${datasetRead.ownerNm ? datasetRead.ownerNm : "-"}`,
  ];

  // metaInfos for table data
  const metaInfos =
    datasetRead &&
    datasetRead.metaInfos &&
    datasetRead.metaInfos.map(item => [
      `${item.columnNm ? item.columnNm : "-"}`,
      `${item.columnTp ? item.columnTp : "-"}`,
      `${item.minValue ? item.minValue : "-"}`,
      `${item.maxValue ? item.maxValue : "-"}`,
      `${item.avgValue ? item.avgValue : "-"}`,
      `${item.notNullCnt ? item.notNullCnt : "-"}`,
      `${item.nullRate !== "" ? `${item.nullRate}  %` : "-"}`,
      `-`,
    ]);

  // chartTp for chart select
  const chartTp = [
    "line",
    "bar",
    "scatterplot",
    // "histogram",
    // "box plot",
    // "simpleBar",
  ];

  // state for chart
  const [chartNm, setChartNm] = useState("");
  const [chartTpItem, setChartTpItem] = useState("");
  const [idValueItem, setIdValueItem] = useState("");
  const [xValueItem, setXvalueItem] = useState("");
  const [yValueItem, setYValueItem] = useState("");
  const [chartDatasProps, setChartDatasProps] = useState([]);
  const [chartConditions, setChartConditions] = useState([]);
  const [chartPostData, setChartPostData] = useState("");
  const nextId = useRef(0);

  const chartDatas =
    datasetRead &&
    datasetRead.charts &&
    datasetRead.charts.map(item => ({
      ...item,
      conditions: JSON.parse(item.conditions),
      axis: JSON.parse(item.axis),
    }));

  useEffect(() => {
    // 마운트 된 후에 datasetRead 를 통해 가져온 값을 chartDatasProps 에 담음.
    setChartDatasProps(chartDatas);
  }, [datasetRead.charts]);

  const onChangeChartNm = e => {
    setChartNm(e.target.value);
  };
  const onChangeChartTp = e => {
    setChartTpItem(e.target.value);
  };
  const onChangeIdValue = e => {
    setIdValueItem(e.target.value);
  };
  const onChangeXValue = e => {
    setXvalueItem(e.target.value);
  };
  const onChangeYValue = e => {
    setYValueItem(e.target.value);
  };

  function settingChartPostData(chartTpItem) {
    const dataNm = datasetRead.dataNm && datasetRead.dataNm;
    const dataId = datasetRead.dataId && datasetRead.dataId;

    if (chartTpItem === "line") {
      setChartPostData({
        dataId: dataId,
        chartNm: chartNm,
        chartTp: chartTpItem,
        axis: JSON.stringify({
          xAxis: xValueItem,
          yAxis: yValueItem,
        }),
        conditions: JSON.stringify({
          id: dataNm,
          data:
            datasetRead &&
            datasetRead.datas &&
            datasetRead.datas.slice(0, 8).map(item => ({
              x: item[xValueItem],
              y: item[yValueItem],
            })),
        }),
      });
    }

    if (chartTpItem === "bar") {
      setChartPostData({
        dataId: dataId,
        chartNm: chartNm,
        chartTp: chartTpItem,
        axis: JSON.stringify({
          xAxis: xValueItem,
          yAxis: yValueItem,
        }),
        conditions: JSON.stringify({
          id: dataNm,
          data:
            datasetRead &&
            datasetRead.datas &&
            datasetRead.datas.slice(0, 8).map(item => ({
              x: item[xValueItem],
              y: item[yValueItem],
            })),
        }),
      });
    }

    if (chartTpItem === "scatterplot") {
      setChartPostData({
        dataId: dataId,
        chartNm: chartNm,
        chartTp: chartTpItem,
        axis: JSON.stringify({
          xAxis: xValueItem,
          yAxis: yValueItem,
        }),
        conditions: JSON.stringify({
          id: idValueItem,
          data:
            datasetRead &&
            datasetRead.datas &&
            datasetRead.datas.slice(0, 8).map(item => ({
              x: item[xValueItem],
              y: item[yValueItem],
            })),
        }),
      });
    }
  }

  const onAddConditions = () => {
    const date = new Date().getTime();
    const dataNm = datasetRead.dataNm && datasetRead.dataNm;
    settingChartPostData(chartTpItem);

    setChartConditions(
      chartConditions.concat({
        id: nextId.current,
        dataNm: dataNm,
        chartNm: chartNm,
        chartTp: chartTpItem,
        idValue: idValueItem,
        xValue: xValueItem,
        yValue: yValueItem,
        date: date,
      }),
    );

    // 여기서 post request 를 보내면 될 듯.

    nextId.current += 1;
    setChartNm("");
    setChartTpItem("");
    setIdValueItem("");
    setXvalueItem("");
    setYValueItem("");
  };

  useEffect(() => {
    if (chartPostData !== "") {
      const date = new Date().getTime();

      datasetCreateChart(chartPostData);
      setChartDatasProps(
        chartDatasProps.concat({
          dataId: chartPostData.dataId,
          chartNm: chartPostData.chartNm,
          chartTp: chartPostData.chartTp,
          axis: JSON.parse(chartPostData.axis),
          conditions: JSON.parse(chartPostData.conditions),
          regDt: date,
        }),
      );
    }
  }, [chartPostData]);

  console.log("chartDatasProps: ", chartDatasProps);

  const sortedChart =
    chartDatasProps &&
    chartDatasProps.sort((a, b) => (a.regDt < b.regDt ? 1 : -1));

  const onDelete = id => {
    datasetDeleteChart(id);
    setChartDatasProps(chartDatasProps.filter(item => item.chartId !== id));
  };

  const [open, setOpen] = useState(false);
  const [selectedChart, setSelectedChart] = useState("");
  const handleOpen = id => {
    setOpen(true);
    setSelectedChart(chartDatasProps.filter(item => item.chartId === id));
  };
  const handleClose = id => {
    setOpen(false);
  };

  return (
    <>
      {datasetRead && (
        <>
          <style.Card>
            <style.CardContent className="CardContentHeader">
              <StyledCardTitle>데이터셋 상세정보</StyledCardTitle>
              <Link to="/admin/dataset">
                <Button color="primary" variant="contained">
                  이전
                </Button>
              </Link>
            </style.CardContent>

            <style.CardContent>
              <StyeldCardBody>
                <StyledCardBodyInfo>
                  데이터셋 이름
                  <span>{datasetRead.dataNm}</span>
                </StyledCardBodyInfo>
                <StyledCardBodyInfo>
                  설명
                  <span>{datasetRead.dataDesc}</span>
                </StyledCardBodyInfo>
              </StyeldCardBody>
              <StyeldCardBody>
                <StyledFlex
                  justifyprops={"space-between"}
                  style={{ padding: "0" }}
                >
                  <StyledCardBodyTitle>데이터셋 기본정보</StyledCardBodyTitle>
                  <DatasetTemporaryDrawer
                    buttonProps={"수정"}
                    buttoncolorprops={"#9c27b0"}
                  />
                </StyledFlex>
                <Table
                  tableHead={[
                    "종류",
                    "데이터베이스",
                    "용량",
                    "...",
                    "공개여부",
                    "생성자",
                    "소유자",
                  ]}
                  datasetInfos
                  for
                  table
                  data
                  tableData={[datasetInfos]}
                />
              </StyeldCardBody>
              <StyeldCardBody>
                <StyledCardBodyTitle>데이터셋 탐색</StyledCardBodyTitle>
                {datasetRead && datasetRead.metaInfos && (
                  <Table
                    tableHead={[
                      "컬럼명",
                      "컬럼 타입",
                      "최소값",
                      "최대값",
                      "평균값",
                      "유효값",
                      "누락비율",
                      "...",
                    ]}
                    tableData={metaInfos}
                  />
                )}
              </StyeldCardBody>

              {datasetRead && datasetRead.datas && datasetRead.datas[0] ? (
                <StyeldCardBody>
                  <StyledCardBodyTitle>차트</StyledCardBodyTitle>

                  <ChartSetWrapper>
                    <form
                      className={classes.formControl}
                      noValidate
                      autoComplete="off"
                    >
                      <TextField
                        id="standard-basic"
                        label="차트명"
                        value={chartNm}
                        onChange={onChangeChartNm}
                      />
                    </form>
                    <FormControl className={classes.formControl}>
                      <InputLabel id="demo-simple-select-label">
                        차트 종류
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={chartTpItem}
                        name="chartTp"
                        onChange={onChangeChartTp}
                      >
                        {chartTp.map((item, index) => (
                          <MenuItem key={index} value={item}>
                            {item}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                    {chartTpItem === "scatterplot" && (
                      <FormControl className={classes.formControl}>
                        <InputLabel id="demo-simple-select-label">
                          ID
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={idValueItem}
                          onChange={onChangeIdValue}
                          name="idValue"
                        >
                          {Object.keys(datasetRead.datas[0]).map(
                            (item, index) => (
                              <MenuItem key={index} value={item}>
                                {item}
                              </MenuItem>
                            ),
                          )}
                        </Select>
                      </FormControl>
                    )}
                    <FormControl className={classes.formControl}>
                      <InputLabel id="demo-simple-select-label">
                        X AXIS
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={xValueItem}
                        onChange={onChangeXValue}
                        name="xValue"
                      >
                        {Object.keys(datasetRead.datas[0]).map(
                          (item, index) => (
                            <MenuItem key={index} value={item}>
                              {item}
                            </MenuItem>
                          ),
                        )}
                      </Select>
                    </FormControl>
                    {chartTpItem !== "histogram" && (
                      <FormControl className={classes.formControl}>
                        <InputLabel id="demo-simple-select-label">
                          Y AXIS
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={yValueItem}
                          onChange={onChangeYValue}
                          name="yValue"
                        >
                          {Object.keys(datasetRead.datas[0]).map(item => (
                            <MenuItem key={item} value={item}>
                              {item}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    )}
                    <Button
                      onClick={onAddConditions}
                      color="primary"
                      variant="outlined"
                    >
                      생성
                    </Button>
                  </ChartSetWrapper>

                  {
                    <ChartWrapper>
                      <Grid container spacing={2}>
                        <Grid item xs={12}>
                          <Grid container spacing={2}>
                            {chartConditions &&
                              chartDatasProps &&
                              sortedChart.map(item => (
                                <Grid item key={item.chartId}>
                                  <ChartEachWrapper>
                                    <ChartInfo className="chartTitle">
                                      <div>{item.chartNm}</div>
                                      <div>
                                        <Button
                                          onClick={() =>
                                            handleOpen(item.chartId)
                                          }
                                          color="primary"
                                          variant="text"
                                          style={{
                                            minWidth: "unset",
                                            backgroundColor: "unset",
                                            margin: "0 0.3rem",
                                          }}
                                        >
                                          <SearchIcon color="primary" />
                                        </Button>
                                        <Button
                                          onClick={() => onDelete(item.chartId)}
                                          color="primary"
                                          variant="text"
                                          style={{
                                            minWidth: "unset",
                                            backgroundColor: "unset",
                                            margin: "0 0.3rem",
                                          }}
                                        >
                                          <CloseIcon />
                                        </Button>
                                      </div>
                                    </ChartInfo>
                                    <ChartInfo className="chartInfo">
                                      <span
                                        style={{
                                          fontSize: "0.5rem",
                                          color: "gray",
                                        }}
                                      >
                                        by
                                      </span>
                                      <span style={{ margin: "0 0.5rem" }}>
                                        regNm
                                      </span>
                                      <div style={{ fontSize: "0.5rem" }}>
                                        {`(${moment(item.date).format(
                                          "YYYY-MM-DD",
                                        )})`}
                                      </div>
                                    </ChartInfo>

                                    <ChartSize>
                                      <DataVisualization
                                        chartDatas={item}
                                        datas={datasetRead.datas}
                                        id={item.chartId}
                                        dataNm={item.chartNm}
                                        chartTp={item.chartTp}
                                        idValue={item.idValue}
                                        xValue={item.axis.xValue}
                                        yValue={item.axis.yValue}
                                        item={item}
                                        onDelete={onDelete}
                                        item={item}
                                        width={300}
                                        height={300}
                                      />
                                    </ChartSize>
                                  </ChartEachWrapper>
                                </Grid>
                              ))}

                            {selectedChart !== "" && (
                              <Modal
                                aria-labelledby="transition-modal-title"
                                aria-describedby="transition-modal-description"
                                className={classes.modal}
                                open={open}
                                onClose={() => handleClose(selectedChart[0].id)}
                                closeAfterTransition
                                BackdropComponent={Backdrop}
                                BackdropProps={{
                                  timeout: 500,
                                }}
                              >
                                <Fade in={open}>
                                  <ChartSize
                                    className={classes.paper}
                                    style={{
                                      width: "1000px",
                                      height: "530px",
                                      flexDirection: "column",
                                      padding: "1rem 0",
                                    }}
                                  >
                                    <div
                                      style={{
                                        fontSize: "2rem",
                                        marginTop: "1.1rem",
                                        borderBottom:
                                          "1px solid rgba(0,0,0,0.3)",
                                        padding: "1rem",
                                        width: "100%",
                                      }}
                                    >
                                      {selectedChart[0].chartNm}
                                    </div>
                                    <DataVisualization
                                      datas={datasetRead.datas}
                                      id={selectedChart[0].chartId}
                                      dataNm={selectedChart[0].dataNm}
                                      chartTp={selectedChart[0].chartTp}
                                      idValue={selectedChart[0].idValue}
                                      xValue={selectedChart[0].xValue}
                                      yValue={selectedChart[0].yValue}
                                      item={selectedChart[0]}
                                      onDelete={onDelete}
                                      chartDatas={selectedChart[0]}
                                      width={800}
                                      height={440}
                                    />
                                  </ChartSize>
                                </Fade>
                              </Modal>
                            )}
                          </Grid>
                        </Grid>
                      </Grid>
                    </ChartWrapper>
                  }
                </StyeldCardBody>
              ) : (
                "no datas for chart"
              )}
            </style.CardContent>
          </style.Card>
        </>
      )}
    </>
  );
}

export default withRouter(DatasetDetail);
