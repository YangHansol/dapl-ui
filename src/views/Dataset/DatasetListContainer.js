import React, { useEffect, useCallback, useState } from "react";
import { withRouter, Link } from "react-router-dom";

// libraries
import { Pagination } from "react-bootstrap";

// connect state
import { parseQueryString } from "library/queryrouter/parseQueryString";
import useDatasetSelector from "../../state/hooks/useDatasetSelector";
import useDatasetDispatch from "../../state/hooks/useDatasetDispatch";

import Utils from "library/utils/Utils";
import {
  DatasetType,
  QueryDbType,
  QueryStatus,
  ShareText,
  // DatasetTypeCode,
  // QueryStatusCode,
} from "library/constants/Code";

// custom style
import styled from "styled-components";
import DatasetDeleteAlert from "./DatasetDeleteAlert";
import DateViewer from "library/common/components/DateViewer.jsx";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Card,
  CardContent,
  Typography,
} from "@material-ui/core";

const styles = {};
const useStyles = makeStyles(styles);

const style = {
  Card: styled(Card)`
    box-shadow: unset;
    border: 1px solid ${props => props.theme.GrayColor};
  `,
  CardContent: styled(CardContent)`
    &.CardContentHeader {
      display: flex;
      justify-content: space-between;
      align-items: center;
      border-bottom: 1px solid ${props => props.theme.GrayColor};
    }
  `,

  TableCell: styled(TableCell)`
    padding: 0.8rem;
    &.TableHeadCell {
      background-color: ${props => props.theme.GrayLightColor};
    }
  `,
};

function DatasetListContainer({ location, history }) {
  const classes = useStyles();

  // select & dispatch
  const { update, datasetList, datasetRead } = useDatasetSelector();
  const {
    initAction,
    getDatasetList,
    goDetail,
    getListForPaging,
    deleteDataset,
    getDatasetRead,
  } = useDatasetDispatch({ history });

  console.log("dataset list dataset service: ", datasetList);

  const { totalElements, content, totalPages } = datasetList;
  const locationQuery = parseQueryString(location.search);
  const pageNo = parseInt(locationQuery.number, 10) + 1;
  let activePage = pageNo ? pageNo : 1;
  const tHead = [
    { title: "이름", sortBy: "dataNm", align: "text-left" },
    { title: "종류", sortBy: "dataTp", align: "text-left" },
    { title: "파일 이름", sortBy: "fileNm", align: "text-left" },
    { title: "용량", sortBy: "fileSz", align: "text-left" },
    { title: "사용기간", sortBy: "exptStDt", align: "text-left" },
    { title: "공개여부", sortBy: null, align: "text-left" },
    { title: "소유자 (생성자)", sortBy: null, align: "text-left" },
    { title: "설정", sortBy: null, align: "text-left" },
  ];

  // detail page
  let actPreQuery = {
    ...locationQuery,
  };
  const goDatasetDetail = (dataId, actPreQuery) => {
    goDetail({
      dataId,
      actPreSort: actPreQuery.sort,
      actPrePage: actPreQuery.number,
    });
  };

  // pagination
  const [pageState, setPageState] = useState({ page: 0 });
  const handlePageSelect = page => {
    setPageState({ page });

    let query = {
      ...locationQuery,
      number: page - 1,
    };

    getListForPaging(query);
  };

  // loadList
  const loadList = useCallback(() => {
    const query = {
      ...locationQuery,
    };
    if (query.searchType) {
      query[query.searchType] = query.searchText;
    }
    if (!query.sort) {
      query.sort = "regDt.desc";
    }
    const reqQuery = {
      query: { ...locationQuery },
      sort: "regDt.desc",
      number: 0,
      size: 10,
      delFl: "N",
    };
    initAction();
    // getCommonList(query);
    getDatasetList(reqQuery);
    getDatasetRead("15741376342862");
  }, [locationQuery.number]);

  const [toggleTableSize, setToggleTableSize] = useState(true);
  const [tableSize, setTableSize] = useState("medium");

  // useEffect with deps(lcationQuery.number for pagination, update for create and delete)
  useEffect(() => {
    loadList();
  }, [locationQuery.number, update]);
  console.log("dataset read: ", datasetRead);

  return (
    <>
      <style.Card>
        <style.CardContent className="CardContentHeader">
          <Typography component="h5" variant="h5">
            데이터셋
          </Typography>
          <Typography
            variant="subtitle1"
            color="textSecondary"
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <Link to="/admin/dataset/create">생성</Link>
          </Typography>
        </style.CardContent>

        <TableContainer
          component={Paper}
          style={{ padding: "1rem", boxShadow: "unset" }}
        >
          <Table
            className={classes.table}
            size={toggleTableSize === true ? `medium` : "small"}
            aria-label="simple table"
          >
            <TableHead>
              <TableRow>
                {tHead &&
                  tHead.map(data => (
                    <style.TableCell className="TableHeadCell" key={data.title}>
                      {data.title}
                    </style.TableCell>
                  ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {content &&
                content.map((data, index) => (
                  <TableRow key={index}>
                    <style.TableCell>
                      <Link
                        to={`/admin/dataset/detail/${data.dataId}`}
                        style={{ color: "inherit" }}
                      >
                        <Typography style={{ height: "10px", padding: "0px" }}>
                          {data.dataNm + `(${data.count ? data.count : `0`})`}
                        </Typography>
                      </Link>
                    </style.TableCell>
                    <style.TableCell>
                      <Typography>{DatasetType[data.dataTp]}</Typography>
                    </style.TableCell>
                    <style.TableCell>
                      <Typography>{data.fileNm}</Typography>
                    </style.TableCell>
                    <style.TableCell>
                      <Typography>{Utils.formatBytes(data.fileSz)}</Typography>
                    </style.TableCell>
                    <style.TableCell>
                      <Typography>
                        {data.exptStDt === null ? (
                          <span>-</span>
                        ) : (
                          <DateViewer date={data.exptStDt} half />
                        )}
                        <span> ~ </span>
                        {data.exptEdDt === null ? (
                          <span>-</span>
                        ) : (
                          <DateViewer date={data.exptEdDt} half />
                        )}
                      </Typography>
                    </style.TableCell>
                    <style.TableCell>
                      <Typography>{ShareText[data.pubFl]}</Typography>
                    </style.TableCell>
                    <style.TableCell>
                      <Typography>
                        {data.ownerNm} ({data.regNm})
                      </Typography>
                    </style.TableCell>
                    <style.TableCell>
                      <Typography>
                        <DatasetDeleteAlert
                          deleteAction={deleteDataset}
                          selectedId={data.dataId}
                          DatasetData={data}
                        />
                      </Typography>
                    </style.TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
        {datasetList && totalElements !== 0 && (
          <Pagination
            prev
            next
            items={totalPages}
            boundaryLinks={true}
            ellipsis={false}
            maxButtons={5}
            activePage={activePage}
            onSelect={handlePageSelect}
            style={{
              display: "flex",
              justifyContent: "center",
            }}
          />
        )}
      </style.Card>
    </>
  );
}

export default withRouter(DatasetListContainer);
