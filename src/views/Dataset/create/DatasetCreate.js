import React, { useState, useEffect, useRef } from "react";
import DatasetChoose from "./DatasetChoose";
import DatasetSettings from "./DatasetSettings";
import DatasetExtract from "./DatasetExtract";
import DatasetSummary from "./DatasetSummary";
import { Link } from "react-router-dom";
import moment from "moment";
import DatasetDataTable from "./DatasetDataTable";
import DatasetFileUpload from "./DatasetFileUpload";

import useDatasetSelector from "../../../state/hooks/useDatasetSelector";
import useDatasetDispatch from "../../../state/hooks/useDatasetDispatch";

import { makeStyles } from "@material-ui/core/styles";
import {
  TableCell,
  Card,
  CardContent,
  Stepper,
  Step,
  StepLabel,
  Button,
  Typography,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import styled from "styled-components";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginBottom: "5rem",
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

const style = {
  InputWrapper: styled.div`
    display: flex;
    justify-content: center;
    &.Top {
      margin-top: 2rem;
    }
    &.Left {
      justify-content: flex-end;
    }
    &.Bottom {
      margin-top: 5rem;
    }
  `,
  Card: styled(Card)`
    box-shadow: unset;
    border: 1px solid ${props => props.theme.GrayColor};
  `,
  CardContent: styled(CardContent)`
    &.CardContentHeader {
      display: flex;
      justify-content: space-between;
      align-items: center;
      border-bottom: 1px solid ${props => props.theme.GrayColor};
    }
  `,

  TableCell: styled(TableCell)`
    padding: 0.8rem;
    &.TableHeadCell {
      background-color: ${props => props.theme.GrayLightColor};
      position: sticky;
      top: 0;
    }
  `,
};

// stepper 항목
function getSteps() {
  return [
    "데이터셋 추출 타입",
    "데이터셋 기본정보",
    "데이터 추출",
    "데이터셋 요약정보",
  ];
}

// stepper 설명
function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return "데이터셋 추출 타입";
    case 1:
      return "데이터셋 기본정보";
    case 2:
      return "데이터 추출";
    case 3:
      return "데이터셋 요약정보";
    default:
      return "데이터셋 생성 완료";
  }
}

export default function DatasetCreate({ history }) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  // console.log(activeStep);
  const steps = getSteps();

  // dataset create
  const { databaseDatas } = useDatasetSelector();
  const { datasetCreate } = useDatasetDispatch({ history });

  // alert
  const [open, setOpen] = useState(false);
  // const [title, setTitle] = useState("");
  const [text, setText] = useState("");
  const handleClose = () => {
    setOpen(false);
  };

  // TODO: 통합 state 는 각 컴포넌트의 개발이 끝난 후 통합.
  const [createState, setCreateState] = useState({
    dataNm: "",
    dataDesc: "",
    dataTp: "",
    datas: [{}],
    exptStDt: moment().format("YYYY-MM-DD"),
    exptEdDt: moment()
      .add(1, "months")
      .format("YYYY-MM-DD"),
    pubFl: "",
    dbId: "",
    dbNm: "",
    dbIp: "",
    queryStr: "",
    queryDbTp: "",
    sqlLength: "",
    showDownload: "",
    extractTp: "",
    extractCondition: "",
    tableNm: "",
    conditions: "",
    query: "test",
    selectCondition: "",
    viewConditions: "",
    sqlRequest: "",
    dataView: "",
    isProgress: false,
    progress: 0,
    files: "",
    filePath: "",
    fileNm: "",
    fileSz: 0,
    dataCnt: 0,
  });
  console.log("createState: ", createState);

  const validateInput = () => {
    let {
      dataTp,
      dataNm,
      dataDesc,
      exptStDt,
      exptEdDt,
      pubFl,
      dbNm,
      extractTp,
      tableNm,
      query,
      fileNm,
      filePath,
    } = createState;

    // if (activeStep === 0) {
    //   // setTitle("데이터셋 추출 타입");
    //   if (dataTp === "") {
    //     setOpen(true);
    //     setText("데이터셋 추출 타입을 선택해 주세요.");
    //     return false;
    //   }
    //   return true;
    // }

    if (activeStep === 1) {
      // setTitle("데이터셋 기본정보");
      if (dataNm.trim().length === 0) {
        setOpen(true);
        setText("데이터셋 이름을 입력해 주세요.");
        return false;
      }
      if (dataDesc.trim().length === 0) {
        setOpen(true);
        setText("데이터셋 설명을 입력해 주세요.");
        return false;
      }
      if (!exptStDt) {
        setOpen(true);
        setText("시작 일자를 선택해 주세요.");
        return false;
      }
      if (!exptEdDt) {
        setOpen(true);
        setText("종료 일자를 선택해 주세요.");
        return false;
      }
      if (!pubFl) {
        setOpen(true);
        setText("공개 여부를 선택해 주세요.");
        return false;
      }
      return true;
    }

    if (activeStep === 2 && createState.dataTp === "0") {
      // setTitle("데이터 추출");
      if (dbNm.trim().length === 0) {
        setOpen(true);
        setText("데이터셋 이름을 입력해 주세요.");
        return false;
      }
      if (!extractTp) {
        setOpen(true);
        setText("추출 방법을 선택해 주세요.");
        return false;
      }
      if (extractTp === "0") {
        if (!tableNm) {
          setOpen(true);
          setText("테이블 명을 선택해 주세요.");
          return false;
        }
      }
      if (extractTp === "1") {
        if (!query) {
          setOpen(true);
          setText("SQL 명령을 입력해 주세요.");
          return false;
        }
      }
    }

    if (activeStep === 2 && createState.dataTp === "1") {
      // setTitle("데이터 추출");
      if (!fileNm) {
        setOpen(true);
        setText("파일을 선택해 주세요.");
        return false;
      }
      if (!filePath) {
        setOpen(true);
        setText("파일을 업로드해 주세요.");
        return false;
      }
    }
    return true;
  };

  const handleNext = () => {
    if (validateInput() === false) return;

    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };
  const handleConfirm = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
    datasetCreate(createState);
  };
  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };
  const handleReset = () => {
    setActiveStep(0);
  };

  const [datas, setDatas] = useState(databaseDatas);
  useEffect(() => {
    setDatas(databaseDatas);
  }, [databaseDatas]);
  useEffect(() => {
    setDatas("");
  }, [
    history.location.pathname,
    createState.dbId,
    createState.extractTp,
    createState.tableNm,
  ]);

  return (
    <div className={classes.root}>
      {/* stepper */}
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>

      {/* views routing */}
      <style.InputWrapper className="Top">
        <Typography
          className={classes.instructions}
          variant="h5"
          style={{ marginBottom: "2rem" }}
        >
          {getStepContent(activeStep)}
        </Typography>
      </style.InputWrapper>

      {activeStep === 0 && (
        <div>
          <DatasetChoose
            handleNext={handleNext}
            createState={createState}
            setCreateState={setCreateState}
          />
        </div>
      )}
      {activeStep === 1 && (
        <div>
          <DatasetSettings
            createState={createState}
            setCreateState={setCreateState}
          />
        </div>
      )}
      {activeStep === 2 && createState.dataTp === "1" && (
        <div>
          <DatasetFileUpload
            createState={createState}
            setCreateState={setCreateState}
          />
        </div>
      )}
      {activeStep === 2 && createState.dataTp === "0" && (
        <div>
          <DatasetExtract
            createState={createState}
            setCreateState={setCreateState}
          />
        </div>
      )}
      {activeStep === 3 && (
        <div>
          <DatasetSummary createState={createState} />
        </div>
      )}

      {activeStep === steps.length && (
        <style.InputWrapper>
          <Link to="/admin/dataset">
            <Typography variant="h6">데이터셋 페이지로 이동</Typography>
          </Link>
        </style.InputWrapper>
      )}
      {activeStep === 2 &&
        databaseDatas &&
        databaseDatas.data &&
        datas !== "" && <DatasetDataTable databaseDatas={datas.data} />}
      {/* button section */}
      <style.InputWrapper className="Left Bottom">
        {activeStep !== steps.length && activeStep !== 0 && (
          // (
          //   <div>
          //     <Typography className={classes.instructions}>
          //       All steps completed
          //     </Typography>
          //     <Button onClick={handleReset}>Reset</Button>
          //   </div>
          // ) :
          <div>
            {/* <Typography className={classes.instructions}>
              {getStepContent(activeStep)}
            </Typography> */}
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.backButton}
              >
                이전
              </Button>
              {activeStep === steps.length - 1 ? (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleConfirm}
                >
                  생성
                </Button>
              ) : (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleNext}
                >
                  다음
                </Button>
              )}
            </div>
          </div>
        )}
      </style.InputWrapper>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        fullWidth={true}
        maxWidth={"xs"}
      >
        {/* <DialogTitle id="alert-dialog-title">{title}</DialogTitle> */}
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {text}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            확인
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
