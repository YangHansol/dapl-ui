import React from "react";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns";
import format from "date-fns/format";
import koLocale from "date-fns/locale/ko";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";

import { makeStyles } from "@material-ui/core/styles";
import {
  TextField,
  Grid,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
} from "@material-ui/core";
import styled from "styled-components";

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: 230,
    },
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 230,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
const style = {
  Wrapper: styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  `,
  ItemWrapper: styled.div`
    width: 550px;
  `,
  InputWrapper: styled.div`
    display: flex;
    justify-content: space-between;
  `,
  InputTitle: styled.div``,
  Grid: styled(Grid)`
    display: flex;
    justify-content: space-between;
  `,
};

class LocalizedUtils extends DateFnsUtils {
  getDatePickerHeaderText(date) {
    return format(date, "MM", { locale: this.locale });
  }
}

function DatasetSettings({ createState, setCreateState }) {
  const classes = useStyles();

  const { exptStDt } = createState;

  const handleChange = name => event => {
    setCreateState({
      ...createState,
      [name]: event.target.value,
    });
  };

  const handleDateChange = name => date => {
    setCreateState({
      ...createState,
      [name]: moment(date).format("YYYY-MM-DD"),
    });
  };
  const onSubmit = e => {
    e.preventDefault();
  };

  // const validateInput = () => {
  //   let { dataNm, dataDesc, exptStDt, exptEdDt, pubFl } = createState;
  //   if (dataNm.trim().length === 0) {
  //     alert("데이터셋 이름을 입력해 주세요.");
  //     return false;
  //   }
  //   if (dataDesc.trim().length === 0) {
  //     alert("데이터셋 설명을 입력해 주세요.");
  //     return false;
  //   }
  //   if (!exptStDt) {
  //     alert("시작 일자를 선택해 주세요.");
  //     return false;
  //   }
  //   if (!exptEdDt) {
  //     alert("종료 일자를 선택해 주세요.");
  //     return false;
  //   }
  //   if (!pubFl) {
  //     alert("공개 여부를 선택해 주세요.");
  //     return false;
  //   }
  //   return true;
  // };

  // const onConfirm = () => {
  //   if (validateInput() === false) return;

  //   let { dataNm, dataDesc, exptStDt, exptEdDt, pubFl } = createState;

  //   let postData = {
  //     dataNm,
  //     dataDesc,
  //     exptStDt: moment(exptStDt).format("YYYYMMDD"),
  //     exptEdDt: moment(exptEdDt).format("YYYYMMDD"),
  //     pubFl,
  //   };
  // };

  return (
    <>
      <style.Wrapper>
        <style.ItemWrapper>
          <style.InputWrapper>
            <form
              name="dataNm"
              className={classes.root}
              noValidate
              autoComplete="off"
            >
              <TextField
                id="standard-basic-name"
                label="데이터셋 이름"
                value={createState.dataNm}
                onChange={handleChange("dataNm")}
                inputProps={{
                  name: "dataNm",
                }}
              />
            </form>
            <form
              name="dataDesc"
              className={classes.root}
              noValidate
              autoComplete="off"
            >
              <TextField
                id="standard-basic-description"
                label="설명"
                value={createState.dataDesc}
                onChange={handleChange("dataDesc")}
                inputProps={{
                  name: "dataDesc",
                }}
              />
            </form>
          </style.InputWrapper>
          <style.InputWrapper>
            <MuiPickersUtilsProvider utils={LocalizedUtils} locale={koLocale}>
              <style.Grid container>
                <KeyboardDatePicker
                  name="exptStDt"
                  className={classes.root}
                  disableToolbar
                  variant="inline"
                  format="yyyy/MM/dd"
                  margin="normal"
                  id="date-picker-inline-start"
                  label="시작 일자"
                  value={createState.exptStDt}
                  onChange={handleDateChange("exptStDt")}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                    name: "exptStDt",
                  }}
                  inputProps={{
                    name: "exptStDt",
                  }}
                  minDate={moment().format("YYYY-MM-DD")}
                />
                <KeyboardDatePicker
                  name="exptEdDt"
                  className={classes.root}
                  disableToolbar
                  variant="inline"
                  format="yyyy/MM/dd"
                  margin="normal"
                  id="date-picker-inline-end"
                  label="종료 일자"
                  value={createState.exptEdDt}
                  onChange={handleDateChange("exptEdDt")}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  inputProps={{
                    name: "exptEdDt",
                  }}
                  minDate={moment(exptStDt).format("YYYY-MM-DD")}
                  maxDate={moment(exptStDt)
                    .add(2, "months")
                    .format("YYYY-MM-DD")}
                />
              </style.Grid>
            </MuiPickersUtilsProvider>
          </style.InputWrapper>
          <style.InputWrapper>
            <FormControl name="pubFl" className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">공개 여부</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={createState.pubFl}
                onChange={handleChange("pubFl")}
                inputProps={{
                  name: "pubFl",
                }}
              >
                <MenuItem value={"Y"}>공개</MenuItem>
                <MenuItem value={"N"}>비공개</MenuItem>
                <MenuItem value={"P"}>미공개 (개인정보포함)</MenuItem>
              </Select>
            </FormControl>
          </style.InputWrapper>
        </style.ItemWrapper>
      </style.Wrapper>
    </>
  );
}

export default DatasetSettings;
