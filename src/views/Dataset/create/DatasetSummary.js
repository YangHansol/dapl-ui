import React from "react";

import styled from "styled-components";
import { TableCell, Card, CardContent } from "@material-ui/core";

const style = {
  Card: styled(Card)`
    box-shadow: unset;
    border: 1px solid ${props => props.theme.GrayColor};
    width: 500px;
  `,
  CardContent: styled(CardContent)`
    display: flex;
    justify-content: space-between;
    align-items: center;
    &.CardContentHeader {
      border-bottom: 1px solid ${props => props.theme.GrayColor};
    }
  `,

  TableCell: styled(TableCell)`
    padding: 0.8rem;
    &.TableHeadCell {
      background-color: ${props => props.theme.GrayLightColor};
    }
  `,
  Wrapper: styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  `,
  ItemWrapper: styled.div`
    width: 550px;
  `,
  InputWrapper: styled.div`
    display: flex;
  `,
  InputTitle: styled.div``,
};

function DatasetSummary({ createState }) {
  const PUBFL = {
    Y: "공개",
    N: "비공개",
    P: "미공개(개인정보포함)",
  };

  return (
    <style.Wrapper>
      <style.Card>
        <style.CardContent>
          <div>데이터셋 이름</div>
          <div>{createState.dataNm}</div>
        </style.CardContent>
        <style.CardContent>
          <div>설명</div>
          <div>{createState.dataDesc}</div>
        </style.CardContent>
        <style.CardContent>
          <div>사용 기간</div>
          <div>{`${createState.exptStDt} ~ ${createState.exptStDt}`}</div>
        </style.CardContent>
        <style.CardContent>
          <div>공개여부</div>
          <div>{PUBFL[createState.pubFl]}</div>
        </style.CardContent>
        <style.CardContent>
          <div>데이터베이스 정보</div>
          {createState.dbNm && createState.dbIp && (
            <div>
              {createState.dbNm} ({createState.dbIp})
            </div>
          )}
        </style.CardContent>
      </style.Card>
    </style.Wrapper>
  );
}

export default DatasetSummary;
