import React, { useMemo, useState } from "react";

function list(files) {
  const label = file =>
    `'${file.name}' of size '${file.size}' and type '${file.type}'`;
  return files.map(file => <li key={file.name}>{label(file)}</li>);
}

const FileList = ({ files }) => {
  console.log("files: ", files);
  if (files.length === 0) {
    return <div>선택된 파일 없음</div>;
  }
  const fileList = () => {
    return list(files);
  };
  return <div>{files && files.map(file => file.name)}</div>;
};

export default FileList;
