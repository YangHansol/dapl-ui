import React, { useCallback, useState } from "react";
import styled from "styled-components";
import { DatabaseIcon, FileIcon } from "utils/styles/Icons";

const style = {
  Title: styled.div``,
  ContentWrapper: styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
  `,
  ItemWrapper: styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-size: 1.5rem;
    font-weight: 300;
  `,
  Item: styled.div`
    cursor: pointer;
    width: 12rem;
    height: 12rem;
    margin: 2rem;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    border: ${props => props.theme.Border};
    &.select {
      background-color: ${props => props.theme.GrayLightColor};
      border-bottom: 2px solid ${props => props.theme.PrimaryColor};
    }
  `,
};

function DatasetChoose({ handleNext, createState, setCreateState }) {
  const [select, setSelect] = useState("");
  const onDataTpChange = useCallback(
    name => e => {
      handleNext();

      setCreateState({
        ...createState,
        dataTp: name,
      });
      setSelect(name);

      if (name === createState.dataTp) {
        setCreateState({
          ...createState,
          dataTp: "",
        });
        setSelect("");
      }
    },
    [createState],
  );

  return (
    <div>
      <style.ContentWrapper>
        <style.ItemWrapper>
          <style.Item
            onClick={onDataTpChange("1")}
            className={select === "1" && "select"}
          >
            <FileIcon size={80} />
          </style.Item>
          <div onClick={onDataTpChange("1")} style={{ cursor: "pointer" }}>
            FILE
          </div>
        </style.ItemWrapper>
        <style.ItemWrapper>
          <style.Item
            onClick={onDataTpChange("0")}
            className={select === "0" && "select"}
          >
            <DatabaseIcon size={80} />
          </style.Item>
          <div onClick={onDataTpChange("0")} style={{ cursor: "pointer" }}>
            DATABASE
          </div>
        </style.ItemWrapper>
      </style.ContentWrapper>
    </div>
  );
}

export default DatasetChoose;
