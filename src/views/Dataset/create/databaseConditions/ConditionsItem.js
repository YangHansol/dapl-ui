import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

import styled from "styled-components";

// connect

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: 113,
    },
  },
  formControl: {
    margin: theme.spacing(1),
    Width: 113,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const style = {
  Wrapper: styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  `,
  ItemWrapper: styled.div`
    width: 500px;
  `,
  InputWrapper: styled.div`
    display: flex;
    align-items: center;
    width: 550px;
    &.Left {
      justify-content: flex-end;
    }
  `,
  InputTitle: styled.div``,
  AddButton: styled.button`
    margin: 0;
    padding: 0;
    border: none;
    background: none;
    color: ${props => props.theme.PrimaryColor};
    margin-left: 0.5rem;
    font-weight: bolder;
  `,
  FormControl: styled(FormControl)`
    min-width: 113px;
  `,
};

const ConditionsItem = ({
  item,
  createState,
  conditionsArray,
  databaseTableColumn,
  operatorState,
  comparisonState,
  onRemove,
  onEdit,
}) => {
  const classes = useStyles();
  const onSubmit = e => {
    e.preventDefault();
  };

  return (
    <>
      {createState.tableNm !== "" && item && (
        <style.InputWrapper>
          {item.id !== conditionsArray[0].id ? (
            <style.FormControl name="operator" className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">논리연산자</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                name="operator"
                value={item.operator}
                onChange={onEdit(item.id)}
                inputProps={{
                  name: "operator",
                }}
              >
                {operatorState.map((item, index) => (
                  <MenuItem key={index} value={item.id}>
                    {item && item.value}
                  </MenuItem>
                ))}
              </Select>
            </style.FormControl>
          ) : (
            <style.FormControl style={{ margin: "8px" }}></style.FormControl>
          )}
          <style.FormControl name="column" className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">컬럼</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="column"
              value={item.column}
              onChange={onEdit(item.id)}
              inputProps={{
                name: "column",
              }}
            >
              {databaseTableColumn.map((item, index) => (
                <MenuItem key={index} value={item.columnNm}>
                  {item.columnNm}
                </MenuItem>
              ))}
            </Select>
          </style.FormControl>
          <style.FormControl name="comparison" className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">비교연산자</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="comparison"
              value={item.comparison}
              onChange={onEdit(item.id)}
              inputProps={{
                name: "comparison",
              }}
            >
              {comparisonState.map((item, index) => (
                <MenuItem key={index} value={item.id}>
                  {item && item.value}
                </MenuItem>
              ))}
            </Select>
          </style.FormControl>
          <style.FormControl name="value" className={classes.formControl}>
            <form
              className={classes.root}
              noValidate
              autoComplete="off"
              onSubmit={onSubmit}
            >
              <TextField
                id="standard-basic"
                label="값"
                name="value"
                onChange={onEdit(item.id)}
                inputProps={{
                  name: "value",
                }}
              />
            </form>
          </style.FormControl>
          <style.AddButton onClick={() => onRemove(item.id)}>X</style.AddButton>
        </style.InputWrapper>
      )
      // ))
      }
    </>
  );
};

export default React.memo(ConditionsItem);
