import React from "react";
import ConditionsItem from "./ConditionsItem";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  /* justify-content: center; */
  align-items: center;
  width: max-content;
`;

function ConditionsLists({
  conditionsArray,
  createState,
  databaseTableColumn,
  operatorState,
  comparisonState,
  onRemove,
  onEdit,
}) {
  return (
    <div>
      {conditionsArray.map((item, index) => (
        <Wrapper key={item.id}>
          {/* <div>조건 {index + 1}: </div> */}
          <ConditionsItem
            item={item}
            key={item.id}
            createState={createState}
            conditionsArray={conditionsArray}
            databaseTableColumn={databaseTableColumn}
            operatorState={operatorState}
            comparisonState={comparisonState}
            onRemove={onRemove}
            onEdit={onEdit}
          />
        </Wrapper>
      ))}
    </div>
  );
}

export default React.memo(ConditionsLists);
