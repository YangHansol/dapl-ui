import React, { useRef, useState, useCallback } from "react";
import { RegExp } from "library/constants/RegExp";

import FileuploadForm from "./FileuploadForm";
import DownloadLoading from "library/common/loading/DownloadLoading";
import Axios from "axios";
import { DEVELOP_API_SERVER } from "state/constants/ApiConstants";
import DatasetDataTable from "./DatasetDataTable";
import TargetBox from "./TargetBox";
import FileList from "./FileList";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

function DatasetFileUpload({ createState, setCreateState }) {
  const inputRef = useRef();

  const [open, setOpen] = useState(false);
  const [text, setText] = useState("");
  const [check, setCheck] = useState(false);

  const [results, setResults] = useState([]);

  const handleClose = () => {
    setOpen(false);
  };

  const onChange = e => {
    let files = inputRef.current.files || [];
    let ins = files.length;
    let fileNm = files[0].name;
    let FileSz = 0;

    for (var i = 0; i < ins; i++) {
      FileSz += files[i].size;
    }

    setCreateState({
      ...createState,
      files: files,
      fileNm: fileNm,
      fileSz: FileSz,
    });
  };

  // react-dnd file upload
  const [droppedFiles, setDroppedFiles] = useState([]);
  const handleFileDrop = useCallback((item, monitor) => {
    if (monitor) {
      const files = monitor.getItem().files;
      setDroppedFiles(files);
      let ins = files.length;
      let fileNm = files[0].name;
      let FileSz = 0;

      for (var i = 0; i < ins; i++) {
        FileSz += files[i].size;
      }
      setCreateState({
        ...createState,
        files: files,
        fileNm: fileNm,
        fileSz: FileSz,
      });
    }
  }, []);

  const onConfirm = () => {
    setOpen(true);
    let files = createState.files || [];
    let ins = files.length;

    // if (this.validateInput() === false) return;

    if (ins === 0) {
      setText("업로드할 파일을 선택해 주세요.");
    } else {
      for (var i = 0; i < ins; i++) {
        if (RegExp.KOREAN.test(files[i].name)) {
          setText("파일 이름은 영문으로 입력해 주세요.");
          return;
        }
      }

      if (createState.FileSz >= 10737418240) {
        // 107374182000 -> 10737418200 로 줄였는데 맞는 비율인지 확인 필요.
        // 10GB 로 제한 변경.
        setText("파일 크기는 100GB를 넘을 수 없습니다.");
        return;
      }

      setCheck(true);
      setText("등록하시겠습니까?");
    }
  };

  const onUpload = () => {
    let files = createState.files || [];
    let ins = files.length;
    let formData = new FormData();

    setOpen(false);

    for (var i = 0; i < ins; i++) {
      formData.append("files", files[i]);
    }

    let url = `${DEVELOP_API_SERVER.datasetServer}/dataset/v1/multiupload`;
    const config = {
      headers: { "Content-type": "multipart/form-data" },
      onUploadProgress: progressEvent => {
        let percentCompleted = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total,
        );
        setCreateState({
          ...createState,
          isProgress: true,
          progress: percentCompleted,
        });
      },
    };
    const data = formData !== null ? formData : {};
    const downloadCaller = () => {
      return Axios.post(url, data, config);
    };

    downloadCaller()
      .then(results => {
        setResults(results);
        setCreateState({
          ...createState,
          filePath: results.data.map(item => item.filePath)[0],
        });
      })
      .catch(err => {
        console.log("err: ", err);
      });
  };

  // console.log(
  //   "files results: ",
  //   inputRef.current && inputRef.current.files && inputRef.current.files,
  // );
  // console.log("droppedFiles: ", droppedFiles);

  return (
    <Wrapper>
      {createState.isProgress && createState.progress !== 100 && (
        <DownloadLoading
          progressText={"업로드 중 입니다."}
          progress={createState.progress}
        />
      )}

      {/* react-dnd for file upload */}
      {/* <TargetBox onDrop={handleFileDrop} />
      <FileList files={droppedFiles} />
      <button onClick={onConfirm}>upload</button> */}

      <FileuploadForm
        type="file"
        label="파일 업로드"
        inputRef={ref => (inputRef.current = ref)}
        totalFileSz={createState.fileSz}
        maxFileSz="10GB"
        help="*파일선택시 화면이 일시적으로 멈추는 현상이 일어날 수도 있습니다."
        onChange={onChange}
        onConfirm={onConfirm}
        // multiple
      />

      {results.length !== 0 &&
        results.data.map((item, index) => (
          <DatasetDataTable databaseDatas={item.datas} key={index} />
        ))}

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        fullWidth={true}
        maxWidth={"xs"}
      >
        <DialogTitle id="alert-dialog-title">파일 업로드</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {text}
          </DialogContentText>
        </DialogContent>
        {check === true && (
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              취소
            </Button>
            <Button onClick={onUpload} color="primary" autoFocus>
              확인
            </Button>
          </DialogActions>
        )}
      </Dialog>
    </Wrapper>
  );
}

export default DatasetFileUpload;
