import React, { useState, useEffect, useCallback, useRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  Typography,
  TableCell,
  Card,
  CardContent,
  TextField,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
} from "@material-ui/core";
import styled from "styled-components";

// connect
import useDatasetSelector from "../../../state/hooks/useDatasetSelector";
import useDatasetDispatch from "../../../state/hooks/useDatasetDispatch";
import { parseQueryString } from "library/queryrouter/parseQueryString";
import { withRouter } from "react-router-dom";

// database conditions
import ConditionsLists from "./databaseConditions/ConditionsLists";

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: 230,
    },
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 230,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const style = {
  Wrapper: styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  `,
  ItemWrapper: styled.div`
    width: 550px;
  `,
  InputWrapper: styled.div`
    display: flex;
    justify-content: space-between;
    &.Left {
      justify-content: flex-end;
    }
    &.FlexEnd {
      align-items: flex-end;
      justify-content: space-evenly;
    }
  `,
  InputTitle: styled.div``,
  AddButton: styled.button`
    margin: 0;
    padding: 0;
    border: none;
    background: none;
    color: ${props => props.theme.PrimaryColor};
    margin-left: 0.5rem;
    font-weight: bolder;
  `,
  FormControl: styled(FormControl)`
    min-width: 150px;
  `,
  Card: styled(Card)`
    box-shadow: unset;
    border: 1px solid ${props => props.theme.GrayColor};
  `,
  CardContent: styled(CardContent)`
    &.CardContentHeader {
      display: flex;
      justify-content: space-between;
      align-items: center;
      border-bottom: 1px solid ${props => props.theme.GrayColor};
    }
  `,

  TableCell: styled(TableCell)`
    padding: 0.8rem;
    &.TableHeadCell {
      background-color: ${props => props.theme.GrayLightColor};
    }
  `,
};

function DatasetExtract({ createState, setCreateState, history, location }) {
  const classes = useStyles();
  const locationQuery = parseQueryString(location.search);
  const {
    databaseList,
    // databaseListTest,
    databaseTable,
    databaseTableColumn,
    update,
  } = useDatasetSelector();
  const {
    getDatabaseList,
    // getDatabaseListTest,
    getDatabaseTable,
    getDatabaseTableColumn,
    getDatabaseDatas,
    initAction,
  } = useDatasetDispatch({
    history,
  });

  const loadList = useCallback(() => {
    const query = {
      ...locationQuery,
    };
    if (query.searchType) {
      query[query.searchType] = query.searchText;
    }
    if (!query.sort) {
      query.sort = "regDt.desc";
    }

    const reqQuery = {
      query: { ...locationQuery },
      sort: "regDt.desc",
      number: 0,
      size: 10,
    };

    initAction();
    getDatabaseList(reqQuery);
  }, [locationQuery.number]);

  useEffect(() => {
    loadList();
    if (createState.dbId) {
      getDatabaseTable(createState.dbId);
      getDatabaseTableColumn(createState.dbId, createState.tableNm);
    }
    // if (createState.dbId) {
    //   getDatabaseListTest(createState.dbId);
    // }
    // if (databaseListTest) {
    //   getDatabaseTable(databaseListTest.dbId);
    //   getDatabaseTableColumn(databaseListTest.dbId, createState.tableNm);
    // }
  }, [update, createState.dbId, createState.dbId, createState.tableNm]);

  const handleChange = name => event => {
    let dbNm = databaseList.content[0] && databaseList.content[0].dbNm;
    let dbIp = databaseList.content[0] && databaseList.content[0].dbIp;

    setCreateState({
      ...createState,
      [name]: event.target.value,
      dbNm: dbNm,
      dbIp: dbIp,
    });
  };

  // const onSubmit = e => {
  //   e.preventDefault();
  // };

  const [operatorState] = useState([
    { id: 0, value: "AND" },
    { id: 1, value: "OR" },
    { id: 2, value: "NOT" },
  ]);
  const [comparisonState] = useState([
    { id: 0, value: "같다(=)" },
    { id: 1, value: "크다(>)" },
    { id: 2, value: "작다(<)" },
    { id: 4, value: "크거나 같다(>=)" },
    { id: 5, value: "작거나 같다(<=)" },
    { id: 6, value: "포함된다(in)" },
    { id: 7, value: "포함되지 않는다(not in)" },
  ]);

  const nextId = useRef(0);
  const [conditionsArray, setConditionsArray] = useState([]);
  const [conditionsObject, setConditionsObject] = useState({
    id: nextId.current,
    operator: 0,
    column: "",
    comparison: "",
    value: "",
  });

  // tableNm 을 다시 설정할 경우, 기존의 conditions 값 reset.
  useEffect(() => {
    setConditionsArray([]);
    setConditionsObject({
      id: 0,
      operator: 0,
      column: "",
      comparison: "",
      value: "",
    });
  }, [createState.tableNm]);

  const onAddConditions = () => {
    setConditionsArray(conditionsArray.concat(conditionsObject));
    nextId.current += 1;
    setConditionsObject({
      id: nextId.current,
      operator: 0,
      column: "",
      comparison: "",
      value: "",
    });
  };

  const onRemove = useCallback(
    id => {
      setConditionsArray(conditionsArray.filter(item => item.id !== id));
    },
    [conditionsArray],
  );

  const onEdit = useCallback(
    id => e => {
      const { name, value } = e.target;
      setConditionsArray(
        conditionsArray.map(item =>
          item.id === id ? { ...item, [name]: value } : item,
        ),
      );
    },
    [conditionsArray],
  );

  const onDataShow = useCallback(() => {
    const { dbId, extractTp, tableNm, query } = createState;
    // console.log(dbId, extractTp, tableNm, conditions, sqlRequest, query);

    function replacer(key, value) {
      if (key === "id") {
        return undefined;
      }
      return value;
    }
    const validConditions = conditionsArray.filter(item => item.column !== "");
    const conditionsProps = JSON.stringify(validConditions, replacer);

    setCreateState({
      ...createState,
      conditions: encodeURI(conditionsProps),
    });

    getDatabaseDatas(
      dbId,
      extractTp,
      tableNm,
      encodeURI(conditionsProps),
      query,
    );
  }, [conditionsArray]);

  const onSQLDataShow = useCallback(() => {
    const { dbId, extractTp, tableNm, conditions, query } = createState;

    if (query[query.length - 1] === ";") {
      const setQuery = query.replace(query[query.length - 1], "");
      setCreateState({
        ...createState,
        query: setQuery,
      });
      getDatabaseDatas(dbId, extractTp, tableNm, conditions, setQuery);
      return;
    }

    getDatabaseDatas(dbId, extractTp, tableNm, conditions, query);
  }, [createState.query]);

  return (
    <>
      <style.Wrapper>
        <style.ItemWrapper>
          <style.InputWrapper>
            <FormControl name="dbId" className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">
                데이터 베이스
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="queryDbTp"
                name="queryDbTp"
                label="데이터 베이스 타입"
                value={createState.dbId}
                onChange={handleChange("dbId")}
                inputProps={{
                  name: "dbId",
                }}
              >
                {databaseList &&
                  databaseList.content &&
                  databaseList.content.map(item => (
                    <MenuItem key={item.dbId} value={item.dbId}>
                      {item.dbTp}
                    </MenuItem>
                  ))}
              </Select>
            </FormControl>

            <FormControl name="extractTp" className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">추출 방법</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={createState.extractTp}
                onChange={handleChange("extractTp")}
                inputProps={{
                  name: "extractTp",
                }}
              >
                <MenuItem value={"0"}>간편 추출</MenuItem>
                <MenuItem value={"1"}>SQL 추출</MenuItem>
              </Select>
            </FormControl>
          </style.InputWrapper>
          {createState.extractTp === "0" && databaseTable.tableNm && (
            <>
              <style.InputWrapper>
                <FormControl name="tableNm" className={classes.formControl}>
                  <InputLabel id="demo-simple-select-label">
                    테이블명
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    name="tableNm"
                    value={createState.tableNm}
                    onChange={handleChange("tableNm")}
                    inputProps={{
                      name: "tableNm",
                    }}
                  >
                    {databaseList &&
                      databaseTable &&
                      databaseTable.tableNm.map((item, index) => (
                        <MenuItem key={index} value={item}>
                          {item}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              </style.InputWrapper>
              {createState.tableNm !== "" && (
                <>
                  <style.InputWrapper style={{ margin: "2rem 0" }}>
                    <Typography>조회 조건</Typography>
                    <style.InputWrapper className="Right">
                      <style.AddButton onClick={onAddConditions}>
                        +
                      </style.AddButton>
                    </style.InputWrapper>
                  </style.InputWrapper>
                  <ConditionsLists
                    conditionsArray={conditionsArray}
                    createState={createState}
                    databaseTableColumn={databaseTableColumn}
                    operatorState={operatorState}
                    comparisonState={comparisonState}
                    onRemove={onRemove}
                    onEdit={onEdit}
                  />
                </>
              )}
              <style.InputWrapper className="Left">
                <Button variant="outlined" color="primary" onClick={onDataShow}>
                  조회
                </Button>
              </style.InputWrapper>
            </>
          )}

          {createState.extractTp === "1" && (
            <>
              <style.InputWrapper className="FlexEnd">
                <>
                  <form
                    name="sqlRequest"
                    className={classes.root}
                    noValidate
                    autoComplete="off"
                  >
                    <TextField
                      id="outlined-multiline-static"
                      label="SQL명령"
                      multiline
                      rows="4"
                      placeholder="SELECT * FROM TABLE;"
                      variant="outlined"
                      onChange={handleChange("query")}
                      inputProps={{
                        name: "sqlRequest",
                      }}
                      style={{ minWidth: "460px", margin: "0" }}
                    />
                  </form>
                </>
                <style.InputWrapper>
                  <Button
                    variant="outlined"
                    color="primary"
                    onClick={onSQLDataShow}
                    style={{ height: "2.3rem", marginTop: "5rem" }}
                  >
                    조회
                  </Button>
                </style.InputWrapper>
              </style.InputWrapper>
            </>
          )}
        </style.ItemWrapper>
      </style.Wrapper>
    </>
  );
}

export default withRouter(DatasetExtract);
