import React from "react";
import {
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock,
  Grid,
  Row,
} from "react-bootstrap";
import Utils from "../../../library/utils/Utils";
import { Button } from "@material-ui/core";

import styled from "styled-components";

const FormWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const FileuploadForm = ({
  id,
  label,
  help,
  totalFileSz,
  maxFileSz = null,
  onConfirm,
  ...props
}) => {
  console.log("files props: ", props);
  // FormControl 의 props 를 확인 하기 위해서는 FormControl 을 콘솔에 찍고, Sources 를 확인하면 됨.
  console.log("FormControl: ", FormControl);
  return (
    <FormGroup controlId={id}>
      <Grid fluid>
        <Row>
          <div>
            <ControlLabel>{label}</ControlLabel>
          </div>
          <div>
            <FormWrapper>
              <FormControl {...props} place="fileupload" accept=".csv" />
              {!!maxFileSz && (
                <span>
                  ({Utils.formatBytes(totalFileSz)} / {maxFileSz})
                </span>
              )}
              <Button
                color="primary"
                variant="outlined"
                onClick={onConfirm}
                style={{ margin: "0 1rem" }}
              >
                업로드
              </Button>
            </FormWrapper>
            {help && <HelpBlock>{help}</HelpBlock>}
          </div>
        </Row>
      </Grid>
    </FormGroup>
  );
};

export default FileuploadForm;
