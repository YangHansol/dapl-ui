import React from "react";
import { NativeTypes } from "react-dnd-html5-backend";
import { useDrop } from "react-dnd";
import styled from "styled-components";

const Wrapper = styled.div`
  border: ${props => props.theme.Border};
  height: 3rem;
  width: 15rem;
  padding: 2rem;
  text-align: center;
`;
const style = {
  border: "1px solid gray",
  height: "3rem",
  width: "15rem",
  padding: "2rem",
  textAlign: "center",
};
const TargetBox = props => {
  const { onDrop } = props;
  const [{ canDrop, isOver }, drop] = useDrop({
    accept: [NativeTypes.FILE],
    drop(item, monitor) {
      if (onDrop) {
        onDrop(props, monitor);
      }
    },
    collect: monitor => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });
  const isActive = canDrop && isOver;
  return (
    <Wrapper ref={drop}>
      {isActive ? "파일을 놓으세요." : "파일을 드래그 하세요."}
    </Wrapper>
  );
};
export default TargetBox;
