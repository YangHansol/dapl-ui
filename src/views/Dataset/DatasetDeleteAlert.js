import React from "react";
import Utils from "library/utils/Utils";
import { DatasetType } from "library/constants/Code";

import { makeStyles } from "@material-ui/core/styles";
import {
  TableCell,
  Card,
  CardContent,
  Typography,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@material-ui/core";
import DateViewer from "library/common/components/DateViewer";
import styled from "styled-components";

const style = {
  Card: styled(Card)`
    box-shadow: unset;
    border: 1px solid ${props => props.theme.GrayColor};
    width: 350px;
  `,
  CardContent: styled(CardContent)`
    display: flex;
    justify-content: space-between;
    align-items: center;
    &.CardContentHeader {
      border-bottom: 1px solid ${props => props.theme.GrayColor};
    }
  `,

  TableCell: styled(TableCell)`
    padding: 0.8rem;
    &.TableHeadCell {
      background-color: ${props => props.theme.GrayLightColor};
    }
  `,
  Wrapper: styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  `,
  ItemWrapper: styled.div`
    width: 500px;
  `,
  InputWrapper: styled.div`
    display: flex;
  `,
  InputTitle: styled.div``,
  TypographyThin: styled(Typography)`
    font-weight: 300;
    color: #4a4a4a;
  `,
};

export default function DatasetDeleteAlert({
  deleteAction,
  selectedId,
  DatasetData,
}) {
  const [open, setOpen] = React.useState(false);
  // const classes = useStyles();

  // common
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleOk = () => {
    deleteAction(selectedId);
    setOpen(false);
  };

  const handleClose = () => {
    setOpen(false);
  };

  // for dataset
  let selectedData = {};
  if (DatasetData.dataId === selectedId) {
    selectedData = DatasetData;
  }

  return (
    <>
      <Button
        variant="outlined"
        color="primary"
        onClick={handleClickOpen}
        style={{ padding: "3px 8px", minWidth: "0" }}
      >
        삭제
      </Button>

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"데이터셋 삭제"}</DialogTitle>

        <DialogContent>
          <style.Card>
            <style.CardContent>
              <style.TypographyThin>데이터셋 이름</style.TypographyThin>
              <style.TypographyThin>{selectedData.dataNm}</style.TypographyThin>
            </style.CardContent>
            <style.CardContent>
              <style.TypographyThin>종류</style.TypographyThin>
              <style.TypographyThin>
                {DatasetType[selectedData.dataTp]}
              </style.TypographyThin>
            </style.CardContent>
            <style.CardContent>
              <style.TypographyThin>파일 이름</style.TypographyThin>
              <style.TypographyThin>{selectedData.fileNm}</style.TypographyThin>
            </style.CardContent>
            <style.CardContent>
              <style.TypographyThin>용량</style.TypographyThin>
              <style.TypographyThin>
                {Utils.formatBytes(selectedData.fileSz)}
              </style.TypographyThin>
            </style.CardContent>
            <style.CardContent>
              <style.TypographyThin>생성자</style.TypographyThin>
              <style.TypographyThin>{selectedData.regNm}</style.TypographyThin>
            </style.CardContent>
            <style.CardContent>
              <style.TypographyThin>등록일자</style.TypographyThin>
              <style.TypographyThin>
                <DateViewer date={selectedData.regDt} />
              </style.TypographyThin>
            </style.CardContent>
          </style.Card>

          <Typography
            id="alert-dialog-description"
            align="right"
            style={{ margin: "1.5rem 0.5rem 0 0" }}
          >
            삭제하시겠습니까?
          </Typography>
        </DialogContent>

        <DialogActions>
          <Button onClick={handleClose} color="primary">
            취소
          </Button>
          <Button onClick={handleOk} color="primary" autoFocus>
            확인
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
