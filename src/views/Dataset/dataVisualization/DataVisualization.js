import React from "react";
import BarChart from "./nivo/BarChart";
import LineChart from "./nivo/LineChart";
import ScatterplotChart from "./nivo/ScatterplotChart";
import Histogram from "./d3/Histogram";
import BoxPlot from "./d3/BoxPlot";
import SimpleBarChart from "./react-vis/SimpleBarChart";
// import HistogramTooltip from "./d3/HistogramTooltip";

// scatter plot: 2개 이상의 데이터가 필요함. 각 데이터간의 비교를 위해 사용함.
// line: 시계열. x -> 시간, y -> 척도.
//

function DataVisualization({
  chartDatas,
  width,
  height,
  datas,
  dataNm,
  // chartTp,
  idValue,
  xValue,
  yValue,
  item,
}) {
  // const { chartTp } = chartDatas;
  console.log("datas: ", datas);

  return (
    <>
      {
        {
          line: <LineChart chartDatas={chartDatas} />,
          bar: <BarChart chartDatas={chartDatas} datas={datas} />,
          histogram: <Histogram widthProps={width} heightProps={height} />,
          scatterplot: (
            <ScatterplotChart datas={datas} chartDatas={chartDatas} />
          ),
          simpleBar: <SimpleBarChart />,
          "box plot": <BoxPlot widthProps={width} heightProps={height} />,
        }[`${chartDatas.chartTp}`]
      }
    </>
  );
}

export default DataVisualization;
