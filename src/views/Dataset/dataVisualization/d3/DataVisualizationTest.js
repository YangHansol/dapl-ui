import React, { useRef, useEffect, useState } from "react";
import d3, {
  select,
  line,
  curveLinear,
  axisBottom,
  axisRight,
  scaleLinear,
  scaleBand,
} from "d3";
import styled from "styled-components";
import {
  XYPlot,
  LineSeries,
  VerticalGridLines,
  HorizontalGridLines,
  XAxis,
  YAxis,
  VerticalBarSeries,
  MarkSeries,
  RadialChart,
} from "react-vis";
const Wrapper = styled.svg`
  background-color: grey;
  overflow: visible !important;
`;

function DataVisualizationTest({ datas }) {
  const data = [
    { x: 0, y: 8 },
    { x: 1, y: 5 },
    { x: 2, y: 4 },
    { x: 3, y: 9 },
    { x: 4, y: 1 },
    { x: 5, y: 7 },
    { x: 6, y: 6 },
    { x: 7, y: 3 },
    { x: 8, y: 2 },
    { x: 9, y: 0 },
  ];
  const svgRef = useRef();
  const usingData = datas.slice(0, 10);
  const xData = usingData.map(item => item.age);
  const yData = usingData.map(item => item.target);
  const usingdata = [];
  const c = usingdata.concat(
    usingData.map(item => ({ x: item.age, y: item.target })),
  );
  console.log(xData, yData, c);
  useEffect(() => {
    const svg = select(svgRef.current);
    const xScale = scaleBand()
      .domain(xData)
      .range([0, 300]);

    const yScale = scaleLinear()
      .domain([0, 150])
      .range([150, 0]);

    const xAxis = axisBottom(xScale)
      .ticks(data.length)
      .tickFormat(index => index + 1);
    svg
      .select(".x-axis")
      .style("transform", "translateY(150px)")
      .call(xAxis);

    const yAxis = axisRight(yScale);
    svg
      .select(".y-axis")
      .style("transform", "translateX(300px)")
      .call(yAxis);
  }, [data]);

  return (
    <div className="LineChart">
      <XYPlot height={300} width={300}>
        <VerticalGridLines />
        <HorizontalGridLines />
        <XAxis />
        <YAxis />
        {/* <LineSeries data={data} /> */}
        <VerticalBarSeries data={data} />
      </XYPlot>
      <XYPlot height={300} width={300}>
        <VerticalGridLines />
        <HorizontalGridLines />
        <XAxis />
        <YAxis />
        <LineSeries data={data} />
        {/* <VerticalBarSeries data={data} /> */}
      </XYPlot>
      <XYPlot height={300} width={300}>
        <VerticalGridLines />
        <HorizontalGridLines />
        <XAxis />
        <YAxis />
        {/* <LineSeries data={data} /> */}
        {/* <VerticalBarSeries data={data} /> */}
        <MarkSeries data={data} />
      </XYPlot>
      <XYPlot height={300} width={300}>
        {/* <VerticalGridLines />
        <HorizontalGridLines />
        <XAxis />
        <YAxis /> */}
        {/* <LineSeries data={data} /> */}
        {/* <VerticalBarSeries data={data} /> */}
        {/* <MarkSeries data={data} /> */}
        <RadialChart
          data={data}
          labelsRadiusMultiplier={1.1}
          labelsStyle={{
            fontSize: 12,
          }}
          showLabels
          height={300}
          width={300}
        />
      </XYPlot>
    </div>
  );
}

export default DataVisualizationTest;
