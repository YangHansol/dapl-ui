import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import { select } from "d3";
import styled from "styled-components";

const Wrapper = styled.svg`
  background-color: white;
  overflow: visible;
`;

function BoxPlot({ datas, chartConditions, widthProps, heightProps }) {
  const svgRef = useRef();
  const margin = { top: 20, right: 20, bottom: 30, left: 40 };
  const width = widthProps;
  const height = heightProps;
  const n = width / 40;
  // const data = [
  //   { x: 0.23, y: 326 },
  //   { x: 0.21, y: 326 },
  //   { x: 0.23, y: 327 },
  //   { x: 0.29, y: 334 },
  //   { x: 0.31, y: 335 },
  //   { x: 0.24, y: 336 },
  //   { x: 0.24, y: 336 },
  //   { x: 0.26, y: 337 },
  //   { x: 0.22, y: 337 },
  //   { x: 0.23, y: 338 },
  //   { x: 0.3, y: 339 },
  //   { x: 0.23, y: 340 },
  //   { x: 0.22, y: 342 },
  //   { x: 0.31, y: 344 },
  //   { x: 0.2, y: 345 },
  //   { x: 0.32, y: 345 },
  //   { x: 0.3, y: 348 },
  //   { x: 0.3, y: 351 },
  //   { x: 0.3, y: 351 },
  //   { x: 0.3, y: 351 },
  // ];

  const value = datas.map(item => ({
    x: Number(item[chartConditions.xValue]),
    y: Number(item[chartConditions.yValue]),
  }));

  const bins = d3
    .histogram()
    .thresholds(n)
    .value(d => d.x)(value)
    .map(bin => {
      bin.sort((a, b) => a.y - b.y);
      const values = bin.map(d => d.y);
      const min = values[0];
      const max = values[values.length - 1];
      const q1 = d3.quantile(values, 0.25);
      const q2 = d3.quantile(values, 0.5);
      const q3 = d3.quantile(values, 0.75);
      const iqr = q3 - q1; // interquartile range
      const r0 = Math.max(min, q1 - iqr * 1.5);
      const r1 = Math.min(max, q3 + iqr * 1.5);
      bin.quartiles = [q1, q2, q3];
      bin.range = [r0, r1];
      bin.outliers = bin.filter(v => v.y < r0 || v.y > r1); // TODO
      return bin;
    });

  const x = d3
    .scaleLinear()
    .domain([d3.min(bins, d => d.x0), d3.max(bins, d => d.x1)])
    .rangeRound([margin.left, width - margin.right]);

  const y = d3
    .scaleLinear()
    .domain([d3.min(bins, d => d.range[0]), d3.max(bins, d => d.range[1])])
    .nice()
    .range([height - margin.bottom, margin.top]);

  const yAxis = g =>
    g
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(y).ticks(null, "s"))
      .call(g => g.select(".domain").remove());
  const xAxis = g =>
    g.attr("transform", `translate(0,${height - margin.bottom})`).call(
      d3
        .axisBottom(x)
        .ticks(n)
        .tickSizeOuter(0),
    );

  const chart = () => {
    const svg = select(svgRef.current);

    const g = svg
      .append("g")
      .selectAll("g")
      .data(bins)
      .join("g");

    g.append("path")
      .attr("stroke", "currentColor")
      .attr(
        "d",
        d => `
          M${x((d.x0 + d.x1) / 2)},${y(d.range[1])}
          V${y(d.range[0])}
        `,
      );

    g.append("path")
      .attr("fill", "#ddd")
      .attr(
        "d",
        d => `
          M${x(d.x0) + 1},${y(d.quartiles[2])}
          H${x(d.x1)}
          V${y(d.quartiles[0])}
          H${x(d.x0) + 1}
          Z
        `,
      );

    g.append("path")
      .attr("stroke", "currentColor")
      .attr("stroke-width", 2)
      .attr(
        "d",
        d => `
          M${x(d.x0) + 1},${y(d.quartiles[1])}
          H${x(d.x1)}
        `,
      );

    g.append("g")
      .attr("fill", "currentColor")
      .attr("fill-opacity", 0.2)
      .attr("stroke", "none")
      .attr("transform", d => `translate(${x((d.x0 + d.x1) / 2)},0)`)
      .selectAll("circle")
      .data(d => d.outliers)
      .join("circle")
      .attr("r", 2)
      .attr("cx", () => (Math.random() - 0.5) * 4)
      .attr("cy", d => y(d.y));

    svg.append("g").call(xAxis);

    svg.append("g").call(yAxis);

    return svg.node();
  };

  useEffect(() => {
    chart();
  }, [value]);

  return (
    <div>
      <Wrapper
        ref={svgRef}
        style={{ width: `${widthProps}`, height: `${heightProps}` }}
      >
        <g className="x-axis" />
        <g className="y-axis" />
      </Wrapper>
    </div>
  );
}

export default BoxPlot;
