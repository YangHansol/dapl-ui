import React, { useRef, useEffect, useState } from "react";
import {
  select,
  line,
  curveLinear,
  axisBottom,
  axisRight,
  scaleLinear,
} from "d3";
import styled from "styled-components";

const Wrapper = styled.svg`
  background-color: grey;
  overflow: visible;
`;

function DataVisualization({ datas }) {
  const usingData = datas.slice(0, 10);
  const xData = usingData.map(item => item.target);
  const yData = usingData.map(item => item.pclass);
  const usingdata = usingData.map(item => [item.target, item.pclass]);
  const [data, setData] = useState([25, 30, 45, 60, 20, 65, 75]);
  const svgRef = useRef();

  useEffect(() => {
    const svg = select(svgRef.current);
    const xScale = scaleLinear()
      .domain([0, usingdata.length - 1])
      .range([0, 300]);

    const yScale = scaleLinear()
      .domain([0, 150])
      .range([150, 0]);

    const xAxis = axisBottom(xScale)
      .ticks(usingdata.length)
      .tickFormat(index => index + 1);
    svg
      .select(".x-axis")
      .style("transform", "translateY(150px)")
      .call(xAxis);

    const yAxis = axisRight(yScale);
    svg
      .select(".y-axis")
      .style("transform", "translateX(300px)")
      .call(yAxis);

    const Line = line()
      .x((value, index) => xScale(index))
      .y(yScale)
      .curve(curveLinear);

    svg
      .selectAll(".line")
      .data([usingdata])
      .join("path")
      .attr("class", "line")
      .attr("d", Line)
      .attr("fill", "none")
      .attr("stroke", "blue");
  }, [data]);

  return (
    <>
      <Wrapper ref={svgRef}>
        <g className="x-axis" />
        <g className="y-axis" />
      </Wrapper>
      <button onClick={() => setData(data.map(value => value + 5))}>
        update data
      </button>
      <button onClick={() => setData(data.filter(value => value < 35))}>
        filter data
      </button>
    </>
  );
}

export default DataVisualization;
