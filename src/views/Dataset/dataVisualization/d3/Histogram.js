import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import { select, axisBottom, ticks, scaleLinear, histogram } from "d3";
import styled from "styled-components";

const Wrapper = styled.svg`
  background-color: white;
  overflow: visible;
`;

function Histogram({ datas, chartConditions, widthProps, heightProps }) {
  const svgRef = useRef();
  const margin = { top: 20, right: 20, bottom: 30, left: 40 };
  const height = heightProps;
  const width = widthProps;
  const value = datas.map(item => Number(item[chartConditions.xValue]));
  const data = [
    5.1,
    4.9,
    8.6,
    6.2,
    5.1,
    7.1,
    6.7,
    6.1,
    5,
    5,
    5.2,
    7.9,
    11.1,
    5.9,
    5.5,
    5.6,
    6.5,
    7.7,
    5.7,
    6.7,
    5.7,
  ];

  const x = d3
    .scaleLinear()
    .domain(d3.extent(value))
    .nice()
    .range([margin.left, width - margin.right]);

  const bins = d3
    .histogram()
    .domain(x.domain())
    .thresholds(x.ticks(40))(value);

  const y = d3
    .scaleLinear()
    .domain([0, d3.max(bins, d => d.length)])
    .nice()
    .range([height - margin.bottom, margin.top]);

  const yAxis = g =>
    g
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(y).ticks(height / 40))
      .call(g => g.select(".domain").remove())
      .call(g =>
        g
          .select(".tick:last-of-type text")
          .clone()
          .attr("x", 4)
          .attr("text-anchor", "start")
          .attr("font-weight", "bold")
          .text(value.y),
      );
  const xAxis = g =>
    g
      .attr("transform", `translate(0,${height - margin.bottom})`)
      .call(
        d3
          .axisBottom(x)
          .ticks(width / 80)
          .tickSizeOuter(0),
      )
      .call(g =>
        g
          .append("text")
          .attr("x", width - margin.right)
          .attr("y", -4)
          .attr("fill", "currentColor")
          .attr("font-weight", "bold")
          .attr("text-anchor", "end")
          .text(value.x),
      );

  const chart = () => {
    const svg = select(svgRef.current);

    svg
      .append("g")
      .attr("fill", "steelblue")
      .selectAll("rect")
      .data(bins)
      .join("rect")
      .attr("x", d => x(d.x0) + 1)
      .attr("width", d => Math.max(0, x(d.x1) - x(d.x0) - 1))
      .attr("y", d => y(d.length))
      .attr("height", d => y(0) - y(d.length));

    svg.append("g").call(xAxis);

    svg.append("g").call(yAxis);

    return svg.node();
  };

  useEffect(() => {
    chart();
  }, [data]);

  return (
    <div>
      <Wrapper
        ref={svgRef}
        style={{ width: `${widthProps}`, height: `${heightProps}` }}
      >
        <g className="x-axis" />
        <g className="y-axis" />
      </Wrapper>
    </div>
  );
}

export default Histogram;
