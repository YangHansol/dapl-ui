import React, { useRef } from "react";

import DataVisualization from "../dataVisualization/DataVisualization.js";
import { Button, Grid } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import CloseIcon from "@material-ui/icons/Close";
import moment from "moment";
import styled from "styled-components";
import { DndProvider } from "react-dnd";
import Backend from "react-dnd-html5-backend";
import update from "immutability-helper";
import { useDrag, useDrop } from "react-dnd";
import ItemTypes from "./ItemTypes";

const style = {
  border: "1px dashed gray",
  padding: "0.5rem 1rem",
  marginBottom: ".5rem",
  backgroundColor: "white",
  cursor: "move",
};

const ChartEachWrapper = styled.div`
  border: ${props => props.theme.Border};
`;

const ChartInfo = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 1.2rem;
  padding: 0 1rem;
  margin: 0.2rem 0;
  border-bottom: 1px solid #e1e1e1;
  height: 2.6rem;
  &.chartTitle {
    font-size: 2rem;
    border-bottom: none;
    padding-top: 0.5rem;
  }

  &.chartInfo {
    padding-top: 0.5rem;
    justify-content: flex-start;
    align-items: baseline;
  }
`;

const ChartSize = styled.div`
  /* width: 1000px;
  height: 500px;
  &.small {
    width: 380px;
    height: 380px;
  } */
  width: 380px;
  height: 380px;
  display: flex;
  justify-content: center;
  align-items: center;
  &.big {
    width: 1000px;
    height: 500px;
    flex-direction: column;
  }
`;

function ChartCard({ data, datas, handleOpen, onDelete }) {
  return (
    <Grid item>
      <ChartEachWrapper>
        <ChartInfo className="chartTitle">
          <div>{data.chartNm}</div>
          <div>
            <Button
              onClick={() => handleOpen(data.id)}
              color="primary"
              variant="text"
              style={{
                minWidth: "unset",
                backgroundColor: "unset",
                margin: "0 0.3rem",
              }}
            >
              <SearchIcon color="primary" />
            </Button>
            <Button
              onClick={() => onDelete(data.id)}
              color="primary"
              variant="text"
              style={{
                minWidth: "unset",
                backgroundColor: "unset",
                margin: "0 0.3rem",
              }}
            >
              <CloseIcon />
            </Button>
          </div>
        </ChartInfo>
        <ChartInfo className="chartInfo">
          <span
            style={{
              fontSize: "0.5rem",
              color: "gray",
            }}
          >
            by
          </span>
          <span style={{ margin: "0 0.5rem" }}>regNm</span>
          <div style={{ fontSize: "0.5rem" }}>
            {`(${moment(data.date).format("YYYY-MM-DD")})`}
          </div>
        </ChartInfo>

        <ChartSize>
          <DataVisualization
            datas={datas}
            id={data.id}
            dataNm={data.dataNm}
            chartTp={data.chartTp}
            idValue={data.idValue}
            xValue={data.xValue}
            yValue={data.yValue}
            onDelete={onDelete}
            width={300}
            height={300}
          />
        </ChartSize>
      </ChartEachWrapper>
    </Grid>
  );
}

export default ChartCard;
