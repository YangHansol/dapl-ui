export const datas = [
  { id: "a", target: 1, pclass: 3, age: 1, fare: 0, isalone: 0, argclass: 3 },
  { id: "B", target: 1, pclass: 1, age: 2, fare: 3, isalone: 0, argclass: 2 },
  {
    id: "c",
    target: 1,
    pclass: 3,
    age: 1,
    fare: null,
    isalone: 1,
    argclass: 3,
  },
  { id: "d", target: 1, pclass: 1, age: 2, fare: 3, isalone: 0, argclass: 2 },
  { id: "e", target: 0, pclass: 3, age: 2, fare: 1, isalone: 1, argclass: 6 },
  { id: "f", target: 0, pclass: 3, age: 1, fare: 1, isalone: 1, argclass: 3 },
  { id: "g", target: 0, pclass: 1, age: 3, fare: 3, isalone: 1, argclass: 3 },
  { id: "h", target: 0, pclass: 3, age: 0, fare: 2, isalone: 0, argclass: 0 },
  { id: "i", target: 1, pclass: 3, age: 1, fare: 1, isalone: 0, argclass: 3 },
  { id: "j", target: 1, pclass: 2, age: 0, fare: 2, isalone: 0, argclass: 0 },
  { id: "k", target: 1, pclass: 3, age: 0, fare: 2, isalone: 0, argclass: 0 },
];

export const barData = [
  {
    country: "AD",
    "hot dog": 148,
    "hot dogColor": "hsl(156, 70%, 50%)",
    burger: 186,
    burgerColor: "hsl(164, 70%, 50%)",
    sandwich: 166,
    sandwichColor: "hsl(145, 70%, 50%)",
    kebab: 184,
    kebabColor: "hsl(307, 70%, 50%)",
    fries: 64,
    friesColor: "hsl(200, 70%, 50%)",
    donut: 80,
    donutColor: "hsl(191, 70%, 50%)",
  },
  {
    country: "AE",
    "hot dog": 37,
    "hot dogColor": "hsl(142, 70%, 50%)",
    burger: 174,
    burgerColor: "hsl(49, 70%, 50%)",
    sandwich: 109,
    sandwichColor: "hsl(260, 70%, 50%)",
    kebab: 91,
    kebabColor: "hsl(202, 70%, 50%)",
    fries: 135,
    friesColor: "hsl(283, 70%, 50%)",
    donut: 163,
    donutColor: "hsl(265, 70%, 50%)",
  },
  {
    country: "AF",
    "hot dog": 168,
    "hot dogColor": "hsl(339, 70%, 50%)",
    burger: 163,
    burgerColor: "hsl(123, 70%, 50%)",
    sandwich: 97,
    sandwichColor: "hsl(234, 70%, 50%)",
    kebab: 11,
    kebabColor: "hsl(354, 70%, 50%)",
    fries: 107,
    friesColor: "hsl(190, 70%, 50%)",
    donut: 9,
    donutColor: "hsl(336, 70%, 50%)",
  },
  {
    country: "AG",
    "hot dog": 118,
    "hot dogColor": "hsl(347, 70%, 50%)",
    burger: 94,
    burgerColor: "hsl(115, 70%, 50%)",
    sandwich: 75,
    sandwichColor: "hsl(59, 70%, 50%)",
    kebab: 135,
    kebabColor: "hsl(212, 70%, 50%)",
    fries: 85,
    friesColor: "hsl(325, 70%, 50%)",
    donut: 159,
    donutColor: "hsl(60, 70%, 50%)",
  },
  {
    country: "AI",
    "hot dog": 108,
    "hot dogColor": "hsl(20, 70%, 50%)",
    burger: 12,
    burgerColor: "hsl(358, 70%, 50%)",
    sandwich: 77,
    sandwichColor: "hsl(112, 70%, 50%)",
    kebab: 79,
    kebabColor: "hsl(230, 70%, 50%)",
    fries: 136,
    friesColor: "hsl(204, 70%, 50%)",
    donut: 43,
    donutColor: "hsl(149, 70%, 50%)",
  },
  {
    country: "AL",
    "hot dog": 92,
    "hot dogColor": "hsl(260, 70%, 50%)",
    burger: 46,
    burgerColor: "hsl(206, 70%, 50%)",
    sandwich: 93,
    sandwichColor: "hsl(290, 70%, 50%)",
    kebab: 102,
    kebabColor: "hsl(343, 70%, 50%)",
    fries: 193,
    friesColor: "hsl(191, 70%, 50%)",
    donut: 79,
    donutColor: "hsl(91, 70%, 50%)",
  },
  {
    country: "AM",
    "hot dog": 17,
    "hot dogColor": "hsl(62, 70%, 50%)",
    burger: 139,
    burgerColor: "hsl(324, 70%, 50%)",
    sandwich: 129,
    sandwichColor: "hsl(156, 70%, 50%)",
    kebab: 102,
    kebabColor: "hsl(78, 70%, 50%)",
    fries: 70,
    friesColor: "hsl(263, 70%, 50%)",
    donut: 117,
    donutColor: "hsl(209, 70%, 50%)",
  },
];

export const singleBarData = [
  {
    country: "AD",
    "hot dog": 148,
    donut: 80,
  },
  {
    country: "AE",
    "hot dog": 37,
    donut: 163,
  },
  {
    country: "AF",
    "hot dog": 168,
    donut: 9,
  },
  {
    country: "AG",
    "hot dog": 118,
    donut: 159,
  },
  {
    country: "AI",
    "hot dog": 108,
    donut: 43,
  },
  {
    country: "AL",
    "hot dog": 92,
    donut: 79,
  },
  {
    country: "AM",
    "hot dog": 17,
    donut: 117,
  },
];

export const dataSample = [
  { x: "a", y: 8 },
  { x: "b", y: 5 },
  { x: "c", y: 4 },
  { x: "d", y: 9 },
  { x: "e", y: 1 },
  { x: "f", y: 7 },
  { x: "g", y: 6 },
  { x: "h", y: 3 },
  { x: "i", y: 2 },
  { x: "j", y: 0 },
];

export const singleLineData = [
  {
    id: 1,
    data: [
      { x: "a", y: 8 },
      { x: "b", y: 5 },
      { x: "c", y: 4 },
      { x: "d", y: 9 },
      { x: "e", y: 1 },
      { x: "f", y: 7 },
      { x: "g", y: 6 },
      { x: "h", y: 3 },
      { x: "i", y: 2 },
      { x: "j", y: 0 },
    ],
  },
];

export const multiLineData = [
  {
    id: "japan",
    color: "hsl(208, 70%, 50%)",
    data: [
      {
        x: "plane",
        y: 248,
      },
      {
        x: "helicopter",
        y: 142,
      },
      {
        x: "boat",
        y: 126,
      },
      {
        x: "train",
        y: 144,
      },
      {
        x: "subway",
        y: 261,
      },
      {
        x: "bus",
        y: 102,
      },
      {
        x: "car",
        y: 166,
      },
      {
        x: "moto",
        y: 172,
      },
      {
        x: "bicycle",
        y: 97,
      },
      {
        x: "horse",
        y: 104,
      },
      {
        x: "skateboard",
        y: 19,
      },
      {
        x: "others",
        y: 273,
      },
    ],
  },
  {
    id: "france",
    color: "hsl(217, 70%, 50%)",
    data: [
      {
        x: "plane",
        y: 216,
      },
      {
        x: "helicopter",
        y: 44,
      },
      {
        x: "boat",
        y: 39,
      },
      {
        x: "train",
        y: 32,
      },
      {
        x: "subway",
        y: 267,
      },
      {
        x: "bus",
        y: 144,
      },
      {
        x: "car",
        y: 114,
      },
      {
        x: "moto",
        y: 141,
      },
      {
        x: "bicycle",
        y: 279,
      },
      {
        x: "horse",
        y: 180,
      },
      {
        x: "skateboard",
        y: 80,
      },
      {
        x: "others",
        y: 79,
      },
    ],
  },
  {
    id: "us",
    color: "hsl(235, 70%, 50%)",
    data: [
      {
        x: "plane",
        y: 87,
      },
      {
        x: "helicopter",
        y: 54,
      },
      {
        x: "boat",
        y: 271,
      },
      {
        x: "train",
        y: 161,
      },
      {
        x: "subway",
        y: 28,
      },
      {
        x: "bus",
        y: 185,
      },
      {
        x: "car",
        y: 75,
      },
      {
        x: "moto",
        y: 154,
      },
      {
        x: "bicycle",
        y: 231,
      },
      {
        x: "horse",
        y: 145,
      },
      {
        x: "skateboard",
        y: 47,
      },
      {
        x: "others",
        y: 217,
      },
    ],
  },
  {
    id: "germany",
    color: "hsl(119, 70%, 50%)",
    data: [
      {
        x: "plane",
        y: 82,
      },
      {
        x: "helicopter",
        y: 136,
      },
      {
        x: "boat",
        y: 67,
      },
      {
        x: "train",
        y: 52,
      },
      {
        x: "subway",
        y: 153,
      },
      {
        x: "bus",
        y: 282,
      },
      {
        x: "car",
        y: 173,
      },
      {
        x: "moto",
        y: 142,
      },
      {
        x: "bicycle",
        y: 172,
      },
      {
        x: "horse",
        y: 283,
      },
      {
        x: "skateboard",
        y: 41,
      },
      {
        x: "others",
        y: 173,
      },
    ],
  },
  {
    id: "norway",
    color: "hsl(347, 70%, 50%)",
    data: [
      {
        x: "plane",
        y: 100,
      },
      {
        x: "helicopter",
        y: 204,
      },
      {
        x: "boat",
        y: 75,
      },
      {
        x: "train",
        y: 186,
      },
      {
        x: "subway",
        y: 82,
      },
      {
        x: "bus",
        y: 145,
      },
      {
        x: "car",
        y: 25,
      },
      {
        x: "moto",
        y: 290,
      },
      {
        x: "bicycle",
        y: 187,
      },
      {
        x: "horse",
        y: 287,
      },
      {
        x: "skateboard",
        y: 262,
      },
      {
        x: "others",
        y: 184,
      },
    ],
  },
];

export const scatterplotData = [
  {
    id: "group A",
    data: [
      {
        x: 10,
        y: 47,
      },
      {
        x: 92,
        y: 31,
      },
      {
        x: 22,
        y: 30,
      },
      {
        x: 52,
        y: 22,
      },
      {
        x: 64,
        y: 73,
      },
      {
        x: 70,
        y: 8,
      },
      {
        x: 22,
        y: 17,
      },
      {
        x: 31,
        y: 72,
      },
      {
        x: 52,
        y: 88,
      },
      {
        x: 18,
        y: 26,
      },
      {
        x: 67,
        y: 40,
      },
      {
        x: 22,
        y: 87,
      },
      {
        x: 8,
        y: 87,
      },
      {
        x: 96,
        y: 60,
      },
      {
        x: 65,
        y: 53,
      },
      {
        x: 36,
        y: 60,
      },
      {
        x: 13,
        y: 79,
      },
      {
        x: 27,
        y: 28,
      },
      {
        x: 94,
        y: 1,
      },
      {
        x: 77,
        y: 9,
      },
      {
        x: 7,
        y: 116,
      },
      {
        x: 26,
        y: 71,
      },
      {
        x: 46,
        y: 110,
      },
      {
        x: 23,
        y: 84,
      },
      {
        x: 93,
        y: 97,
      },
      {
        x: 7,
        y: 51,
      },
      {
        x: 84,
        y: 97,
      },
      {
        x: 48,
        y: 56,
      },
      {
        x: 87,
        y: 40,
      },
      {
        x: 9,
        y: 23,
      },
      {
        x: 59,
        y: 76,
      },
      {
        x: 32,
        y: 29,
      },
      {
        x: 66,
        y: 68,
      },
      {
        x: 64,
        y: 95,
      },
      {
        x: 33,
        y: 46,
      },
      {
        x: 23,
        y: 20,
      },
      {
        x: 20,
        y: 86,
      },
      {
        x: 85,
        y: 75,
      },
      {
        x: 24,
        y: 20,
      },
      {
        x: 75,
        y: 102,
      },
      {
        x: 30,
        y: 56,
      },
      {
        x: 56,
        y: 82,
      },
      {
        x: 23,
        y: 40,
      },
      {
        x: 75,
        y: 31,
      },
      {
        x: 72,
        y: 50,
      },
      {
        x: 49,
        y: 8,
      },
      {
        x: 37,
        y: 45,
      },
      {
        x: 73,
        y: 61,
      },
      {
        x: 72,
        y: 3,
      },
      {
        x: 96,
        y: 53,
      },
    ],
  },
  {
    id: "group B",
    data: [
      {
        x: 86,
        y: 22,
      },
      {
        x: 38,
        y: 62,
      },
      {
        x: 52,
        y: 17,
      },
      {
        x: 94,
        y: 84,
      },
      {
        x: 0,
        y: 24,
      },
      {
        x: 90,
        y: 10,
      },
      {
        x: 69,
        y: 99,
      },
      {
        x: 74,
        y: 1,
      },
      {
        x: 6,
        y: 66,
      },
      {
        x: 19,
        y: 104,
      },
      {
        x: 49,
        y: 53,
      },
      {
        x: 61,
        y: 89,
      },
      {
        x: 14,
        y: 95,
      },
      {
        x: 26,
        y: 73,
      },
      {
        x: 30,
        y: 64,
      },
      {
        x: 8,
        y: 31,
      },
      {
        x: 18,
        y: 3,
      },
      {
        x: 20,
        y: 112,
      },
      {
        x: 33,
        y: 11,
      },
      {
        x: 28,
        y: 4,
      },
      {
        x: 49,
        y: 84,
      },
      {
        x: 6,
        y: 1,
      },
      {
        x: 58,
        y: 19,
      },
      {
        x: 16,
        y: 102,
      },
      {
        x: 90,
        y: 23,
      },
      {
        x: 37,
        y: 71,
      },
      {
        x: 5,
        y: 107,
      },
      {
        x: 21,
        y: 65,
      },
      {
        x: 27,
        y: 77,
      },
      {
        x: 35,
        y: 109,
      },
      {
        x: 86,
        y: 79,
      },
      {
        x: 51,
        y: 42,
      },
      {
        x: 83,
        y: 64,
      },
      {
        x: 61,
        y: 2,
      },
      {
        x: 5,
        y: 3,
      },
      {
        x: 11,
        y: 33,
      },
      {
        x: 32,
        y: 34,
      },
      {
        x: 9,
        y: 61,
      },
      {
        x: 9,
        y: 92,
      },
      {
        x: 32,
        y: 76,
      },
      {
        x: 83,
        y: 58,
      },
      {
        x: 81,
        y: 84,
      },
      {
        x: 75,
        y: 101,
      },
      {
        x: 47,
        y: 66,
      },
      {
        x: 37,
        y: 104,
      },
      {
        x: 79,
        y: 75,
      },
      {
        x: 81,
        y: 13,
      },
      {
        x: 58,
        y: 83,
      },
      {
        x: 93,
        y: 111,
      },
      {
        x: 29,
        y: 39,
      },
    ],
  },
  {
    id: "group C",
    data: [
      {
        x: 83,
        y: 32,
      },
      {
        x: 91,
        y: 68,
      },
      {
        x: 67,
        y: 81,
      },
      {
        x: 35,
        y: 8,
      },
      {
        x: 60,
        y: 59,
      },
      {
        x: 23,
        y: 105,
      },
      {
        x: 26,
        y: 42,
      },
      {
        x: 8,
        y: 87,
      },
      {
        x: 80,
        y: 66,
      },
      {
        x: 13,
        y: 109,
      },
      {
        x: 99,
        y: 32,
      },
      {
        x: 80,
        y: 106,
      },
      {
        x: 2,
        y: 97,
      },
      {
        x: 71,
        y: 45,
      },
      {
        x: 65,
        y: 86,
      },
      {
        x: 18,
        y: 19,
      },
      {
        x: 25,
        y: 116,
      },
      {
        x: 11,
        y: 78,
      },
      {
        x: 36,
        y: 17,
      },
      {
        x: 9,
        y: 2,
      },
      {
        x: 91,
        y: 9,
      },
      {
        x: 98,
        y: 7,
      },
      {
        x: 89,
        y: 76,
      },
      {
        x: 28,
        y: 68,
      },
      {
        x: 83,
        y: 92,
      },
      {
        x: 84,
        y: 16,
      },
      {
        x: 12,
        y: 31,
      },
      {
        x: 4,
        y: 68,
      },
      {
        x: 14,
        y: 27,
      },
      {
        x: 96,
        y: 19,
      },
      {
        x: 87,
        y: 82,
      },
      {
        x: 78,
        y: 39,
      },
      {
        x: 69,
        y: 109,
      },
      {
        x: 33,
        y: 86,
      },
      {
        x: 91,
        y: 45,
      },
      {
        x: 34,
        y: 88,
      },
      {
        x: 33,
        y: 110,
      },
      {
        x: 51,
        y: 101,
      },
      {
        x: 20,
        y: 37,
      },
      {
        x: 88,
        y: 14,
      },
      {
        x: 53,
        y: 1,
      },
      {
        x: 12,
        y: 19,
      },
      {
        x: 80,
        y: 114,
      },
      {
        x: 84,
        y: 20,
      },
      {
        x: 32,
        y: 80,
      },
      {
        x: 73,
        y: 23,
      },
      {
        x: 72,
        y: 5,
      },
      {
        x: 87,
        y: 57,
      },
      {
        x: 82,
        y: 72,
      },
      {
        x: 76,
        y: 84,
      },
    ],
  },
  {
    id: "group D",
    data: [
      {
        x: 78,
        y: 34,
      },
      {
        x: 88,
        y: 73,
      },
      {
        x: 27,
        y: 22,
      },
      {
        x: 75,
        y: 120,
      },
      {
        x: 96,
        y: 112,
      },
      {
        x: 8,
        y: 23,
      },
      {
        x: 56,
        y: 38,
      },
      {
        x: 86,
        y: 1,
      },
      {
        x: 70,
        y: 110,
      },
      {
        x: 65,
        y: 61,
      },
      {
        x: 86,
        y: 69,
      },
      {
        x: 61,
        y: 52,
      },
      {
        x: 93,
        y: 53,
      },
      {
        x: 81,
        y: 9,
      },
      {
        x: 62,
        y: 38,
      },
      {
        x: 11,
        y: 79,
      },
      {
        x: 61,
        y: 114,
      },
      {
        x: 82,
        y: 104,
      },
      {
        x: 18,
        y: 120,
      },
      {
        x: 82,
        y: 42,
      },
      {
        x: 59,
        y: 66,
      },
      {
        x: 33,
        y: 95,
      },
      {
        x: 20,
        y: 66,
      },
      {
        x: 8,
        y: 59,
      },
      {
        x: 38,
        y: 55,
      },
      {
        x: 60,
        y: 24,
      },
      {
        x: 29,
        y: 44,
      },
      {
        x: 60,
        y: 22,
      },
      {
        x: 34,
        y: 65,
      },
      {
        x: 86,
        y: 113,
      },
      {
        x: 71,
        y: 108,
      },
      {
        x: 22,
        y: 6,
      },
      {
        x: 44,
        y: 13,
      },
      {
        x: 45,
        y: 70,
      },
      {
        x: 32,
        y: 112,
      },
      {
        x: 43,
        y: 25,
      },
      {
        x: 69,
        y: 118,
      },
      {
        x: 96,
        y: 29,
      },
      {
        x: 100,
        y: 95,
      },
      {
        x: 9,
        y: 70,
      },
      {
        x: 57,
        y: 47,
      },
      {
        x: 65,
        y: 88,
      },
      {
        x: 56,
        y: 6,
      },
      {
        x: 43,
        y: 77,
      },
      {
        x: 90,
        y: 36,
      },
      {
        x: 6,
        y: 4,
      },
      {
        x: 13,
        y: 34,
      },
      {
        x: 59,
        y: 69,
      },
      {
        x: 56,
        y: 54,
      },
      {
        x: 65,
        y: 21,
      },
    ],
  },
  {
    id: "group E",
    data: [
      {
        x: 42,
        y: 40,
      },
      {
        x: 18,
        y: 71,
      },
      {
        x: 16,
        y: 2,
      },
      {
        x: 73,
        y: 96,
      },
      {
        x: 21,
        y: 82,
      },
      {
        x: 100,
        y: 98,
      },
      {
        x: 9,
        y: 76,
      },
      {
        x: 94,
        y: 35,
      },
      {
        x: 47,
        y: 113,
      },
      {
        x: 24,
        y: 64,
      },
      {
        x: 3,
        y: 108,
      },
      {
        x: 88,
        y: 93,
      },
      {
        x: 98,
        y: 95,
      },
      {
        x: 94,
        y: 94,
      },
      {
        x: 9,
        y: 13,
      },
      {
        x: 41,
        y: 116,
      },
      {
        x: 35,
        y: 45,
      },
      {
        x: 4,
        y: 6,
      },
      {
        x: 65,
        y: 96,
      },
      {
        x: 14,
        y: 59,
      },
      {
        x: 64,
        y: 106,
      },
      {
        x: 79,
        y: 115,
      },
      {
        x: 57,
        y: 30,
      },
      {
        x: 79,
        y: 29,
      },
      {
        x: 0,
        y: 27,
      },
      {
        x: 29,
        y: 98,
      },
      {
        x: 83,
        y: 1,
      },
      {
        x: 18,
        y: 14,
      },
      {
        x: 78,
        y: 98,
      },
      {
        x: 75,
        y: 44,
      },
      {
        x: 26,
        y: 112,
      },
      {
        x: 4,
        y: 102,
      },
      {
        x: 6,
        y: 105,
      },
      {
        x: 74,
        y: 82,
      },
      {
        x: 0,
        y: 82,
      },
      {
        x: 85,
        y: 71,
      },
      {
        x: 57,
        y: 88,
      },
      {
        x: 56,
        y: 10,
      },
      {
        x: 25,
        y: 12,
      },
      {
        x: 0,
        y: 100,
      },
      {
        x: 11,
        y: 91,
      },
      {
        x: 67,
        y: 104,
      },
      {
        x: 28,
        y: 58,
      },
      {
        x: 37,
        y: 112,
      },
      {
        x: 57,
        y: 113,
      },
      {
        x: 11,
        y: 14,
      },
      {
        x: 44,
        y: 109,
      },
      {
        x: 39,
        y: 69,
      },
      {
        x: 85,
        y: 111,
      },
      {
        x: 9,
        y: 101,
      },
    ],
  },
];

export const histogramData = [
  {
    id: "histogram",
    data: [
      { x: 10, y: 1, value: 30 },
      { x: 10, y: 2, value: 40 },
      { x: 10, y: 3, value: 50 },
      { x: 10, y: 4, value: 60 },
      { x: 10, y: 5, value: 40 },
      { x: 10, y: 6, value: 30 },

      { x: 20, y: 1, value: 70 },
      { x: 20, y: 2, value: 80 },
      { x: 20, y: 3, value: 90 },
      { x: 20, y: 4, value: 90 },
      { x: 20, y: 5, value: 80 },
      { x: 20, y: 6, value: 70 },
      { x: 20, y: 7, value: 70 },
      { x: 20, y: 8, value: 60 },
      { x: 20, y: 9, value: 60 },
      { x: 20, y: 10, value: 50 },
      { x: 20, y: 11, value: 40 },
      { x: 20, y: 12, value: 30 },

      { x: 30, y: 1, value: 100 },
      { x: 30, y: 2, value: 100 },
      { x: 30, y: 3, value: 80 },
      { x: 30, y: 4, value: 70 },
      { x: 30, y: 5, value: 80 },
      { x: 30, y: 6, value: 90 },
      { x: 30, y: 7, value: 100 },
      { x: 30, y: 8, value: 60 },
      { x: 30, y: 9, value: 50 },
      { x: 30, y: 10, value: 40 },
      { x: 30, y: 11, value: 40 },
      { x: 30, y: 12, value: 30 },
      { x: 30, y: 13, value: 40 },
      { x: 30, y: 14, value: 50 },
      { x: 30, y: 15, value: 30 },

      { x: 40, y: 1, value: 100 },
      { x: 40, y: 2, value: 100 },
      { x: 40, y: 3, value: 100 },
      { x: 40, y: 4, value: 70 },
      { x: 40, y: 5, value: 60 },
      { x: 40, y: 6, value: 50 },
      { x: 40, y: 7, value: 40 },
      { x: 40, y: 8, value: 50 },
      { x: 40, y: 9, value: 60 },
      { x: 40, y: 10, value: 70 },
      { x: 40, y: 11, value: 60 },
      { x: 40, y: 12, value: 50 },
      { x: 40, y: 13, value: 40 },

      { x: 50, y: 1, value: 80 },
      { x: 50, y: 2, value: 70 },
      { x: 50, y: 3, value: 60 },
      { x: 50, y: 4, value: 70 },
      { x: 50, y: 5, value: 60 },
      { x: 50, y: 6, value: 50 },
      { x: 50, y: 7, value: 40 },
      { x: 50, y: 8, value: 50 },
      { x: 50, y: 9, value: 40 },
      { x: 50, y: 10, value: 30 },
      { x: 50, y: 11, value: 20 },

      { x: 60, y: 1, value: 60 },
      { x: 60, y: 2, value: 60 },
      { x: 60, y: 3, value: 50 },
      { x: 60, y: 4, value: 40 },
      { x: 60, y: 5, value: 40 },

      { x: 70, y: 1, value: 40 },
      { x: 70, y: 2, value: 40 },
      { x: 70, y: 3, value: 30 },
      { x: 70, y: 4, value: 20 },
    ],
  },
];
