import React from "react";
import { scaleLinear } from "d3-scale";
import { ScatterPlot } from "@nivo/scatterplot";
import { histogramData } from "../datas";

const sizeScale = scaleLinear()
  .domain([0, 100])
  .range([0, 20]);

const getSymbolSize = d => sizeScale(d.value);

const annotations = [
  {
    x0: 5,
    y0: 0,
    x1: 15,
    y1: 7,
  },
  {
    x0: 55,
    y0: 0,
    x1: 75,
    y1: 6,
  },
];

const Annotations = ({ xScale, yScale }) => {
  return annotations.map((annotation, i) => {
    const x0 = xScale(annotation.x0);
    const y0 = yScale(annotation.y0);
    const x1 = xScale(annotation.x1);
    const y1 = yScale(annotation.y1);

    const width = x1 - x0;
    const height = y0 - y1;

    return (
      <rect
        key={i}
        x={x0}
        y={y1}
        width={width}
        height={height}
        rx={10}
        fill="none"
        stroke="orange"
        strokeWidth={2}
      />
    );
  });
};

function HistogramChart() {
  const data = histogramData;
  console.log("histogram data: ", data);
  return (
    <div>
      <ScatterPlot
        width={400}
        height={500}
        margin={{ top: 60, right: 140, bottom: 70, left: 90 }}
        data={data}
        xScale={{
          type: "linear",
          min: 0,
          max: 80,
        }}
        yScale={{
          type: "linear",
          min: 0,
          max: 16,
        }}
        margin={{
          top: 10,
          right: 10,
          bottom: 36,
          left: 36,
        }}
        // nodeSize={30}
        nodeSize={getSymbolSize}
        colors={["#0095ff"]}
        enableGridX={false}
        layers={[
          "grid",
          "axes",
          "nodes",
          // Annotations,
          "markers",
          "mesh",
          "legends",
        ]}
        colors={{ scheme: "purple_orange" }}
        blendMode="multiply"
        axisTop={null}
        axisRight={null}
        axisBottom={{
          orient: "bottom",
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: "weight",
          legendPosition: "middle",
          legendOffset: 46,
        }}
        axisLeft={{
          orient: "left",
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: "size",
          legendPosition: "middle",
          legendOffset: -60,
        }}
        legends={[
          {
            anchor: "bottom-right",
            direction: "column",
            justify: false,
            translateX: 130,
            translateY: 0,
            itemWidth: 100,
            itemHeight: 12,
            itemsSpacing: 5,
            itemDirection: "left-to-right",
            symbolSize: 12,
            symbolShape: "circle",
            effects: [
              {
                on: "hover",
                style: {
                  itemOpacity: 1,
                },
              },
            ],
          },
        ]}
      />
    </div>
  );
}

export default HistogramChart;
