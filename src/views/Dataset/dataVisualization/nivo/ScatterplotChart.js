import React from "react";
import { ResponsiveScatterPlot } from "@nivo/scatterplot";
import { datas, barData, multiLineData, scatterplotData } from "../datas";

function ScatterplotChart({ datas, chartConditions, chartDatas }) {
  const value = datas.map(item => ({
    x: Number(item[chartDatas.axis.xAxis]),
    y: Number(item[chartDatas.axis.yAxis]),
  }));
  // const data = [
  //   {
  //     id: chartConditions.idValue,
  //     data: value,
  //   },
  // ];

  const dataValue = [{ id: chartDatas.conditions.id, data: value }];
  console.log("dataValue: ", dataValue);
  const data = dataValue;
  console.log("data: ", data);

  return (
    <ResponsiveScatterPlot
      data={data}
      margin={{ top: 50, right: 90, bottom: 90, left: 90 }}
      xScale={{ type: "linear", min: 0, max: "auto" }}
      // xFormat={function(e) {
      //   return e + " kg";
      // }}
      yScale={{ type: "linear", min: 0, max: "auto" }}
      // yFormat={function(e) {
      //   return e + " cm";
      // }}
      colors={{ scheme: "purple_orange" }}
      blendMode="multiply"
      axisTop={null}
      axisRight={null}
      axisBottom={{
        orient: "bottom",
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: `${chartDatas.axis.yAxis}`,
        legendPosition: "middle",
        legendOffset: 46,
      }}
      axisLeft={{
        orient: "left",
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: `${chartDatas.axis.xAxis}`,
        legendPosition: "middle",
        legendOffset: -75,
      }}
      legends={[
        {
          anchor: "bottom-right",
          direction: "column",
          justify: false,
          translateX: 130,
          translateY: 0,
          itemWidth: 100,
          itemHeight: 12,
          itemsSpacing: 5,
          itemDirection: "left-to-right",
          symbolSize: 12,
          symbolShape: "circle",
          effects: [
            {
              on: "hover",
              style: {
                itemOpacity: 1,
              },
            },
          ],
        },
      ]}
    />
  );
}

export default ScatterplotChart;
