import React, { useState } from "react";
import { ResponsiveBar } from "@nivo/bar";
import { datas as datasProps, barData, singleBarData } from "../datas";

function BarChart({ datas, chartDatas }) {
  // keys 로 들어가는 값이 number 가 되야함.
  const [value, setValue] = useState(
    datas.map(item => ({
      ...item,
      [chartDatas.axis.yAxis]: Number(item[chartDatas.axis.yAxis]),
    })),
  );

  // age -> xAxis
  // survive -> yAxis -> number

  const data = value;
  console.log(value);
  return (
    <ResponsiveBar
      data={data}
      keys={[`${chartDatas.axis.yAxis}`]}
      indexBy={chartDatas.axis.xAxis}
      // data={barData}
      // keys={["survive"]}
      // indexBy="age"
      margin={{ top: 50, right: 90, bottom: 90, left: 90 }}
      // padding={0.3}
      groupMode="grouped"
      colors={{ scheme: "purple_orange" }}
      defs={[
        {
          id: "dots",
          type: "patternDots",
          background: "inherit",
          color: "#38bcb2",
          size: 4,
          padding: 1,
          stagger: true,
        },
        {
          id: "lines",
          type: "patternLines",
          background: "inherit",
          color: "#eed312",
          rotation: -45,
          lineWidth: 6,
          spacing: 10,
        },
      ]}
      borderColor={{ from: "color", modifiers: [["darker", 1.6]] }}
      axisTop={null}
      axisRight={null}
      axisBottom={{
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: `${chartDatas.axis.xAxis}`,
        // legend: "age",
        legendPosition: "middle",
        legendOffset: 46,
      }}
      axisLeft={{
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: `${chartDatas.axis.yAxis}`,
        // legend: "survive",
        legendPosition: "middle",
        legendOffset: -75,
      }}
      enableLabel={false}
      labelSkipWidth={12}
      labelSkipHeight={12}
      labelTextColor={{ from: "color", modifiers: [["darker", 1.6]] }}
      legends={[
        {
          dataFrom: "keys",
          anchor: "bottom-right",
          direction: "column",
          justify: false,
          translateX: 120,
          translateY: 0,
          itemsSpacing: 2,
          itemWidth: 100,
          itemHeight: 20,
          itemDirection: "left-to-right",
          itemOpacity: 0.85,
          symbolSize: 20,
          effects: [
            {
              on: "hover",
              style: {
                itemOpacity: 1,
              },
            },
          ],
        },
      ]}
      animate={true}
      motionStiffness={90}
      motionDamping={15}
    />
  );
}

export default BarChart;
