import React, { useState } from "react";
import {
  XYPlot,
  LineSeries,
  VerticalGridLines,
  HorizontalGridLines,
  XAxis,
  YAxis,
  VerticalBarSeries,
  MarkSeries,
  Hint,
} from "react-vis";
import styled from "styled-components";
import { datas, barData, dataSample } from "../datas";

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

function SimpleBarChart({ datas, chartConditions }) {
  console.log("datas: ", datas);
  console.log("nivo data", barData);
  console.log("data sample: ", dataSample);

  const data = datas.map(item => ({
    x: item[chartConditions.xValue],
    y: item[chartConditions.yValue],
  }));
  console.log("data for graph: ", data);

  const [value, setValue] = useState(null);

  const forgetValue = () => {
    setValue(null);
  };

  const rememberValue = value => {
    setValue(value);
  };

  return (
    <Wrapper>
      {/* <XYPlot height={300} width={300} xType="ordinal">
        <VerticalGridLines />
        <HorizontalGridLines />
        <XAxis />
        <YAxis />
        <LineSeries
          data={dataSample}
          onValueMouseOver={rememberValue}
          onValueMouseOut={forgetValue}
        />
        {value ? <Hint value={value} /> : null}
        <Hint value={value} />
      </XYPlot> */}
      <XYPlot height={300} width={300} xType="ordinal">
        <VerticalGridLines />
        <HorizontalGridLines />
        <XAxis />
        <YAxis />
        <VerticalBarSeries data={data} />
        {/* <VerticalBarSeries data={dataSample} color="#9c27b0" /> */}
        {/* <VerticalBarSeries data={dataSample} color="#ede7f6" /> */}
      </XYPlot>
      {/* <XYPlot height={300} width={300} xType="ordinal">
        <VerticalGridLines />
        <HorizontalGridLines />
        <XAxis />
        <YAxis />
        <MarkSeries
          data={dataSample}
          onValueMouseOver={rememberValue}
          onValueMouseOut={forgetValue}
        />
        {value ? <Hint value={value} /> : null}
      </XYPlot> */}
    </Wrapper>
  );
}

export default SimpleBarChart;
