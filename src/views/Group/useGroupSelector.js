import { useSelector } from "react-redux";

export default function useGroupSelector() {
  const update = useSelector(state => state.GroupReducer.action === "UPDATE");
  const groupListInfo = useSelector(state => state.CommonReducer.group);
  const resourceList = useSelector(state => state.CommonReducer.resourceList);

  return { update, groupListInfo, resourceList };
}
