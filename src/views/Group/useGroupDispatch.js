import { useDispatch } from "react-redux";
import { useCallback } from "react";

import * as CommonAction from "../../state/moduels/actions/CommonAction";
import * as GroupAction from "../../state/moduels/actions/GroupAction";

import { getGroupList } from "state/moduels/actions/GroupAction";
import { generateQuery } from "library/GenerateQuery";
import GroupConstants from "state/constants/GroupConstants";

export default function useGroupDispatch({ history }) {
  // let detailPage = "group/groupdetail";
  // let listPage = "group/grouplist";
  // let pageSize = 10;
  // let pageCategory = "group";

  const { detailPage, listPage, pageSize, pageCategory } = GroupConstants;

  const dispatch = useDispatch();

  const groupDispatch = useCallback(
    // TODO: getGroupList 를 getUserList 등으로 작업.
    () => dispatch(getGroupList({ number: 0, size: 10, sort: "regDt.desc" })),
    [dispatch]
  );

  const getCommonList = useCallback(
    pageQuery => {
      const pageNumber = pageQuery && pageQuery.number ? pageQuery.number : 0;
      dispatch(
        CommonAction.getCommonList(
          pageCategory,
          pageNumber,
          pageSize,
          pageQuery
        )
      );
    },
    [dispatch]
  );

  const initAction = useCallback(() => {
    dispatch(CommonAction.initAction());
  }, [dispatch]);

  const goDetail = useCallback(
    query => {
      let newQuery = {
        pagename: detailPage,
        ...query
      };

      history.push(generateQuery(newQuery));
    },
    [dispatch]
  );

  const getListForPaging = useCallback(
    pageQuery => {
      let newQuery = {};
      if (pageQuery) {
        newQuery = {
          pagename: listPage,
          ...pageQuery
        };
      } else {
        newQuery = {
          pagename: listPage
        };
      }
      console.log(history);
      history.push(generateQuery(newQuery));
    },
    [dispatch, history]
  );

  const getResourceList = useCallback(() => {
    dispatch(CommonAction.getResourceList());
  }, [dispatch]);

  const createGroup = useCallback(
    postData => {
      dispatch(GroupAction.createGroup(postData));
    },
    [dispatch]
  );

  const deleteGroup = useCallback(
    groupId => {
      dispatch(GroupAction.deleteGroup(groupId));
    },
    [dispatch]
  );

  return {
    groupDispatch,
    getCommonList,
    initAction,
    goDetail,
    getListForPaging,
    getResourceList,
    createGroup,
    deleteGroup
  };
}
