import React, { useEffect, useCallback, useState } from "react";

// libraries
import { Pagination } from "react-bootstrap";

// connect state
import { parseQueryString } from "library/queryrouter/parseQueryString";
import useGroupSelector from "./useGroupSelector";
import useGroupDispatch from "./useGroupDispatch";
import { withRouter } from "react-router-dom";

// custom style
import styled from "styled-components";
import { GroupTh } from "../../state/constants/TableHead";
import GroupDrawer from "components/customUI/SideMenu/GroupDrawer.js";
import DeleteAlert from "./DeleteAlert";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Card,
  CardContent,
  Typography,
} from "@material-ui/core";
import FormatLineSpacingIcon from "@material-ui/icons/FormatLineSpacing";

const styles = {};
const useStyles = makeStyles(styles);

const style = {
  Card: styled(Card)`
    box-shadow: unset;
    border: 1px solid ${props => props.theme.GrayColor};
  `,
  CardContent: styled(CardContent)`
    &.CardContentHeader {
      display: flex;
      justify-content: space-between;
      align-items: center;
      border-bottom: 1px solid ${props => props.theme.GrayColor};
    }
  `,

  TableCell: styled(TableCell)`
    padding: 0.8rem;
    &.TableHeadCell {
      background-color: ${props => props.theme.GrayLightColor};
    }
  `,
};

function GroupContainer({ location, history }) {
  const classes = useStyles();

  // select & dispatch
  const { groupListInfo, update, resourceList } = useGroupSelector();
  const {
    groupDispatch,
    getCommonList,
    initAction,
    goDetail,
    getListForPaging,
    getResourceList,
    createGroup,
    deleteGroup,
  } = useGroupDispatch({ history });
  console.log(groupListInfo);
  //
  const { totalElements, content, totalPages } = groupListInfo;
  const locationQuery = parseQueryString(location.search);
  const pageNo = parseInt(locationQuery.number, 10) + 1;
  let activePage = pageNo ? pageNo : 1;

  // detail page
  let actPreQuery = {
    ...locationQuery,
  };
  const goGroupDetail = (groupId, actPreQuery) => {
    goDetail({
      groupId,
      actPreSort: actPreQuery.sort,
      actPrePage: actPreQuery.number,
    });
  };

  // pagination
  const [pageState, setPageState] = useState({ page: 0 });
  const handlePageSelect = page => {
    setPageState({ page });

    let query = {
      ...locationQuery,
      number: page - 1,
    };

    getListForPaging(query);
  };

  // loadList
  const loadList = useCallback(() => {
    const query = {
      ...locationQuery,
    };
    if (query.searchType) {
      query[query.searchType] = query.searchText;
    }
    if (!query.sort) {
      query.sort = "userLv.asc";
    }
    initAction();
    getCommonList(query);
  }, [locationQuery.number]);

  const [toggleTableSize, setToggleTableSize] = useState(true);
  const [tableSize, setTableSize] = useState("medium");

  // useEffect with deps(lcationQuery.number for pagination, update for create and delete)
  useEffect(() => {
    loadList();
    getResourceList();
  }, [locationQuery.number, update]);

  return (
    <>
      <style.Card>
        <style.CardContent className="CardContentHeader">
          <Typography component="h5" variant="h5">
            그룹
          </Typography>
          <Typography
            variant="subtitle1"
            color="textSecondary"
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <FormatLineSpacingIcon
              onClick={() => setToggleTableSize(!toggleTableSize)}
              color="primary"
              style={{ cursor: "pointer", marginRight: "1rem" }}
            />
            <GroupDrawer
              buttonProps="그룹등록"
              buttoncolorprops="white"
              resourceList={resourceList}
              createGroup={createGroup}
            />
          </Typography>
        </style.CardContent>

        <TableContainer
          component={Paper}
          style={{ padding: "1rem", boxShadow: "unset" }}
        >
          <Table
            className={classes.table}
            size={toggleTableSize === true ? `medium` : "small"}
            aria-label="simple table"
          >
            <TableHead>
              <TableRow>
                {GroupTh &&
                  GroupTh.map((title, index) => (
                    <style.TableCell className="TableHeadCell" key={index}>
                      {title}
                    </style.TableCell>
                  ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {content &&
                content.map(group => (
                  <TableRow key={group.id}>
                    <style.TableCell>
                      {group.groupNm + `(${group.count ? group.count : `0`})`}
                    </style.TableCell>
                    <style.TableCell>{group.asgndCpuNum}</style.TableCell>
                    <style.TableCell>{group.asgndGpuNum}</style.TableCell>
                    <style.TableCell>{group.asgndDisSz + "GB"}</style.TableCell>
                    <style.TableCell>{group.asgndMemSz + "GB"}</style.TableCell>
                    <style.TableCell>
                      <DeleteAlert
                        deleteAction={deleteGroup}
                        selectedId={group.groupId}
                      />
                    </style.TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
        {groupListInfo && totalElements !== 0 && (
          <Pagination
            prev
            next
            items={totalPages}
            boundaryLinks={true}
            ellipsis={false}
            maxButtons={5}
            activePage={activePage}
            onSelect={handlePageSelect}
            style={{
              display: "flex",
              justifyContent: "center",
            }}
          />
        )}
      </style.Card>
    </>
  );
}

// const mapStateToProps = (state, ownProps) => {
//   return {
//     update: state.GroupReducer.action === "UPDATE",
//     groupListInfo: state.CommonReducer.group,
//     resourceList: state.CommonReducer.resourceList
//   };
// };

// const mapDispatchToProps = (dispatch, ownProps) => {
//   const {
//     detailPage = "group/groupdetail",
//     listPage = "group/grouplist",
//     pageSize = 10,
//     pageCategory = "group",
//     history
//   } = ownProps;

//   return {
//     initAction: () => {
//       dispatch(CommonAction.initAction());
//     },

//     goDetail: (query = {}) => {
//       let newQuery = {
//         pagename: detailPage,
//         ...query
//       };
//       history.push(generateQuery(newQuery));
//     },

//     getListForPaging: pageQuery => {
//       let newQuery = {};
//       if (pageQuery) {
//         newQuery = {
//           pagename: listPage,
//           ...pageQuery
//         };
//       } else {
//         newQuery = {
//           pagename: listPage
//         };
//       }
//       history.push(generateQuery(newQuery));
//     },

//     getCommonList: pageQuery => {
//       const pageNumber = pageQuery && pageQuery.number ? pageQuery.number : 0;
//       dispatch(
//         CommonAction.getCommonList(
//           pageCategory,
//           pageNumber,
//           pageSize,
//           pageQuery
//         )
//       );
//     },

//     getResourceList: () => {
//       dispatch(CommonAction.getResourceList());
//     },

//     createGroup: postData => {
//       dispatch(GroupAction.createGroup(postData));
//     },

//     deleteGroup: groupId => {
//       dispatch(GroupAction.deleteGroup(groupId));
//     }
//   };
// };

export default withRouter(GroupContainer);
