import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Pagination from "material-ui-flat-pagination";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";

import styled from "styled-components";
import GroupDrawer from "components/customUI/SideMenu/GroupDrawer.js";
import { getGroupList } from "../../state/moduels/actions/GroupAction";
import { GroupTh } from "../../state/constants/TableHead";

const styles = {
  cardCategoryWhite: {
    color: "#fff",
    fontSize: "14px",
    marginLeft: "3px"
  },

  cardCategoryPrimary: {
    color: "inherit",
    fontSize: "14px",
    marginLeft: "3px"
  },

  cardTitleWhite: {
    display: "flex",
    alignItmes: "center",
    color: "#fff",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "700",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "0",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const StyledCardHeader = styled(CardHeader)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 2rem;
  font-weight: 400;
  margin-top: 1rem;
`;

const StyledCardHeaderTitle = styled.h4`
  font-weight: 500;
  font-size: 1.7rem;
  display: flex;
  align-items: center;
`;

const StyledCard = styled(Card)`
  margin-top: 0.5rem;
`;

const useStyles = makeStyles(styles);

export default function GroupContainer() {
  const classes = useStyles();
  const groupList = useSelector(state => state.GroupReducer.groupList);
  const { totalElements, content } = groupList;

  const dispatch = useDispatch();
  const groupDispatch = useCallback(
    () => dispatch(getGroupList({ number: 0, size: 10, sort: "regDt.desc" })),
    [dispatch]
  );

  useEffect(() => {
    groupDispatch();
  }, [groupDispatch, dispatch]);

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <StyledCardHeader>
          <StyledCardHeaderTitle>
            <RadioButtonUncheckedIcon
              style={{ color: "#9c27b0", marginRight: "0.5rem" }}
            />
            그룹
            <span className={classes.cardCategoryPrimary}>
              {!!totalElements ? `(${totalElements})` : `(0)`}
            </span>
          </StyledCardHeaderTitle>
          <GroupDrawer buttonProps="그룹등록" buttoncolorprops="white" />
        </StyledCardHeader>
        <StyledCard>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={GroupTh}
              tableData={
                !!content
                  ? content.map(group => {
                      return [
                        group.groupNm +
                          ` (${!!group.count ? group.count : `0`})`,
                        group.asgndCpuNum,
                        group.asgndGpuNum,
                        group.asgndDisSz + "GB",
                        group.asgndMemSz + "GB",
                        "-"
                      ];
                    })
                  : [["-", "-", "-", "-", "-", "-"]]
              }
            />
          </CardBody>
          <Pagination
            limit={10}
            offset={0}
            total={100}
            currentPageColor="primary"
            otherPageColor="default"
            style={{ textAlign: "center", marginBottom: "10px" }}
          />
        </StyledCard>
      </GridItem>
    </GridContainer>
  );
}
