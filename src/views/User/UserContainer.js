import React, { useCallback, useEffect } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import Pagination from "material-ui-flat-pagination";

import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { getGroupList } from "state/moduels/actions/GroupAction";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";

// import GroupDrawer from "components/customUI/SideMenu/GroupDrawer.js";

const styles = {
  cardCategoryWhite: {
    color: "#fff",
    fontSize: "14px",
    marginLeft: "3px"
  },

  cardCategoryPrimary: {
    color: "inherit",
    fontSize: "14px",
    marginLeft: "3px"
  },

  cardTitleWhite: {
    display: "flex",
    alignItmes: "center",
    color: "#fff",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "700",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "0",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const StyledCardHeader = styled(CardHeader)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 2rem;
  font-weight: 400;
  margin-top: 1rem;
`;

const StyledCardHeaderTitle = styled.h4`
  font-weight: 500;
  font-size: 1.7rem;
  display: flex;
  align-items: center;
`;

const StyledCard = styled(Card)`
  margin-top: 0.5rem;
`;

const useStyles = makeStyles(styles);

export default function UserContainer() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const groupDispatch = useCallback(
    // TODO: getGroupList 를 getUserList 등으로 작업.
    () => dispatch(getGroupList({ number: 0, size: 10, sort: "regDt.desc" })),
    [dispatch]
  );

  useEffect(() => {
    groupDispatch();
  }, [groupDispatch, dispatch]);

  const groupList = useSelector(state => state.GroupReducer.groupList);
  const { totalElements, content } = groupList;
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <StyledCardHeader>
          <StyledCardHeaderTitle>
            <RadioButtonUncheckedIcon
              style={{ color: "#9c27b0", marginRight: "0.5rem" }}
            />
            사용자
            <span className={classes.cardCategoryPrimary}>
              {!!totalElements ? `(${totalElements})` : `(0)`}
            </span>
          </StyledCardHeaderTitle>
        </StyledCardHeader>
        <StyledCard>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["이름", "CPU", "GPU", "디스크", "메모리", "설정"]}
              tableData={[
                ["Admin(5)", "2", "8", "100GB", "8GB", ""],
                ["User(5)", "1", "3", "20GB", "1GB", ""]
              ]}
            />
          </CardBody>
          <Pagination
            limit={10}
            offset={0}
            total={100}
            currentPageColor={"primary"}
            otherPageColor={"default"}
            style={{ textAlign: "center", marginBottom: "10px" }}
          />
        </StyledCard>
      </GridItem>
      {/* <GridItem xs={12} sm={12} md={12}>
        <Card plain>
          <CardHeader plain color="primary">
            <h4 className={classes.cardTitleWhite}>
              Table on Plain Background
            </h4>
            <p className={classes.cardCategoryWhite}>
              Here is a subtitle for this table
            </p>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["ID", "Name", "Country", "City", "Salary"]}
              tableData={[
                ["1", "Dakota Rice", "$36,738", "Niger", "Oud-Turnhout"],
                ["2", "Minerva Hooper", "$23,789", "Curaçao", "Sinaai-Waas"],
                ["3", "Sage Rodriguez", "$56,142", "Netherlands", "Baileux"],
                [
                  "4",
                  "Philip Chaney",
                  "$38,735",
                  "Korea, South",
                  "Overland Park"
                ],
                [
                  "5",
                  "Doris Greene",
                  "$63,542",
                  "Malawi",
                  "Feldkirchen in Kärnten"
                ],
                ["6", "Mason Porter", "$78,615", "Chile", "Gloucester"]
              ]}
            />
          </CardBody>
        </Card>
      </GridItem> */}
    </GridContainer>
  );
}
