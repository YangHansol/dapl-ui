import { useDispatch } from "react-redux";
import { useCallback } from "react";

import * as CommonAction from "../moduels/actions/CommonAction";
import * as DatasetAction from "../moduels/actions/DatasetAction";
import * as LoadingAction from "library/loading/LoadingAction";
import * as ErrorAction from "library/error/ErrorAction";

import { getDatasetList } from "state/moduels/actions/DatasetAction";
import { generateQuery } from "library/GenerateQuery";
import DatasetConstants from "../constants/DatasetConstants";

export default function useDatasetDispatch({ history }) {
  const { pageSize, listPage, detailPage, pageCategory } = DatasetConstants;

  const dispatch = useDispatch();

  const initAction = useCallback(() => {
    dispatch(DatasetAction.initAction());
  }, [dispatch]);

  const getDatasetList = useCallback(
    reqQuery => {
      const pageNumber =
        reqQuery && reqQuery.query && reqQuery.query.number
          ? reqQuery.query.number
          : 0;
      const query = {
        ...reqQuery,
        number: pageNumber,
      };
      dispatch(DatasetAction.getDatasetList(query));
    },
    [dispatch],
  );

  const goDetail = useCallback(
    query => {
      let newQuery = {
        pagename: detailPage,
        ...query,
      };

      history.push(generateQuery(newQuery));
    },
    [dispatch],
  );

  const getListForPaging = useCallback(
    pageQuery => {
      let newQuery = {};
      if (pageQuery) {
        newQuery = {
          pagename: listPage,
          ...pageQuery,
        };
      } else {
        newQuery = {
          pagename: listPage,
        };
      }
      history.push(generateQuery(newQuery));
    },
    [dispatch, history],
  );

  const datasetUpdate = useCallback(
    data => {
      dispatch(DatasetAction.datasetUpdate(data));
    },
    [dispatch],
  );

  const deleteDataset = useCallback(
    id => {
      dispatch(DatasetAction.deleteDataset(id));
    },
    [dispatch],
  );

  const fileUpload = useCallback(
    checkData => {
      dispatch(DatasetAction.fileUpload(checkData));
    },
    [dispatch],
  );

  const startCall = useCallback(() => {
    dispatch(LoadingAction.startCall);
  }, [dispatch]);

  const completeCall = useCallback(() => {
    dispatch(LoadingAction.completeCall);
  }, [dispatch]);

  const error = useCallback(
    e => {
      dispatch(ErrorAction.Error(e));
    },
    [dispatch],
  );

  const initSql = useCallback(() => {
    dispatch(DatasetAction.initSql());
  }, [dispatch]);

  const getDatabaseList = useCallback(
    reqQuery => {
      const pageNumber =
        reqQuery && reqQuery.query && reqQuery.query.number
          ? reqQuery.query.number
          : 0;
      const query = {
        ...reqQuery,
        number: pageNumber,
      };
      dispatch(DatasetAction.getDatabaseList(query));
    },
    [dispatch],
  );

  const getDatabaseListTest = useCallback(
    param => {
      dispatch(DatasetAction.getDatabaseListTest(param));
    },
    [dispatch],
  );

  const getDatabaseTable = useCallback(
    param => {
      dispatch(DatasetAction.getDatabaseTable(param));
    },
    [dispatch],
  );

  const getDatabaseTableColumn = useCallback(
    (dbId, tableNm) => {
      dispatch(DatasetAction.getDatabaseTableColumn(dbId, tableNm));
    },
    [dispatch],
  );

  const getDatabaseDatas = useCallback(
    (dbId, extractTp, tableNm, conditions, query) => {
      dispatch(
        DatasetAction.getDatabaseDatas(
          dbId,
          extractTp,
          tableNm,
          conditions,
          query,
        ),
      );
    },
    [dispatch],
  );

  const registDatasetSQL = useCallback(
    param => {
      dispatch(DatasetAction.registDatasetSQL(param));
    },
    [dispatch],
  );

  const getDatasetSQL = useCallback(
    (queryStr, queryDbTp) => {
      dispatch(DatasetAction.getDatasetSQL(queryStr, queryDbTp));
    },
    [dispatch],
  );

  const datasetUpload = useCallback(
    param => {
      dispatch(DatasetAction.datasetUpload(param));
    },
    [dispatch],
  );

  const datasetCreate = useCallback(
    param => {
      dispatch(DatasetAction.datasetCreate(param));
    },
    [dispatch],
  );

  const getDatasetRead = useCallback(
    dataId => {
      dispatch(DatasetAction.getDatasetRead(dataId));
    },
    [dispatch],
  );

  const datasetCreateChart = useCallback(
    param => {
      dispatch(DatasetAction.datasetCreateChart(param));
    },
    [dispatch],
  );

  const datasetDeleteChart = useCallback(
    param => {
      dispatch(DatasetAction.datasetDeleteChart(param));
    },
    [dispatch],
  );

  return {
    initAction,
    getDatasetList,
    goDetail,
    getListForPaging,
    datasetUpdate,
    deleteDataset,
    fileUpload,
    startCall,
    completeCall,
    error,
    initSql,
    getDatabaseList,
    getDatabaseListTest,
    getDatabaseTable,
    getDatabaseTableColumn,
    getDatabaseDatas,
    registDatasetSQL,
    getDatasetSQL,
    datasetUpload,
    datasetCreate,
    getDatasetRead,
    datasetCreateChart,
    datasetDeleteChart,
  };
}
