import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAlgorithmList } from "state/moduels/actions/AlgorithmAction";
import * as AlgorithmAction from "../../state/moduels/actions/AlgorithmAction";
import * as CommonAction from "../../state/moduels/actions/CommonAction";
import { generateQuery } from "library/GenerateQuery";
import AlgorithmConstants from "state/constants/AlgorithmConstants";

function useAlgorithmList() {
  const { listPage, pageSize, pageCategory } = AlgorithmConstants();
  // selector
  const update = useSelector(
    state => state.AlgorithmReducer.action === "UPDATE"
  );
  const algorithmListInfo = useSelector(state => state.CommonReducer.algorithm);

  // dispatch
  const dispatch = useDispatch();

  const initAction = useCallback(() => {
    dispatch(CommonAction.initAction());
  }, [dispatch]);

  const deleteAlgo = useCallback(
    athmId => {
      dispatch(AlgorithmAction.deleteAlgorithm(athmId));
    },
    [dispatch]
  );

  const getListForPaging = useCallback(
    pageQuery => {
      let newQuery = {};
      if (pageQuery) {
        newQuery = {
          pagename: listPage,
          ...pageQuery
        };
      } else {
        newQuery = {
          pagename: listPage
        };
      }
      history.push(generateQuery(newQuery));
    },
    [listPage]
  );

  const getCommonList = useCallback(
    pageQuery => {
      const pageNumber = pageQuery && pageQuery.number ? pageQuery.number : 0;
      dispatch(
        CommonAction.getCommonList(
          pageCategory,
          pageNumber,
          pageSize,
          pageQuery
        )
      );
    },
    [dispatch, pageCategory, pageSize]
  );

  const updateAlgo = useCallback(
    postData => {
      dispatch(AlgorithmAction.updateAlgorithm(postData));
    },
    [dispatch]
  );

  const algorithmDispatch = useCallback(
    () =>
      dispatch(getAlgorithmList({ number: 0, size: 10, sort: "regDt.desc" })),
    [dispatch]
  );

  return {
    algorithmDispatch,
    update,
    algorithmListInfo,
    initAction,
    deleteAlgo,
    getListForPaging,
    getCommonList,
    updateAlgo
  };
}

export default useAlgorithmList;
