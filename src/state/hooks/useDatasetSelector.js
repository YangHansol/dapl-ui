import { useSelector } from "react-redux";

export default function useDatasetSelector() {
  const update = useSelector(state => state.DatasetReducer.action === "UPDATE");
  const datasetListInfo = useSelector(state => state.CommonReducer.dataset);
  const datasetList = useSelector(state => state.DatasetReducer.datasetList);
  const databaseCommonList = useSelector(state => state.CommonReducer.database);
  const databaseList = useSelector(state => state.DatasetReducer.databaseList);
  const databaseListTest = useSelector(
    state => state.DatasetReducer.databaseListTest,
  );
  const databaseTable = useSelector(
    state => state.DatasetReducer.databaseTable,
  );
  const databaseTableColumn = useSelector(
    state => state.DatasetReducer.databaseTableColumn,
  );
  const databaseDatas = useSelector(
    state => state.DatasetReducer.databaseDatas,
  );
  const sqlList = useSelector(state => state.DatasetReducer.sqlList);
  const datasetUpload = useSelector(
    state => state.DatasetReducer.datasetUpload,
  );
  const datasetCreate = useSelector(
    state => state.DatasetReducer.datasetCreate,
  );
  const datasetRead = useSelector(state => state.DatasetReducer.datasetRead);
  // file upload
  const fileUploadResult = useSelector(
    state => state.DatasetReducer.fileUploadResult,
  );
  const datasetCreateChart = useSelector(
    state => state.DatasetReducer.datasetCreateChart,
  );

  const datasetDeleteChart = useSelector(
    state => state.DatasetReducer.datasetDeleteChart,
  );

  return {
    update,
    datasetListInfo,
    datasetList,
    fileUploadResult,
    databaseList,
    databaseCommonList,
    databaseListTest,
    databaseTable,
    databaseTableColumn,
    databaseDatas,
    sqlList,
    datasetUpload,
    datasetCreate,
    datasetRead,
    datasetCreateChart,
    datasetDeleteChart,
  };
}
