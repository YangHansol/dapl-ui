import { ActionTypes as types } from "../actions/DatasetAction";

const initialState = {
  action: "",
  datasetList: {},
  localDataset: {},
  databaseList: {},
  databaseCommonList: {},
  databaseListTest: {},
  databaseTable: {},
  databaseTableColumn: {},
  databaseDatas: {},
  datasetDetail: {
    data: [],
  },
  localDatasetDetail: {},
  localFileListInfo: {},
  resultDw: null,
  datasetUpload: {},
  datasetCreate: {},
  datasetRead: {},
  datasetCreateChart: {},
  datasetDeleteChart: {},
};

export default function DatasetReducer(state = initialState, action) {
  switch (action.type) {
    case types.UPDATE_DATASET:
    case types.DELETE_DATASET:
    case types.FILE_UPLOAD:
    case types.REGIST_DATASET_SQL:
      return Object.assign({}, state, {
        action: "UPDATE",
      });

    case types.INIT_ACTION:
      return Object.assign({}, state, {
        action: initialState.action,
      });

    case types.INIT_MEMBER_INFO:
      return Object.assign({}, state, {
        datasetList: initialState.databaseList,
      });

    case types.GET_DATABASE_LIST:
      return Object.assign({}, state, {
        databaseList: action.payload.databaseList,
      });

    case types.GET_DATABASE_COMMON_LIST:
      return Object.assign({}, state, {
        databaseCommonList: action.payload.databaseCommonList,
      });

    case types.GET_DATABASE_LIST_TEST:
      return Object.assign({}, state, {
        databaseListTest: action.payload.databaseListTest,
      });

    case types.GET_DATABASE_TABLE:
      return Object.assign({}, state, {
        databaseTable: action.payload.databaseTable,
      });

    case types.GET_DATABASE_TABLE_COLUMN:
      return Object.assign({}, state, {
        databaseTableColumn: action.payload.databaseTableColumn,
      });

    case types.GET_DATABASE_DATAS:
      return Object.assign({}, state, {
        databaseDatas: action.payload.databaseDatas,
      });

    case types.INIT_DATASET:
      return Object.assign({}, state, {
        datasetDetail: initialState.datasetDetail,
        datasetReadInfo: initialState.datasetReadInfo,
      });

    case types.GET_DATASET_DETAIL:
      return Object.assign({}, state, {
        datasetDetail: action.payload.datasetDetail,
      });

    case types.TRANSFER_DW:
      return Object.assign({}, state, {
        resultDw: action.payload.resultDw,
      });

    case types.INIT_TRANSFER_DW:
      return Object.assign({}, state, {
        action: initialState.resultDw,
      });

    case types.GET_DATASET_LIST:
      return Object.assign({}, state, {
        datasetList: action.payload.datasetList,
      });

    case types.DATASET_UPLOAD:
      return Object.assign({}, state, {
        datasetUpload: action.payload.datasetUpload,
      });

    case types.DATASET_CREATE:
      return Object.assign({}, state, {
        datasetCreate: action.payload.datasetCreate,
      });

    case types.GET_DATASET_READ:
      return Object.assign({}, state, {
        datasetRead: action.payload.datasetRead,
      });

    case types.DATASET_CREATE_CHART:
      return Object.assign({}, state, {
        datasetCreateChart: action.payload.datasetCreateChart,
      });

    case types.DATASET_DELETE_CHART:
      return Object.assign({}, state, {
        datasetDeleteChart: action.payload.datasetDeleteChart,
      });

    default:
      return state;
  }
}
