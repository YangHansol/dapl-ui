import { combineReducers } from "redux";
import AlgorithmReducer from "./AlgorithmReducers";
import GroupReducer from "./GroupReducers";
import CommonReducer from "./CommonReducer";
import DatasetReducer from "./DatasetReducer";

export default combineReducers({
  AlgorithmReducer,
  GroupReducer,
  CommonReducer,
  DatasetReducer
});
