import { ActionTypes as types } from "../actions/GroupAction";

const initialState = {
  action: "",
  groupList: {},
  groupDetail: {
    data: []
  }
};

export default function GroupReducer(state = initialState, action) {
  switch (action.type) {
    case types.INIT_ACTION:
      return {
        ...state,
        action: initialState.action
      };

    case types.CREATE_GROUP:
    case types.UPDATE_GROUP:
    case types.DELETE_GROUP:
      return {
        ...state,
        action: "UPDATE"
      };
    case types.GET_GROUP_LIST:
      return {
        groupList: action.payload.groupList
      };

    default:
      return state;
  }
}
