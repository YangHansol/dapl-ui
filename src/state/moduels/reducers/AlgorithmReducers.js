import { ActionTypes as types } from "../actions/AlgorithmAction";

const initialState = {
  action: "",

  algorithmList: {}
};

export default function AlgorithmReducer(state = initialState, action) {
  switch (action.type) {
    case types.INIT_ACTION:
      return Object.assign({}, state, {
        action: initialState.action
      });

    case types.UPDATE_ALGORITHM_LIST:
    case types.ALGORITHM_DELETE:
      return Object.assign({}, state, {
        action: "UPDATE"
      });

    case types.GET_ALGORITHM_LIST:
      return Object.assign({}, state, {
        algorithmList: action.payload.algorithmList
      });

    default:
      return state;
  }
}
