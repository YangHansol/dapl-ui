import DatasetService from "state/services/DatasetService";
import CommonService from "library/service/CommonService";

import * as LoadingAction from "library/loading/LoadingAction";
import * as ErrorAction from "library/error/ErrorAction";

// action type
const CATEGORY = "dataset";

export const ActionTypes = Object.freeze({
  INIT_ACTION: "INIT_ACTION",
  INIT_MEMBER_INFO: "INIT_MEMBER_INFO",
  INIT_DATASET: `${CATEGORY}/INIT_DATASET`,
  INIT_SQL: `${CATEGORY}/INIT_SQL`,

  GET_DATASET_LIST: `${CATEGORY}/GET_DATASET_LIST`,
  GET_DATASET_DETAIL: `${CATEGORY}/GET_DATASET_DETAIL`,
  GET_DATASET_READ: `${CATEGORY}/GET_DATASET_READ`,
  DELETE_DATASET: `${CATEGORY}/DELETE_DATASET`,
  REGIST_DATASET_SQL: `${CATEGORY}/REGIST_DATASET_SQL`,

  GET_LOCAL_DATASET_LIST: `${CATEGORY}/GET_LOCAL_DATASET_LIST`,
  CREATE_LOCAL_DATASET: `${CATEGORY}/CREATE_LOCAL_DATASET`,

  GET_DATABASE_LIST: `${CATEGORY}/GET_DATABASE_LIST`,
  GET_DATABASE_COMMON_LIST: `${CATEGORY}/GET_DATABASE_COMMON_LIST`,
  GET_DATABASE_LIST_TEST: `${CATEGORY}/GET_DATABASE_LIST_TEST`,
  GET_DATABASE_TABLE: `${CATEGORY}/GET_DATABASE_TABLE`,
  GET_DATABASE_TABLE_COLUMN: `${CATEGORY}/GET_DATABASE_TABLE_COLUMN`,
  GET_DATABASE_DATAS: `${CATEGORY}/GET_DATABASE_DATAS`,
  UPDATE_DATASET: `${CATEGORY}/UPDATE_DATASET`,
  GET_DATASET_SQL: `${CATEGORY}/GET_DATESET_SQL`,
  REGIST_DATASET_SQL: `${CATEGORY}/REGIST_DATASET_SQL`,
  DATASET_UPLOAD: `${CATEGORY}/DATASET_UPLOAD`,
  DATASET_CREATE: `${CATEGORY}/DATASET_CREATE`,

  INIT_TRANSFER_DW: `${CATEGORY}/TRANSFER_DW`,
  TRANSFER_DW: `${CATEGORY}/TRANSFER_DW`,

  FILE_UPLOAD: `${CATEGORY}/FILE_UPLOAD`,
  DATASET_CREATE_CHART: `${CATEGORY}/DATASET_CREATE_CHART`,
  DATASET_DELETE_CHART: `${CATEGORY}/DATASET_DELETE_CHART`,
});

// action creator
function generateAPIQuery(params) {
  let query = { ...params };
  for (let key of Object.keys(query)) {
    if (Array.isArray(query[key] === true)) {
      query[key] = query[key].toString();
    } else if (query[key] === "") {
      delete query[key];
    }
  }
  return query;
}

export function initAction() {
  return {
    type: ActionTypes.INIT_ACTION,
  };
}

export function initDataset() {
  return {
    type: ActionTypes.INIT_DATASET,
  };
}

export function initSql() {
  return { type: ActionTypes.INIT_SQL };
}

export function initTransferDw() {
  return {
    type: ActionTypes.INIT_TRANSFER_DW,
  };
}

export function getDatasetList(reqQuery, query) {
  return (dispatch, getState) => {
    let _query = {};
    if (query) {
      _query = { ...query };
    }
    return DatasetService.getDatasetList(reqQuery, generateAPIQuery(_query))
      .then(results => {
        dispatch({
          type: ActionTypes.GET_DATASET_LIST,
          payload: {
            datasetList: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function fileUpload(params) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return DatasetService.fileUpload(params)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        alert("정상적으로 등록되었습니다.");
        dispatch({
          type: ActionTypes.FILE_UPLOAD,
        });
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function getLocalDatasetList(page, size, wkspcId) {
  return (dispatch, getState) => {
    return DatasetService.getLocalDatasetList(page, size, wkspcId)
      .then(results => {
        dispatch({
          type: ActionTypes.GET_LOCAL_DATASET_LIST,
          payload: {
            localDataset: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function getDatasetDetail(dataId) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return DatasetService.getDatasetView(dataId)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        dispatch({
          type: ActionTypes.GET_DATASET_DETAIL,
          payload: {
            datasetDetail: results.data,
          },
        });
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}
export function getDatasetRead(dataId) {
  return (dispatch, getState) => {
    return DatasetService.getDatasetRead(dataId)
      .then(results => {
        dispatch({
          type: ActionTypes.GET_DATASET_READ,
          payload: {
            datasetRead: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}
export function registDatasetSQL(params) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return DatasetService.registDatasetSQL(params)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        dispatch({
          type: ActionTypes.REGIST_DATASET_SQL,
        });
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}
export function deleteDataset(id) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return DatasetService.deleteDataset(id)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        dispatch({
          type: ActionTypes.DELETE_DATASET,
        });
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}
export function transferDw(dataId) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return DatasetService.transferDw(dataId)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        dispatch({
          type: ActionTypes.TRANSFER_DW,
          payload: {
            resultDw: results.data,
          },
        });
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}
export function datasetUpdate(data) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return DatasetService.datasetUpdate(data)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        alert("수정되었습니다.");
        dispatch({
          type: ActionTypes.UPDATE_DATASET,
        });
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}
export function createLocalDataset(dataId) {
  return (dispatch, getState) => {
    return DatasetService.createLocalDataset(dataId)
      .then(results => {
        dispatch({
          type: ActionTypes.CREATE_LOCAL_DATASET,
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function getDatabaseList(reqQuery, query) {
  return (dispatch, getState) => {
    let _query = {};
    if (query) {
      _query = { ...query };
    }
    return DatasetService.getDatabaseList(reqQuery, generateAPIQuery(_query))
      .then(results => {
        dispatch({
          type: ActionTypes.GET_DATABASE_LIST,
          payload: {
            databaseList: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function getDatabaseCommonList(page, size, query) {
  return (dispatch, getState) => {
    let _query = {};
    if (query) {
      _query = { ...query };
    }
    return DatasetService.getDatabaseCommonList(
      page,
      size,
      generateAPIQuery(_query),
    )
      .then(results => {
        dispatch({
          type: ActionTypes.GET_DATABASE_COMMON_LIST,
          payload: {
            databaseCommonList: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function getDatabaseListTest(dbId) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());

    return DatasetService.getDatabaseListTest(dbId)
      .then(results => {
        dispatch(LoadingAction.completeCall());

        dispatch({
          type: ActionTypes.GET_DATABASE_LIST_TEST,
          payload: {
            databaseListTest: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function getDatasetSQL(queryStr, queryDbTp) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return DatasetService.getDatasetSQL(queryStr, queryDbTp)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        dispatch({
          type: ActionTypes.GET_DATASET_SQL,
          payload: {
            sqlList: results.data,
          },
        });
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        dispatch(ErrorAction.Error(e));
      });
  };
}

export function getDatabaseTable(dbId) {
  return (dispatch, getState) => {
    return DatasetService.getDatabaseTable(dbId)
      .then(results => {
        dispatch({
          type: ActionTypes.GET_DATABASE_TABLE,
          payload: {
            databaseTable: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function getDatabaseTableColumn(dbId, tableNm) {
  return (dispatch, getState) => {
    return DatasetService.getDatabaseTableColumn(dbId, tableNm)
      .then(results => {
        dispatch({
          type: ActionTypes.GET_DATABASE_TABLE_COLUMN,
          payload: {
            databaseTableColumn: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function getDatabaseDatas(dbId, extractTp, tableNm, conditions, query) {
  return (dispatch, getState) => {
    return DatasetService.getDatabaseDatas(
      dbId,
      extractTp,
      tableNm,
      conditions,
      query,
    )
      .then(results => {
        dispatch({
          type: ActionTypes.GET_DATABASE_DATAS,
          payload: {
            databaseDatas: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function datasetUpload(dbData) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());

    return DatasetService.datasetUpload(dbData)
      .then(results => {
        dispatch(LoadingAction.completeCall());

        dispatch({
          type: ActionTypes.DATASET_UPLOAD,
          payload: {
            datasetUpload: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function datasetCreate(data) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());

    return DatasetService.datasetCreate(data)
      .then(results => {
        dispatch(LoadingAction.completeCall());

        dispatch({
          type: ActionTypes.DATASET_CREATE,
          payload: {
            datasetCreate: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function datasetCreateChart(data) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());

    return DatasetService.datasetCreateChart(data)
      .then(results => {
        dispatch(LoadingAction.completeCall());

        dispatch({
          type: ActionTypes.DATASET_CREATE_CHART,
          payload: {
            datasetCreateChart: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function datasetDeleteChart(data) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());

    return DatasetService.datasetDeleteChart(data)
      .then(results => {
        dispatch(LoadingAction.completeCall());

        dispatch({
          type: ActionTypes.DATASET_DELETE_CHART,
          payload: {
            datasetDeleteChart: results.data,
          },
        });
      })
      .catch(e => {
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}
