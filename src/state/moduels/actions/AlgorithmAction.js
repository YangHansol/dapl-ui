import AlgorithmService from "state/services/AlgorithmServices";
import * as LoadingAction from "library/loading/LoadingAction";
import * as ErrorAction from "library/error/ErrorAction";

const CATEGORY = "ALGORITHM";

// action types
export const ActionTypes = Object.freeze({
  INIT_ACTION: "INIT_ACTION",

  GET_ALGORITHM_LIST: `${CATEGORY}/GET_ALGORITHM_LIST`,
  UPDATE_ALGORITHM_LIST: `${CATEGORY}/UPDATE_ALGORITHM_LIST`,
  ALGORITHM_DELETE: `${CATEGORY}/ALGORITHM_DELETE`,
  BACKUP_ALGORITHM: `${CATEGORY}/BACKUP_ALGORITHM`
});

// action creator
export function deleteAlgorithm(athmId, wkspcId) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return AlgorithmService.deleteAlgorithm(athmId, wkspcId)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        dispatch({
          type: ActionTypes.ALGORITHM_DELETE
        });
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        dispatch(ErrorAction.Error(e));
      });
  };
}

export function getAlgorithmList(query) {
  return (dispatch, getState) => {
    return AlgorithmService.getAlgorithmList(query)
      .then(results => {
        dispatch({
          type: ActionTypes.GET_ALGORITHM_LIST,
          payload: {
            algorithmList: results.data
          }
        });
      })
      .catch(e => {
        console.log(e);
      });
  };
}

export function updateAlgorithm(postData) {
  return (dispatch, getState) => {
    return AlgorithmService.updateAlgorithm(postData)
      .then(res => {
        dispatch({
          type: ActionTypes.UPDATE_ALGORITHM_LIST
        });
      })
      .catch(e => {
        // dispatch(LoadingAction.completeCall());
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function backupAlgorithm(wkspcId) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return AlgorithmService.backupAlgorithm(wkspcId)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        dispatch({
          type: ActionTypes.BACKUP_ALGORITHM
        });
        // dispatch(WorkspaceAction.deleteWorkspace(wkspcId));
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        dispatch(ErrorAction.Error(e));
      });
  };
}
