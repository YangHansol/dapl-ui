import GroupService from "state/services/GroupServices";

import * as LoadingAction from "library/loading/LoadingAction";
import * as ErrorAction from "../../../library/error/ErrorAction";

const CATEGORY = "GROUP";

export const ActionTypes = Object.freeze({
  INIT_ACTION: "INIT_ACTION",
  INIT_GROUP: `${CATEGORY}/INIT_GROUP`,

  GET_GROUP_LIST: `${CATEGORY}/GET_GROUP_LIST`,
  CREATE_GROUP: `${CATEGORY}/CREATE_GROUP`,
  UPDATE_GROUP: `${CATEGORY}/UPDATE_GROUP`,
  DELETE_GROUP: `${CATEGORY}/DELETE_GROUP`,
});

function generateAPIQuery(params) {
  let query = { ...params };
  for (let key of Object.keys(query)) {
    if (Array.isArray(query[key] === true)) {
      query[key] = query[key].toString();
    } else if (query[key] === "") {
      delete query[key];
    }
  }
  return query;
}

export function initGroup() {
  return {
    type: ActionTypes.INIT_GROUP,
  };
}

export function getGroupList(query) {
  return (dispatch, getState) => {
    return GroupService.getGroupList(query)
      .then(results => {
        dispatch({
          type: ActionTypes.GET_GROUP_LIST,
          payload: {
            groupList: results.data,
          },
        });
      })
      .catch(e => {
        console.log(e);
      });
  };
}

export function createGroup(postData) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return GroupService.createGroup(postData)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        dispatch({
          type: ActionTypes.CREATE_GROUP,
        });
      })
      .catch(e => {
        // dispatch(LoadingAction.completeCall());
        // dispatch(ErrorAction.Error(e));
        console.log(e);
      });
  };
}

export function updateGroup(postData) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return GroupService.updateGroup(postData)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        alert("수정되었습니다.");
        dispatch({
          type: ActionTypes.UPDATE_GROUP,
        });
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        dispatch(ErrorAction.Error(e));
      });
  };
}

export function deleteGroup(groupId) {
  return (dispatch, getState) => {
    dispatch(LoadingAction.startCall());
    return GroupService.deleteGroup(groupId)
      .then(results => {
        dispatch(LoadingAction.completeCall());
        dispatch({
          type: ActionTypes.DELETE_GROUP,
        });
      })
      .catch(e => {
        dispatch(LoadingAction.completeCall());
        dispatch(ErrorAction.Error(e));
      });
  };
}
