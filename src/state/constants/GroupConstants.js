const GroupConstants = {
  pageSize: 10,

  listPage: "group/grouplist",
  detailPage: "group/groupdetail",
  pageCategory: "group",
  userDetailPage: "user/userdetail",
  userLogDetailPage: "user/userlogdetail"
};

export default GroupConstants;
