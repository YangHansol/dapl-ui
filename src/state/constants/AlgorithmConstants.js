const AlgorithmConstants = {
  pageSize: 10,

  listPage: "algorithm/algorithmlist",
  pageCategory: "algorithm"
};

export default AlgorithmConstants;
