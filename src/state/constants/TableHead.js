export const GroupTh = ["이름", "CPU", "GPU", "디스크", "메모리", "설정"];

export const AlgoTh = [
  "이름",
  "구분",
  "저장소",
  "설명",
  "공개여부",
  "생성자",
  "수정일자",
  "삭제"
];
