const DatasetConstants = {
  pageSize: 10,

  // 메인
  listPage: "dataset/datasetlist",
  detailPage: "dataset/datasetdetail",
  pageCategory: "dataset",
  SearchItem: [
    { value: "dataNm", title: "이름" },
    { value: "ownerNm", title: "소유자" },
    { value: "dataTp", title: "종류" }
  ],
  RadioSearchItem: {
    dataTp: [{ value: "FL", title: "File" }, { value: "QR", title: "SQL" }]
  }
};

export default DatasetConstants;
