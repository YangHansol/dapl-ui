import axios from "axios";
import { API_SERVER, DEVELOP_API_SERVER } from "../constants/ApiConstants";

function getAPIHost(server = "admin") {
  if (process.env.NODE_ENV === "development") {
    {
      return DEVELOP_API_SERVER[server];
    }
  } else {
    {
      return API_SERVER[server];
    }
  }
}

axios.defaults.xsrfHeaderName = "tkn";
axios.defaults.headers.common["tkn"] = sessionStorage.getItem("tkn");

export function getMakeURL(server = null, url) {
  if (url.indexOf("http") === 0) {
    return url;
  } else {
    if (url.substr(0, 1) === "/") {
      return getAPIHost(server).concat(url);
    } else {
      return getAPIHost(server).concat("/", url);
    }
  }
}

class RestAPICaller {
  static checkURL(url) {
    if (
      url.indexOf("/null/") !== -1 ||
      url.indexOf("/undefined/") !== -1 ||
      url.indexOf("/NaN/") !== -1
    ) {
      return false;
    }
    return true;
  }

  static get(server = null, url, params = null) {
    const fullUrl = getMakeURL(server, url);
    if (RestAPICaller.checkURL(url) === false) {
      throw Error("null parameter exception : " + fullUrl);
    }
    const fetchParams =
      params !== null ? { params: { ...params } } : { params: {} };

    return axios(fullUrl, fetchParams)
      .then(response => {
        return response;
      })
      .catch(e => {
        throw e;
      });
  }

  static post(server = null, url, params = null) {
    const fullUrl = getMakeURL(server, url);
    if (RestAPICaller.checkURL(url) === false) {
      throw Error("null parameter exception : " + fullUrl);
    }
    const fetchParams = params !== null ? params : {};

    return axios.post(fullUrl, fetchParams);
  }

  static fileUpload(url, params = null) {
    const fullUrl = getMakeURL(url);
    if (RestAPICaller.checkURL(url) === false) {
      throw Error("null parameter exception : " + fullUrl);
    }
    const fetchParams = params !== null ? params : {};
    const options = {
      headers: { "Content-type": "application/json" },
    };
    return axios.post(fullUrl, fetchParams, options);
  }
}

export default RestAPICaller;
