import RestAPICaller from "./RestAPICaller";
import moment from "moment";

const API_CONSTANTAS = {
  dataset: "/dataset",
  database: "/database",
  datasetServer: "datasetServer",
};
const { dataset, database, datasetServer } = API_CONSTANTAS;

export default class DatasetService {
  // dataset
  static getDatasetList(reqQuery, params) {
    const url = `${dataset}/v1/list?sort=${reqQuery.sort}&number=${reqQuery.number}&size=${reqQuery.size}&defFl=${reqQuery.delFl}`;
    return RestAPICaller.get(datasetServer, url, params);
  }

  static getDatasetMoreList(page, size, params) {
    const url = `${dataset}/v1/list?number=${page}&size=${size}`;
    let paramsData = {};
    paramsData = params ? params : null;
    return RestAPICaller.get(datasetServer, url, paramsData);
  }

  static getLocalDatasetList(page, size, wkspcId) {
    const url = `${dataset}/v1/local/list?number=${page}&size=${size}&wkspcId=${wkspcId}`;
    return RestAPICaller.get(datasetServer, url);
  }

  static getDownloadLocalData(dataId, wkspcId) {
    const url = `${dataset}/v1/local/download?dataId=${dataId}&wkspcId=${wkspcId}`;
    return RestAPICaller.get(datasetServer, url);
  }

  static getViewLocalData(localDataId) {
    const url = `${dataset}/v1/local/view?localDataId=${localDataId}`;
    return RestAPICaller.get(datasetServer, url);
  }

  static getReadLocalData(localDataId) {
    const url = `${dataset}/v1/local/read?localDataId=${localDataId}`;
    return RestAPICaller.get(datasetServer, url);
  }

  static uploadLocalData(data) {
    const url = `${dataset}/v1/local/upload`;
    const postData = {
      fileNm: data.fileNm,
      dataNm: data.dataNm,
      dataDesc: data.dataDesc,
      pubFl: data.pubFl,
      wkspcId: data.wkspcId,
    };

    return RestAPICaller.post(datasetServer, url, postData);
  }

  static datasetCreate(data) {
    const url = `${dataset}/v1/create`;
    const postData = {
      conditions: data.conditions,
      datas: data.datas,
      dbId: data.dbId,
      extractTp: data.extractTp,
      query: data.query,
      tableNm: data.tableNm,
      dataNm: data.dataNm,
      dataDesc: data.dataDesc,
      dataTp: data.dataTp,
      exptStDt: moment(data.exptStDt).format("YYYYMMDD"),
      exptEdDt: moment(data.exptEdDt).format("YYYYMMDD"),
      pubFl: data.pubFl,
      filePath: data.filePath,
      fileNm: data.fileNm,
      fileSz: data.fileSz,
    };

    return RestAPICaller.post(datasetServer, url, postData);
  }

  static registDatasetSQL(data) {
    const url = `${dataset}/v1/create`;
    const postData = {
      dataNm: data.dataNm,
      dataDesc: data.dataDesc,
      dataTp: data.dataTp,
      queryStr: data.queryStr,
      queryDbTp: data.queryDbTp,
      fileNm: data.fileNm,
      pubFl: data.pubFl,
    };

    return RestAPICaller.post(datasetServer, url, postData);
  }

  static datasetUpdate(data) {
    const url = `${dataset}/v1/update`;
    return RestAPICaller.post(datasetServer, url, data);
  }

  static runQuery(dataId) {
    const url = `${dataset}/v1/query`;
    const postData = {
      dataId,
    };

    return RestAPICaller.post(datasetServer, url, postData);
  }

  static deleteDataset(dataId) {
    const url = `${dataset}/v1/delete`;
    const postData = {
      dataId,
    };

    return RestAPICaller.post(datasetServer, url, postData);
  }

  static deleteLocalDataset(localDataId) {
    const url = `${dataset}/v1/local/delete`;
    const postData = {
      localDataId,
    };

    return RestAPICaller.post(datasetServer, url, postData);
  }

  static getDatasetRead(dataId) {
    const url = `${dataset}/v1/read?dataId=${dataId}`;
    return RestAPICaller.get(datasetServer, url);
  }

  static getDatasetView(dataId) {
    const url = `${dataset}/v1/view?dataId=${dataId}&size=10`;
    return RestAPICaller.get(datasetServer, url);
  }

  static fileUpload(params) {
    const url = `${dataset}/v1/upload`;
    return RestAPICaller.post(datasetServer, url, params);
  }

  static getLocalDatasetFileList(wkspcId) {
    const url = `${dataset}/v1/local/filelist?wkspcId=${wkspcId}`;
    return RestAPICaller.get(datasetServer, url);
  }

  static datasetUpload(dbData) {
    const url = `${dataset}/v1/create`;
    const postData = dbData;
    return RestAPICaller.post(datasetServer, url, postData);
  }

  // database
  static getDatabaseList(reqQuery, params) {
    const url = `${database}/v1/list?sort=${reqQuery.sort}&number=${reqQuery.number}&size=${reqQuery.size}`;
    return RestAPICaller.get(datasetServer, url, params);
  }

  static getDatabaseListTest(dbId) {
    const url = `${database}/v1/test`;
    const postData = {
      dbId,
    };
    return RestAPICaller.post(datasetServer, url, postData);
  }

  static getDatabaseTable(dbId) {
    const url = `${database}/v1/read/tables?dbId=${dbId}`;
    return RestAPICaller.get(datasetServer, url);
  }

  static getDatabaseTableColumn(dbId, tableNm) {
    const url = `${database}/v1/read/columns?dbId=${dbId}&tableNm=${tableNm}`;
    return RestAPICaller.get(datasetServer, url);
  }

  static getDatabaseDatas(dbId, extractTp, tableNm, conditions, query) {
    // 필수가 아닌 요소들을 queryProps 에 담아서 해당 값이 있을 때에만 url 에 추가.
    const queryProps = {
      tableNm: `&tableNm=${tableNm}`,
      conditions: `&conditions=${conditions}`,
      query: `&query=${query}`,
    };
    const url = `${database}/v1/read/datas?dbId=${dbId}&extractTp=${extractTp}${queryProps.tableNm}${queryProps.conditions}${queryProps.query}`;
    return RestAPICaller.get(datasetServer, url);
  }

  static datasetCreateChart(data) {
    const url = `${dataset}/v1/create/charts`;
    const postData = {
      chartNm: data.chartNm,
      chartTp: data.chartTp,
      axis: data.axis,
      conditions: data.conditions,
      dataId: data.dataId,
    };

    return RestAPICaller.post(datasetServer, url, postData);
  }

  static datasetDeleteChart(data) {
    const url = `${dataset}/v1/delete/charts`;
    const postData = {
      chartId: data,
    };

    return RestAPICaller.post(datasetServer, url, postData);
  }
}
