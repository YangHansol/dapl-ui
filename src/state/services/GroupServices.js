import RestAPICaller from "./RestAPICaller";

export default class GroupService {
  static getGroupList(query) {
    const url = `/v1/group/list`;
    return RestAPICaller.get(url, query);
  }

  static createGroup(postData) {
    const url = `/v1/group/create`;
    return RestAPICaller.post(url, postData);
  }

  static updateGroup(postData) {
    const url = `/v1/group/update`;
    return RestAPICaller.post(url, postData);
  }

  static deleteGroup(groupId) {
    const url = `/v1/group/delete`;
    const postData = {
      groupId: groupId
    };

    return RestAPICaller.post(url, postData);
  }
}
