import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import { ExploreIcon } from "../../../utils/styles/Icons";
import styled from "styled-components";
import CustomInputUi from "../Input/CustomInputUi";

const useStyles = makeStyles({
  list: {
    width: 320
  },
  fullList: {
    width: "auto"
  }
});

const StyledForm = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  padding: 1rem;
`;

const StyledButton = styled.span`
  color: ${props =>
    props.buttoncolorprops ? props.buttoncolorprops : "inherit"};
`;

export default function GroupDrawer({
  buttonProps,
  buttoncolorprops,
  resourceList,
  createGroup
}) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false
  });
  const [buttonText] = useState(buttonProps);

  const toggleDrawer = (side, open) => event => {
    // if (
    //   event.type === "keydown" &&
    //   (event.key === "Tab" || event.key === "Shift")
    // ) {
    //   return;
    // }
    setState({ ...state, [side]: open });
  };

  const sideList = side => (
    <div className={classes.list} role="presentation">
      <StyledForm>
        <CustomInputUi
          resourceList={resourceList}
          createGroup={createGroup}
          toggleDrawer={toggleDrawer(side, false)}
        />
      </StyledForm>
    </div>
  );

  return (
    <div>
      <Button
        onClick={toggleDrawer("right", true)}
        style={{ backgroundColor: "#9c27b0" }}
      >
        <StyledButton buttoncolorprops={buttoncolorprops}>
          {buttonText ? buttonText : <ExploreIcon />}
        </StyledButton>
      </Button>

      <Drawer
        anchor="right"
        open={state.right}
        onClose={toggleDrawer("right", false)}
      >
        {sideList("right")}
      </Drawer>
    </div>
  );
}
