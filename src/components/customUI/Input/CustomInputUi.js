import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import Snackbar from "@material-ui/core/Snackbar";
import MySnackbarContentWrapper from "../snackbar/Snackbar";
import ListItem from "@material-ui/core/ListItem";

import styled from "styled-components";

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: 230
    }
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 230
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
  list: {
    width: 370
  },
  fullList: {
    width: "auto"
  }
}));

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 0 auto;
  width: 100%;
`;

const TitleWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: baseline;
  border-bottom: 1px solid rgba(0, 0, 0, 0.2);
  margin-bottom: 1rem;
  width: 100%;
  color: #2c2c2e;
`;

const InputWrapper = styled.div``;

const Title = styled.div`
  font-size: 1.7rem;
  font-weight: 500;
  padding: 1rem 0;
`;

const InputTitle = styled.div`
  color: #2c2c2e;
  font-weight: bolder;
  margin-top: 1rem;
`;

const StyledList = styled(List)`
  display: flex;
  margin: 0.5rem 0;
  width: 12rem;
  height: 4.6rem;
  margin: 0 auto;
`;

const StyledListItem = styled(ListItem)`
  border: 1px solid #9c27b0;
  border-radius: 3px;
  text-align: center;
  margin: 1rem 0.5rem 0;
`;

const StyledTextField = styled(TextField)`
  /* input {
    padding: 1rem;
  } */
`;

export default function CustomInput({
  resourceList,
  createGroup,
  toggleDrawer
}) {
  // drawer style, groupState
  const classes = useStyles();
  const [groupState, setGroupState] = React.useState({
    age: "",
    groupNm: "",
    asgndCpuNum: "",
    asgndDisSz: "",
    asgndMemSz: ""
  });

  const handleChange = name => event => {
    setGroupState({
      ...groupState,
      [name]: event.target.value
    });
  };

  const onSubmit = e => {
    e.preventDefault();
  };

  const [open, setOpen] = React.useState(false);
  const handleClick = () => {
    setOpen(true);
  };
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const validateInput = () => {
    let { groupNm, asgndCpuNum, asgndDisSz, asgndMemSz } = groupState;

    if (groupNm.trim().length === 0) {
      alert("사용자 그룹을 입력해 주세요.");
      return false;
    }

    if (!asgndCpuNum) {
      alert("CPU 값을 선택해 주세요.");
      return false;
    }

    if (!asgndDisSz) {
      alert("디스크 값을 선택해 주세요.");
      return false;
    }

    if (!asgndMemSz) {
      alert("메모리 값을 선택해 주세요.");
      return false;
    }

    return true;
  };

  const onConfirm = () => {
    if (validateInput() === false) return;

    let { groupNm, asgndCpuNum, asgndDisSz, asgndMemSz } = groupState;

    let postData = {
      groupNm,
      asgndCpuNum,
      asgndDisSz,
      asgndMemSz
    };

    createGroup(postData);
    setGroupState({
      age: "",
      groupNm: "",
      asgndCpuNum: "",
      asgndDisSz: "",
      asgndMemSz: ""
    });

    setOpen(true);
    setTimeout(toggleDrawer, 2000);

    // onClose();
  };

  return (
    <>
      <Wrapper>
        <TitleWrapper>
          <Title>그룹 등록</Title>
        </TitleWrapper>
        <InputWrapper>
          <div>
            <InputTitle>그룹명</InputTitle>
            <form
              name="groupNm"
              label="그룹명"
              className={classes.root}
              noValidate
              autoComplete="off"
              onSubmit={onSubmit}
            >
              <StyledTextField
                id="filled-basic"
                variant="outlined"
                value={groupState.groupNm}
                onChange={handleChange("groupNm")}
                inputProps={{
                  name: "groupNm",
                  id: "age-native-simple"
                }}
              />
            </form>
          </div>
          <div>
            <InputTitle>CPU</InputTitle>
            <FormControl
              className={classes.formControl}
              id="asgndCpuNum"
              name="asgndCpuNum"
              variant="outlined"
            >
              <Select
                native
                value={groupState.asgndCpuNum}
                onChange={handleChange("asgndCpuNum")}
                inputProps={{
                  name: "asgndCpuNum",
                  id: "age-native-simple"
                }}
              >
                {resourceList.CPU.map((item, index) => {
                  return (
                    <option key={index} value={item.wkspcCstmNum.toString()}>
                      {item.wkspcCstmNum.toString()}
                    </option>
                  );
                })}
              </Select>
            </FormControl>
          </div>{" "}
          <div>
            <InputTitle>디스크</InputTitle>
            <FormControl className={classes.formControl} variant="outlined">
              <Select
                native
                value={groupState.asgndDisSz}
                onChange={handleChange("asgndDisSz")}
                inputProps={{
                  name: "asgndDisSz",
                  id: "age-native-simple"
                }}
              >
                {resourceList.DISK.map((item, index) => {
                  return (
                    <option key={index} value={item.wkspcCstmSize}>
                      {item.wkspcCstmSize + "GB"}
                    </option>
                  );
                })}
              </Select>
            </FormControl>
          </div>{" "}
          <div>
            <InputTitle>메모리</InputTitle>
            <FormControl className={classes.formControl} variant="outlined">
              <Select
                native
                value={groupState.asgndMemSz}
                onChange={handleChange("asgndMemSz")}
                inputProps={{
                  name: "asgndMemSz",
                  id: "age-native-simple"
                }}
              >
                {resourceList.RAM.map((item, index) => {
                  return (
                    <option key={index} value={item.wkspcCstmSize}>
                      {item.wkspcCstmSize + "GB"}
                    </option>
                  );
                })}
              </Select>
            </FormControl>
          </div>
        </InputWrapper>
        <Divider style={{ margin: "1rem 0" }} />
        <StyledList>
          <StyledListItem button onClick={toggleDrawer}>
            <ListItemText primary={"취소"} />
          </StyledListItem>
          <StyledListItem
            button
            variant="outlined"
            className={classes.margin}
            onClick={onConfirm}
            style={{ backgroundColor: "#9c27b0", color: "white" }}
          >
            <ListItemText primary={"완료"} />
          </StyledListItem>

          <Snackbar
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left"
            }}
            open={open}
            autoHideDuration={2000}
            onClose={handleClose}
          >
            <MySnackbarContentWrapper
              onClose={handleClose}
              variant="success"
              message="완료되었습니다."
            />
          </Snackbar>
        </StyledList>
      </Wrapper>
    </>
  );
}
