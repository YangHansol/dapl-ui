import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import styled from "styled-components";

const StyledCardHeader = styled(CardHeader)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 2rem;
  font-weight: 400;
  margin-top: 1rem;
`;

const StyledCardHeaderTitle = styled.h4`
  font-weight: 500;
  font-size: 1.7rem;
  display: flex;
  align-items: center;
`;

const StyledCard = styled(Card)`
  margin-top: 0.5rem;
  padding: 1rem;
`;

export default { StyledCardHeader, StyledCardHeaderTitle, StyledCard };
