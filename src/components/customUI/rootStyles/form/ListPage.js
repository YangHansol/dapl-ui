import React from "react";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import {
  StyledCardHeader,
  StyledCardHeaderTitle,
  StyledCard
} from "../RootStyles";

export default function Form() {
  return (
    <GridContainer>
      <GridItem>
        <StyledCardHeader>
          <StyledCardHeaderTitle>여기 안에서는 custom</StyledCardHeaderTitle>
        </StyledCardHeader>

        <StyledCard>
          <CardBody>
            {/* or <StyeldCardBody> */}
            여기 안에서는 custoom
          </CardBody>{" "}
          {/* or </StyeldCArdBody> */}
        </StyledCard>
      </GridItem>
    </GridContainer>
  );
}
