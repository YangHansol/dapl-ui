import "styled-components";
import { Theme } from "./MuiTheme";

// and extend them!
declare module "styled-components" {
  export interface DefaultTheme extends Theme {
    PrimaryColor: string;
    PrimaryLightColor: string;
    GrayColor: string;
    GrayLightColor: string;
    Border: string;
  }
}
