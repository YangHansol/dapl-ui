import { createMuiTheme } from "@material-ui/core/styles";

const muiTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#9c27b0",
      light: "#ede7f6",
    },
  },
});

export const theme = {
  ...muiTheme,
  PrimaryColor: "#9c27b0",
  PrimaryLightColor: "#ede7f6",
  GrayColor: "#E1E1E1",
  // GrayLightColor: "#FAFAFA",
  GrayLightColor: "#f5f5f5",
  Border: "1px solid #E1E1E1",
  GrayColor6: "#1c1c1e",
  GrayColor5: "#2c2c2e",
};
