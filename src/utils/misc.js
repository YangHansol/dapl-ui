export function generateAPIQuery(params) {
  let query = { ...params };
  for (let key of Object.keys(query)) {
    if (Array.isArray(query[key] === true)) {
      query[key] = query[key].toString();
    } else if (query[key] === "") {
      delete query[key];
    }
  }
  return query;
}
