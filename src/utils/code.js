// Common
export const DateFormat = {
  full: "YYYY-MM-DD HH:mm:ss",
  RemoveSecondFull: "YYYY-MM-DD HH:mm",
  half: "YYYY-MM-DD",
  time0To12: "hh:mm",
  fullTime0To12: "hh:mm:ss",
  time0To24: "HH:mm",
  fullTime0To24: "HH:mm:ss",
  year: "year",
  month: "month",
  day: "day",
  hour: "hour",
  minute: "minute",
  second: "second"
};

export const ShareText = {
  Y: "공개",
  N: "비공개",
  P: "미공개"
};

// Algorithm 알고리즘
export const AlgoTypeText = {
  BASE: "기본 제공",
  USER: "사용자 생성"
};
