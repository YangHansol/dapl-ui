import React from "react";
import { Link } from "react-router-dom";

function AdminLayout() {
  return (
    <div>
      <Link to="/admin/algorithm">
        <div>alogorithm</div>
      </Link>
      <Link to="/admin/data">
        <div>data</div>
      </Link>
      <Link to="/admin/dataset">
        <div>dataset</div>
      </Link>
      <Link to="/admin/group">
        <div>group</div>
      </Link>
      <Link to="/admin/experiment">
        <div>experiment</div>
      </Link>
      <Link to="/admin/userprofile">
        <div>userprofile</div>
      </Link>
    </div>
  );
}

export default AdminLayout;
