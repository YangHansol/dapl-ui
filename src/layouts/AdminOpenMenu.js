import React from "react";
import clsx from "clsx";
import { Link, withRouter } from "react-router-dom";
import styled from "styled-components";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import TemporaryDrawer from "components/customUI/SideMenu/TemporaryDrawer";
import GroupIcon from "@material-ui/icons/Group";
import PersonIcon from "@material-ui/icons/Person";
import DashboardIcon from "@material-ui/icons/Dashboard";
import CodeIcon from "@material-ui/icons/Code";
import ListIcon from "@material-ui/icons/List";
import ListAltIcon from "@material-ui/icons/ListAlt";
// import LibraryAddIcon from "@material-ui/icons/LibraryAdd";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import CheckIcon from "@material-ui/icons/Check";
import CreateIcon from "@material-ui/icons/Create";
import LaptopMacIcon from "@material-ui/icons/LaptopMac";
import Badge from "@material-ui/core/Badge";
// import MailIcon from "@material-ui/icons/Mail";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import logoNone from "components/img/logoNone.png";
const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: 12
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap"
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1
    }
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  }
}));

const StyledLogo = styled.div`
  position: absolute;
  top: 50%;
  left: 50px;
  display: block;
  width: 33px;
  height: 33px;
  content: "";
  transform: translateY(-50%);
  background: url(${logoNone});
  background-size: cover;
  background-position: 100% 50%;
  margin: 0 1.4rem;
`;

const StyledAppBar = styled(AppBar)`
  background-color: #2c2b2e;
  display: grid;
  grid-template-columns: 9fr 1fr;
  align-items: center;
`;

const StyledAppBarMenu = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  padding: 0 1rem;
`;

const Content = styled.main`
  padding: 8rem 5rem 0;
  width: 100%;
  height: 100%;
  /* @media (max-width: 1280px) {
    padding: 8rem 0 0;
  } */
`;

const StyledListItem = styled.li`
  border-right: 2px solid
    ${props => (props.current === "true" ? "#9c27b0" : "inherit")};
  background-color: ${props =>
    props.current === "true" ? "#ede7f6" : "inherit"};
  display: flex;
  justify-content: flex-end;
  padding-left: 2px;
  &:after {
    content: "";
    height: 0;
    /* width: 2px; */
    border-right: 2px solid #9c27b0;
    transition: height 0.3s;
  }
  &:hover::after {
    height: 3rem;

    /* transition: height 0.3s; */
  }
  border-right: 2px solid
    ${props => (props.current === "true" ? "#9c27b0" : "inherit")};
  background-color: ${props =>
    props.current === "true" ? "inherit" : "inherit"};
  color: ${props => (props.current === "true" ? "#9c27b0" : "black")};
`;

const StyledListItemIcon = styled(ListItemIcon)`
  color: ${props => (props.current === "true" ? "#9c27b0" : "")};
`;

const StyledButton = styled.div`
  transform: ${props => (props.togglemenu === true ? "rotate(90deg)" : "")};
  transition: 0.1s linear;
`;

const StyeldListItemText = styled(ListItemText)``;

function AdminOpenMenu({ children, location: { pathname } }) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  // console.log(open);
  // console.log(pathname);

  const handleDrawerOpen = () => {
    setOpen(!open);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
    console.log(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <StyledAppBar
        position="fixed"
        className={clsx(classes.appBar, {
          // [classes.appBarShift]: open
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              // [classes.hide]: open
            })}
            style={{ paddingLeft: "0.3rem" }}
          >
            <StyledButton togglemenu={open}>
              <MenuIcon />
            </StyledButton>
          </IconButton>
          <Link to="/">
            <Typography variant="h6" noWrap style={{ color: "white" }}>
              <StyledLogo />{" "}
              <span style={{ margin: "0 3rem" }}>AICP-ADMIN</span>
            </Typography>
          </Link>
        </Toolbar>
        <StyledAppBarMenu>
          <IconButton aria-label="4 pending messages" onClick={handleClick}>
            <Badge badgeContent={10} color="secondary">
              <ListAltIcon style={{ color: "white" }} />
            </Badge>
          </IconButton>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            style={{ margin: "2rem 0 0" }}
          >
            <MenuItem onClick={handleClose}>algorithm</MenuItem>
            <MenuItem onClick={handleClose}>dataset</MenuItem>
            <MenuItem onClick={handleClose}></MenuItem>
          </Menu>
          <IconButton aria-label="4 pending messages" onClick={handleClick}>
            <Badge badgeContent={4} color="secondary">
              <NotificationsIcon style={{ color: "white" }} />
            </Badge>
          </IconButton>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            style={{ margin: "2rem 0 0" }}
          >
            <MenuItem onClick={handleClose}>algorithm</MenuItem>
            <MenuItem onClick={handleClose}>dataset</MenuItem>
            <MenuItem onClick={handleClose}>workspace</MenuItem>
          </Menu>
          <TemporaryDrawer />
        </StyledAppBarMenu>
      </StyledAppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open
          })
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <Divider />

        <List style={{ padding: "0" }}>
          <Link to="/admin/dashboard">
            <StyledListItem
              current={pathname === "/admin/dashboard" ? "true" : "false"}
            >
              <ListItem button key={"DashBoard"}>
                <StyledListItemIcon
                  current={pathname === "/admin/dashboard" ? "true" : "false"}
                >
                  <DashboardIcon />
                </StyledListItemIcon>
                <StyeldListItemText primary={"대쉬보드"} />
              </ListItem>
            </StyledListItem>
          </Link>
          <Link to="/admin/group">
            <StyledListItem
              current={pathname === "/admin/group" ? "true" : "false"}
            >
              <ListItem button key={"Group"}>
                <StyledListItemIcon
                  current={pathname === "/admin/group" ? "true" : "false"}
                >
                  <GroupIcon />
                </StyledListItemIcon>
                <StyeldListItemText primary={"그룹"} />
              </ListItem>
            </StyledListItem>
          </Link>
          <Link to="/admin/user">
            <StyledListItem
              current={pathname === "/admin/user" ? "true" : "false"}
            >
              <ListItem button key={"User"}>
                <StyledListItemIcon
                  current={pathname === "/admin/user" ? "true" : "false"}
                >
                  <PersonIcon />
                </StyledListItemIcon>
                <StyeldListItemText primary={"사용자"} />
              </ListItem>
            </StyledListItem>
          </Link>
          <Link to="/admin/workspace">
            <StyledListItem
              current={pathname === "/admin/workspace" ? "true" : "false"}
            >
              <ListItem button key={"Workspace"}>
                <StyledListItemIcon
                  current={pathname === "/admin/workspace" ? "true" : "false"}
                >
                  <LaptopMacIcon />
                </StyledListItemIcon>
                <StyeldListItemText primary={"워크스페이스"} />
              </ListItem>
            </StyledListItem>
          </Link>
          <Link to="/admin/dataset">
            <StyledListItem
              current={pathname === "/admin/dataset" ? "true" : "false"}
            >
              <ListItem button key={"Dataset"}>
                <StyledListItemIcon
                  current={pathname === "/admin/dataset" ? "true" : "false"}
                >
                  <LibraryBooksIcon />
                </StyledListItemIcon>
                <StyeldListItemText primary={"데이터셋"} />
              </ListItem>
            </StyledListItem>
          </Link>{" "}
          <Link to="/admin/package">
            <StyledListItem
              current={pathname === "/admin/package" ? "true" : "false"}
            >
              <ListItem button key={"Package"}>
                <StyledListItemIcon
                  current={pathname === "/admin/package" ? "true" : "false"}
                >
                  <ListIcon />
                </StyledListItemIcon>
                <StyeldListItemText primary={"패키지"} />
              </ListItem>
            </StyledListItem>
          </Link>{" "}
          <Link to="/admin/packagepack">
            <StyledListItem
              current={pathname === "/admin/packagepack" ? "true" : "false"}
            >
              <ListItem button key={"Pack"}>
                <StyledListItemIcon
                  current={pathname === "/admin/packagepack" ? "true" : "false"}
                >
                  <ListAltIcon />
                </StyledListItemIcon>
                <StyeldListItemText primary={"패키지 팩"} />
              </ListItem>
            </StyledListItem>
          </Link>{" "}
          <Link to="/admin/algorithm">
            <StyledListItem
              current={pathname === "/admin/algorithm" ? "true" : "false"}
            >
              <ListItem button key={"Algorithm"}>
                <StyledListItemIcon
                  current={pathname === "/admin/algorithm" ? "true" : "false"}
                >
                  <CodeIcon />
                </StyledListItemIcon>
                <StyeldListItemText primary={"알고리즘"} />
              </ListItem>
            </StyledListItem>
          </Link>
          <Link to="/admin/experiment">
            <StyledListItem
              current={pathname === "/admin/experiment" ? "true" : "false"}
            >
              <ListItem button key={"Release"}>
                <StyledListItemIcon
                  current={pathname === "/admin/experiment" ? "true" : "false"}
                >
                  <CreateIcon />
                </StyledListItemIcon>
                <StyeldListItemText primary={"실험"} />
              </ListItem>
            </StyledListItem>
          </Link>{" "}
          <Link to="/admin/release">
            <StyledListItem
              current={pathname === "/admin/release" ? "true" : "false"}
            >
              <ListItem button key={"Release"}>
                <StyledListItemIcon
                  current={pathname === "/admin/release" ? "true" : "false"}
                >
                  <CheckIcon />
                </StyledListItemIcon>
                <StyeldListItemText primary={"배포"} />
              </ListItem>
            </StyledListItem>
          </Link>{" "}
        </List>

        <Divider />
      </Drawer>
      <Content>{children}</Content>
    </div>
  );
}

export default withRouter(AdminOpenMenu);
