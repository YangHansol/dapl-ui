import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
// import styled from "styled-components";
import Dashboard from "./views/Dashboard/Dashboard";
import AlgorithmContainerTest2 from "./views/Algorithm/AlgorithmContainerTest2";
import DatasetListContainer from "./views/Dataset/DatasetListContainer";
import DatasetCreate from "./views/Dataset/create/DatasetCreate";
import ExperimentContainer from "./views/Experiment/ExperimentContainer";
import UserProfile from "./views/UserProfile/UserProfile";
import PackageContainer from "./views/Package/PackageContainer";
import PackagePackContainer from "./views/PackagePack/PackagePackContainer";
import GroupContainer from "./views/Group/GroupContainer";
import ReleaseContainer from "./views/Release/ReleaseContainer";
import WorkspaceContainer from "./views/Workspace/WorkspaceContainer";
// import Admin from "./layouts/Admin";
import AdminLayout from "./layouts/AdminLayout";
import AdminOpenMenu from "./layouts/AdminOpenMenu";
import UserContainer from "./views/User/UserContainer";
import DatasetDetail from "./views/Dataset/detail/DatasetDetail";

// import DashboardOrigin from "./views/Dashboard/DashboradOrigin";

// const Loading = styled.div`
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   margin: 10rem auto;
//   font-size: 3rem;
// `;

function RouterComponent() {
  return (
    <Router>
      <>
        <AdminOpenMenu>
          <Switch>
            <Route
              path={["/", "/admin/dashboard"]}
              exact
              component={Dashboard}
            />
            <Route path="/admin/admin" component={AdminLayout} />
            <Route
              path="/admin/algorithm"
              exact
              component={AlgorithmContainerTest2}
            />
            <Route
              path="/admin/dataset"
              exact
              component={DatasetListContainer}
            />
            <Route
              path="/admin/dataset/create"
              exact
              component={DatasetCreate}
            />

            <Route
              path="/admin/dataset/detail/:dataId"
              exact
              component={DatasetDetail}
            />

            <Route
              path="/admin/experiment"
              exact
              component={ExperimentContainer}
            />
            <Route path="/admin/group" exact component={GroupContainer} />
            <Route path="/admin/user" exact component={UserContainer} />
            <Route path="/admin/package" exact component={PackageContainer} />
            <Route
              path="/admin/packagepack"
              exact
              component={PackagePackContainer}
            />
            <Route
              path="/admin/experiment"
              exact
              component={ExperimentContainer}
            />
            <Route path="/admin/release" exact component={ReleaseContainer} />
            <Route path="/admin/userprofile" exact component={UserProfile} />
            <Route
              path="/admin/workspace"
              exact
              component={WorkspaceContainer}
            />

            {/* <Route path="/detail/:id" component={Detail} /> */}
            <Redirect from="*" to="/" />
          </Switch>
        </AdminOpenMenu>
      </>
    </Router>
  );
}

export default RouterComponent;
