# dapl-ui project

- 기존 템플릿 기능을 사용하기 위해 package.json 파일과 index 및 App 파일에서 필요한 css, script 자료들을 적용해 줘야 한다.

## style rules

- 기본구조
  material-ui 의 기본 제공값을 그대로 사용한다. (그것을 스타일 규칙으로 활용한다.)

```
  Card
    CardContent
      (Header)
    CardContet
      (Body)
```

- custom

  - material-ui 를 styled-components 로 감싸서 커스텀.
  - style = {} 안에 담아서 엘리먼트 생성. (가독성)
  - 공통으로 사용되는 엘리먼트는 엘리먼트를 생성하고, 공통 엘리먼트 중 특정 부분의 변경이 필요할 경우 className 사용하여 변경.

- theme

  - 스타일 공통화를 위해 theme 사용 권장.
  - 현재 js 이나 편의를 위해 ts 로 변경 가능.

- state 관리
  기존 코드를 활용.

  - (2019. 01. 07 수정내용 추가)

    - 기존의 props 전달 방식을 함수형에서 단계를 줄여 사용할 수 있도록 hooks 사용.
    - modules, selector hook, dispatch hook 의 구조.
    - 추후 location, history 등의 데이터 역시 custom hook 을 작성해 공유하여 사용할 수 있게 도입 가능.

  - mapStateToProps, matDispatchToProps 를 사용하여 connect 한다.
    이때 mapDispatchToProps 에서 해당 컴포넌트의 constants 파일에서 필요한 데이터를 입력해 줘야한다.
    직접 기본 값을 설정하거나, 파일을 가져와서 입력한다.

  - 함수형 컴포넌트이기 때문에 그에 맞게 코드 정리를 한다.

  - useEffect 사용
    useEffect 는 componentDidMount, componentDidUpdate 를 대체한다기보다는
    마운트 된 뒤에 작업을 진행하며, 그 데이터를 기억하고, 동기화 시킨다는 개념으로 이해하여 작업한다.

    그렇기 때문에 loadList() 와 같이 값을 불러오고, 업데이트 됐을 시 다시 init 함수를 통해 다시 호출하고, loadList() 를 다시 호출하지 않고,
    값들을 useEffect 에 담고, dependency 에 주시해야할(변경시 업데이트 할) 요소들을 지정한다.

  - useCallBack 사용
    loadList() 같은 경우는 useEffect() 외부에서 정의하며,
    useCallBack 을 통해 최적화 한다.
    useCallBack 을 사용하여 컴포넌트가 렌더링 될 때 한 번 함수가 생성되며,
    그 후에는 dependecy 의 값에 따라 다시 함수가 생성된다.

  - redux hook 사용
    기존 코드의 작동을 확인한 뒤 redux hook 도입.
    useSelector, useDispatch 를 사용하여 가독성을 높인다.

  - location, history, contants 등의 props
    해당 props 들은 Container 까지 도달하는데 굉장히 많은 경로를 통해 오게 된다.
    그 과정에서 props 전달을 위한 컴포넌트가 생기게 되고
    이것은 데이터의 이해를 어렵게 한다.

    location, hitory 와 같은 데이터는 router hook 을 통해 커스텀 hooks 을 만들어서 쓸 수 있을 것 같다.

    contants 는 각 컴포넌트마다 필요한 값들을 정의해둔 파일인데,
    이것은 각 컴포넌트 디렉토리로 옮겨 필요한 컴포넌트에서 바로 가져와서 쓰는게 좋은 방법일 수 있다.

- 코드 구조 및 props 전달 과정
  PageContainer: props 는 history, location 등의 값이기 때문에 router hook 을 만들어서 사용할 수 있을 듯.
  ->
  ConfigRouter
  ->
  AlgorithmContainer
  ->
  AlgorithmRouter
  ->
  AlgorithmList: 여기서 constants 도 전달. Constants 는 변하는 값이 아닌 정의 된 값이기 때문에 props 로 전달하지 않아도 될 듯. 따로 컴포넌트에서 직접 정의하거나, 호출해서 값 가져올 수 있을 듯.
  ->
  AlgorithmListContainer

- API 호출
  Container -> Actions -> Services -> RestAPICaller
  Container 에서 호출할 때 constants 값을 넣고, 그 값을 기반으로 api 를 호출한다.

- RestAPICaller

  - api 호출 주소 설정

- api 통신 설정
  - 통신하는 요소가 공통 요소라면 CommonReducer 사용.
  - 각 카테고리별 특정 요소라면 각 카테고리의 Action, Reducer, Service 설정.
  - Common 또는 각 요소의 설정에 따라 useSelector, useDispatch 설정.
  - 기존 포맷을 최대한 활용하며 관련 있는 요소들끼리 정리.
  - 컴포넌트에서 dispatch 를 통해 호출, selector 통해 조회.
